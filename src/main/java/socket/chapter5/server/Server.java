package socket.chapter5.server;

import socket.chapter5.constants.TCPConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 17:07
 **/
public class Server {
    public static void main(String[] args) throws IOException {
        TCPServer tcpServer = new TCPServer(TCPConstants.PORT_SERVER);
        boolean isSucceed = tcpServer.start();
        if (!isSucceed) {
            System.out.println("Start TCP server failed!");
            return;
        }
        UDPProvider.start(TCPConstants.PORT_SERVER);
        // 构建服务器端口的键盘输入信息流
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String str;
        // 循环监听
        do {
            // 读取信息
            str = bufferedReader.readLine();
            // 发送至客户端
            tcpServer.broadcast(str);
        } while (!"bye~~~".equalsIgnoreCase(str));

        UDPProvider.stop();
        tcpServer.stop();
    }
}
