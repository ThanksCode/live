package com.com.basic.java;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-13 10:47
 **/
public enum Demo {

    T1(1,"t1"),
    T2(2,"T2");

    private int code ;
    private String msg;
    private Demo(int i , String msg){
        code = i ;
        this.msg = msg;
    }

}