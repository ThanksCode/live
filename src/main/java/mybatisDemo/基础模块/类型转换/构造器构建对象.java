package mybatisDemo.基础模块.类型转换;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 10:59
 **/
class person {
    private String name ;
    public person(String name ){
        this.name = name;
        System.out.println("init name: " + name);
    }

    @Override
    public String toString() {
        return name;
    }
}
public class 构造器构建对象 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class clazz = Class.forName("mybatisDemo.基础模块.类型转换.person");
        Constructor  constructors = clazz.getDeclaredConstructor(String.class);
        String name = "zhangSan";
        Object obj = constructors.newInstance(name);
        // 没有默认构造器 这样的实例化无法行通
        //Object obj = clazz.newInstance();
        HashMap<String,String > map = new HashMap<>();
        System.out.println(map.isEmpty());


    }
}
