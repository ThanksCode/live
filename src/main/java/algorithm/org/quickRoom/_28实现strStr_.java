package algorithm.org.quickRoom;
/***
 * 实现 strStr() 函数。
 *
 * 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如果不存在，则返回  -1。
 *
 * 示例 1:
 *
 * 输入: haystack = "hello", needle = "ll"
 * 输出: 2
 * 示例 2:
 *
 * 输入: haystack = "aaaaa", needle = "bba"
 * 输出: -1
 * 说明:
 *
 * 当 needle 是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。
 *
 * 对于本题而言，当 needle 是空字符串时我们应当返回 0 。这与C语言的 strstr() 以及 Java的 indexOf() 定义相符。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/implement-strstr
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-23 20:15
 **/
public class _28实现strStr_ {

    /***
     * 方法一；暴力匹配算法
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr(String haystack, String needle , int index ) {
        //处理非法数据信息
        if( needle.length() == 0) {
            return 0;
        }

        //进行匹配
        //注意此处匹配下标  i <= haystack.length()
        //一点小优化：避免了遍历整个字符串的操作，优化了部分时间效率
        for( int i = 0 ; i <= haystack.length() - needle.length() ; i ++ ) {
            //顺次匹配
            int count = 0 ;
            for (  int j = 0 ; j < needle.length() ; j ++ ) {
                if( haystack.charAt(i + count) == needle.charAt(j)){
                    count ++ ;
                }else{
                    break;
                }
            }
            if ( count == needle.length() ) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 双指针
     * @param haystack
     * @param needle
     * @return
     */

    public int strStr(String haystack, String needle ) {
        //求得字符串长度信息
        int L = needle.length(), n = haystack.length();
        //异常值的处理
        if (L == 0) {
            return 0;
        }

        //haystack 移动游标索引
        int  pn = 0;
        //或者  pn <= L - n
        while ( pn < n - L + 1) {
            //寻找第一个元素的匹配位置
            while ( pn < n - L - 1 && haystack.charAt(pn) != needle.charAt(0)){
                //避免无用比较，先找到第一个可以匹配的字符信息
                pn ++ ;
            }

            //进行校验
            int currentLen = 0 , pl = 0 ;
            while ( pl < L && pn < n && haystack.charAt(pn) == needle.charAt(pl)){
                //pl用于标定子串失配位置
                pl ++ ;
                //记录匹配长度，便于后序回溯
                currentLen ++ ;
                // pn 记录主串失配位置
                pn ++ ;
            }
            //匹配成功
            if( currentLen == n) {
                return pn - currentLen;
            }

            //回溯处理  无需回溯pl!
            pn = pn - currentLen + 1;

        }
        return -1;
    }

    public int str_v1(String haystack, String needle){

        int L = haystack.length();
        int n = needle.length();

        // 此时字串needle内容为空，属于特殊情况
        if ( n == 0 ){
            return 0;
        }

        // 遍历比较
        // 标定haystack中内容信息
        int ph = 0;
        while ( ph < L-  n + 1) {
            // 寻找主串元素同字串元素第一次出现相同的位置
            while ( ph < L - n + 1
                    && haystack.charAt(ph) != needle.charAt(0)){
                ph ++ ;
            }

            // 匹配后序内容
            // len 记录匹配长度信息
            int len = 0 , pn = 0;

            while ( ph < L && pn < n
                    && haystack.charAt(ph) == needle.charAt(pn)){
                ph ++ ;
                pn ++ ;
                len ++ ;
            }

            if ( len == n ) {
                return ph - n;
            }

            // 回溯处理
            ph = ph - len + 1;
        }


        return -1;
    }
}
