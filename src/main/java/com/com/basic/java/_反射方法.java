package com.com.basic.java;

import com.web.strategy.UserController;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-17 18:35
 **/
public class _反射方法 {


    public static void main(String[] args) {
        Class clazz = UserController.class;
        Method[] methods = clazz.getDeclaredMethods();
        for (Method m: methods
                ) {
            System.out.println(m.getName());
            Class<?> [] parameters = m.getParameterTypes();
            for (Class p : parameters
                 ) {
                System.out.println(p.getSimpleName());
            }
            //System.out.println(m.getParameterTypes());
        }
    }
}
