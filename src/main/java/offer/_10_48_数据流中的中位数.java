package offer;

import java.util.PriorityQueue;
/***
 * 用大顶堆+小顶堆方法，可以看作大顶堆是普通班，小顶堆是实验班。
 * 数量上时刻保持 小顶-大顶<=1（两堆相等或者小顶比大顶多一个）。
 *
 * 新学生先入普通班（大顶堆），此时可能会失去平衡了，于是取大顶堆的第一个（班里最好的学生）
 * 加入实验班（小顶堆），判断若数量过多（不是等于或多一个），
 * 取第一个（实验班里最差的学生）到普通班（大顶堆）里。
 * 取中位数的时候，若两堆数量相等，
 * 则各取堆顶取平均，若小顶比大顶多一，则多的那一个就是中位数。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-22 19:41
 **/
public class _10_48_数据流中的中位数 {

    // 大顶堆，存储左半边元素
    private PriorityQueue<Integer> left = null;

    //小顶堆，存储右半边元素，并且右半边元素都大于左半边
    private PriorityQueue<Integer> right = new PriorityQueue<>();
    // 统计信息总数
    private  int count = 0 ;
    /** initialize your data structure here. */
    public void MedianFinder() {
        left = new PriorityQueue<>((o1, o2) -> o2 - o1);
        right = new PriorityQueue<>();
    }

    /***
     *  N 为偶数的情况下插入到右半边。
     *因为右半边元素都要大于左半边，但是新插入的元素不一定比左半边元素来的大，
     *因此需要先将元素插入左半边，然后利用左半边为大顶堆的特点，取出堆顶元素即为最大元素，此时插入右半边
     *
     * 左侧维护一个大顶堆，右侧维护一个小顶堆
     * @param num
     */
    public void addNum(int num) {
        // 偶数情况
        if( count % 2 == 0){
            left.add(num);
            right.add(left.poll());
        }else{
            right.add(num);
            left.add(right.poll());
        }
        count ++ ;

    }

    public double findMedian() {
        if (count % 2 == 0){
            return (left.peek() + right.peek()) / 2.0;
        } else{
            return (double) right.peek();
        }

    }
}
