package algorithm.org.DataAlgorithm;

import offer.TreeNode;
/***
 * 98. 验证二叉搜索树
 * 给定一个二叉树，判断其是否是一个有效的二叉搜索树。
 *
 * 假设一个二叉搜索树具有如下特征：
 *
 * 节点的左子树只包含小于当前节点的数。
 * 节点的右子树只包含大于当前节点的数。
 * 所有左子树和右子树自身必须也是二叉搜索树。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-28 16:31
 **/
public class _9__验证二叉搜索树 {

      public boolean isValidBST(TreeNode root) {
        if ( root == null) {
            return true;
        }

        return isValidBST(root,null,null);
    }


    public boolean isValidBST(TreeNode root, Integer minVal, Integer maxVal) {
        if (root == null) return true;

        if (minVal != null && root.val <= minVal) return false;
        if (maxVal != null && root.val >= maxVal) return false;

        return isValidBST(root.left, minVal, root.val) && isValidBST(root.right, root.val, maxVal);
    }

    // 遇到问题提供 根节点值 Integer.MAX_VALUE
    private boolean isValidBst(TreeNode root, int min ,int max){
        if ( root == null) {
            return true;
        }

        // root.val > min && root.val < max
        if ( root.val <= min || root.val >= max){
            return true;
        }

        return isValidBst(root.left,min,root.val)
                && isValidBst(root.right,root.val,max);
    }

    public void function(TreeNode root){
          // 二叉搜索树中序遍历是有序的

    }

}
