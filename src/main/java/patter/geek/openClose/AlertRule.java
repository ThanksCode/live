package patter.geek.openClose;

import java.util.Random;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 16:49
 **/
public class AlertRule {


    private int errorCount = 100000;

    /***
     * 此处仅仅为生成方法，实际场景可以利用map来保存各个api所对应的允许的最大错误数信息
     * @param api
     * @return
     */

    // 获取最大错误数
    public int getMaxchedRule(String api) {
        return errorCount;
    }

    // 使用构造链的形式进行处理 ~~~ 主要重构，不是追求功能
    public AlertRule getMatchedRule(String api) {
        return new AlertRule();
    }

    // 单纯为了不报错~~~~
    public int getMaxTps() {
        return new Random().nextInt(200);
    }
}
