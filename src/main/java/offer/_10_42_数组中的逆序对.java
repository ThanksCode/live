package offer;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-18 22:51
 **/
public class _10_42_数组中的逆序对 {

    // 记录逆序对数量信息
    private int count = 0 ;
    private int [] tmp;
    public int reversePairs(int[] nums) {
        merge(nums,0,nums.length - 1);
        tmp = new int [nums.length];
        // 不要随意取模
        return (int) (count % 1000000007);
    }

    private void merge(int [] arr, int lo , int hi) {
        if ( lo < hi ){
            int mid = lo + ( hi - lo) / 2;
            merge(arr,lo,mid);
            merge(arr,mid + 1,hi);
            mergeSort(arr,lo,mid,hi);
        }
    }

    private void mergeSort(int[] arr, int lo, int mid, int hi) {
        int [] aux = new int[arr.length];
        // 此时必超时
        for ( int i = 0 ; i < arr.length ;i ++ ) {
            aux[i] = arr[i];
        }

        // 归并处理
        int left = lo , right = mid + 1;
        for ( int i = lo ; i <= hi ; i ++) {
            // 处理越界情况
            if( left > mid) {
                arr[i] = aux[right ++ ];
            }else if ( right > hi ) {
                arr[i] = aux[left ++ ];
                // 应该是aux中元素比较
            }else if ( arr[left] < arr[right] ){
                // 此时不算逆序对
                arr[i] = aux[left++];
            }else{
                // arr[left....mid] > arr[right]
                count += mid - left + 1;
                arr[i] = aux[right++];
            }
        }
    }




}

class MergeAccess{

    // 记录逆序对数量信息
    private long count = 0 ;
    private int [] aux;
    public int reversePairs(int[] nums) {
        aux = new int [nums.length];
        merge(nums,0,nums.length - 1);
        return (int) (count );
    }

    private void merge(int [] arr, int lo , int hi) {
        if ( hi - lo < 1) {
            return;
        }
        int mid = lo + ( hi - lo) / 2;
        merge(arr,lo,mid);
        merge(arr,mid + 1,hi);
        mergeSort(arr,lo,mid,hi);

    }

    private void mergeSort(int[] arr, int lo, int mid, int hi) {
        for ( int i = lo ; i <=hi ;i ++ ) {
            aux[i] = arr[i];
        }
        // 归并处理
        int left = lo , right = mid + 1;
        for ( int i = lo ; i <= hi ; i ++) {
            // 处理越界情况
            if( left > mid) {
                arr[i] = aux[right ++ ];
            }else if ( right > hi ) {
                arr[i] = aux[left ++ ];
            }else if ( aux[left] <= aux[right] ){
                // 此时不算逆序对
                arr[i] = aux[left++];
            }else{
                // arr[left....mid] > arr[right]
                arr[i] = aux[right++];
                count += mid - left + 1;

            }
        }
    }
}