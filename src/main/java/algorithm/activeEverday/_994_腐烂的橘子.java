package algorithm.activeEverday;

/***
 * 在给定的网格中，每个单元格可以有以下三个值之一：
 *
 * 值 0 代表空单元格；
 * 值 1 代表新鲜橘子；
 * 值 2 代表腐烂的橘子。
 * 每分钟，任何与腐烂的橘子（在 4 个正方向上）相邻的新鲜橘子都会腐烂。
 *
 * 返回直到单元格中没有新鲜橘子为止所必须经过的最小分钟数。如果不可能，返回 -1。
 *
 *  
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/rotting-oranges
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-3-4
 *
 * 解法：BFS算法的应用
 *      腐烂橘子传染的是跟它相邻的新鲜橘子，所以传染的时候要判断被传染的橘子是不是新鲜橘子。
 *      如果是，则先改变其值为3，表示是新腐烂的。
 *          如果直接改为2，会导致其旁边的橘子也直接被传染，而不是一分钟后再被传染。
 *      等所有新感染的橘子都确定了之后，再让它们统一改为2。
 *

 */
public class _994_腐烂的橘子 {
    //标记是否产生新腐烂的橘子
    private boolean flag = true;
    public int orangesRotting(int[][] grid) {
        //使用：int sum = -1 sum ++ 这种循环方式还是可以通过185个标准样例，但是遇到[[1,2]]时出现错误！！
        int sum = 0;
        while ( flag) {
            flag = false;//自己设置退出条件，此处如果不改变flag值，则flag会一直为true函数会一直循环
            grid = rotting(grid);//进行感染处理
           // sum ++ ;不能每次都递增，需要注意的时，只有当有新鲜橘子被感染后在进行递增
            if( flag ){
                sum ++ ;
            }
        }

        if(isHavingFresh(grid)){
            return -1;
        }

        return sum;
    }

    //判断是否新鲜的橘子
    private boolean isHavingFresh(int [][] grid){
        for ( int i = 0 ; i < grid.length ; i ++ ){
            for ( int j = 0 ; j < grid[0].length ; j ++ ){
                if ( grid[i][j] == 1) {
                    return  true;
                }
            }
        }
        return  false;
    }

    //腐烂传染

    /**
     * 判断传染的方法，在于枚举四个方向观察是否有新鲜的橘子
     *  对于判断grid[i][j] == 2的处理
     *      此处绝对不能直接置为2.这样处理话，在第一轮遍历之后数组中与其相邻的元素都会被感染
     *
     * @param grid
     * @return
     */
    private int[][] rotting(int [][] grid){


        for( int i = 0 ; i < grid.length ; i ++ ) {
            for ( int j = 0 ; j < grid[0].length; j ++ ){
                if( grid[i][j] == 2) {//表明有一个橘子腐烂！
                    //枚举四个方向，进行值的判断

                    //左
                    if( i - 1 >= 0 && grid[i-1][j] == 1) {
                        grid[i-1][j] = 4;
                    }
                    //右
                    if ( i + 1 < grid.length && grid[i+1][j] == 1){
                        grid[i+1][j] = 4;
                    }

                    //上
                    if( j - 1 >= 0 && grid[i][j-1] == 1){
                        grid[i][j-1] = 4;
                    }

                    //下
                    if( j + 1 < grid[i].length &&  grid[i][j+1] == 1){
                        grid[i][j+1] = 4;
                    }
                }
            }
        }

        //通过循环遍历，将可能被感染的橘子选则出来，之后进行感染
        int cn = 0;//记录是否有新的橘子被感染
        for( int i = 0 ; i < grid.length ; i ++ ){
            for ( int j = 0 ; j < grid[i].length ; j ++ ){
                if( grid[i][j] == 4) {
                    grid[i][j] = 2;
                    cn ++ ;
                }
            }
        }

        //flag的作用在于标记是否有新的被感染橘子
        if ( cn != 0) {
            flag = true;
        }

        return grid;
    }

}
