package algorithm.org.DataAlgorithm;

import offer.TreeNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-08-01 13:24
 **/
public class _14_二叉搜索树的第k大节点 {

    private int count  = 0 ;
    private TreeNode ret = null;
    public int kthLargest(TreeNode root, int k) {
        inOrder(root,k);
        return ret.val;
    }

    private void inOrder(TreeNode root,int k ){
        if( root == null || count >= k) {
            return;
        }
        inOrder(root.right,k);
        count ++ ;
        if ( count == k) {
            ret = root;
        }
        inOrder(root.left,k);
    }
}
