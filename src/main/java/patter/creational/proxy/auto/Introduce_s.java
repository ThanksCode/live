package patter.creational.proxy.auto;



import java.lang.reflect.Method;

public class Introduce_s implements InvocationHandler_s {

    private Person target;

    //获取被代理人的个人资料
    public Object getInstance(Person target) throws Exception{
        this.target = target;
        Class clazz = target.getClass();
        System.out.println("被代理对象的class是:"+clazz);
        return Proxy_s.newProxyInstance(new ClassLoader_s(), clazz.getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("我是媒婆：得给你找个异性才行");
        System.out.println("开始进行海选...");
        System.out.println("------------");
        method.invoke(this.target, args);//对当前对象的所有方法进行增强
        System.out.println("------------");
        System.out.println("如果合适的话，就准备办事");
        return null;
    }
}
