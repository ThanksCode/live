package offer;

/***
 * 输入一棵二叉树的根节点，判断该树是不是平衡二叉树。如果某二叉树中任意节点的左右子树的深度相差不超过1，
 * 那么它就是一棵平衡二叉树。
 *
 * #date 2020-3-22
 */
public class _10_27_平衡二叉树 {

    public boolean isBalanced(TreeNode root) {

        if ( root == null) {
            return true;
        }

        if ( Math.abs(getHeight(root.left) - getHeight(root.right)) < 1) {
            return isBalanced(root.left) && isBalanced(root.right);
        }
        return false;
    }

    private int getHeight(TreeNode root ) {
        if ( root == null) {
            return 0;
        }
        int left = getHeight(root.left) ;
        int right = getHeight(root.right);

        return Math.max(left , right) + 1;
    }
}
