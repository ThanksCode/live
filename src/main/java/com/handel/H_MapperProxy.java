package com.handel;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class H_MapperProxy implements InvocationHandler {

    private H_SqlSession sqlSession;
    public H_MapperProxy(H_SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    //动态代理生成信息
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {

        //当名称空间匹配时加载信息
        if ( method.getDeclaringClass().getName().equals(H_Configuration.MapperXml.nameSpace)){
            String sql = H_Configuration.MapperXml.methodSqlMapping.get(method.getName());
            return sqlSession.selectOne(sql,String.valueOf(args[0]));
        }

        return null;
    }
}
