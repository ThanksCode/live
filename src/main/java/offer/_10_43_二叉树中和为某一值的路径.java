package offer;

/**
 * 剑指 Offer 34. 二叉树中和为某一值的路径
 * 输入一棵二叉树和一个整数，打印出二叉树中节点值的和为输入整数的所有路径。从树的根节点开始往下一直到叶节点所经过的节点形成一条路径。
 *
 *
 *
 * 示例:
 * 给定如下二叉树，以及目标和 sum = 22，
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 17:36
 **/
public class _10_43_二叉树中和为某一值的路径 {

    // 结果集合信息
    private List<List<Integer>> rs = new ArrayList<>();

    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        backTrace(root,sum,new ArrayList<Integer>());
        return rs;
    }

    private void backTrace(TreeNode root, int sum, ArrayList<Integer> paths) {
        // 递归到空节点
        if ( root == null) {
            return;
        }

        paths.add(root.val);
        sum -= root.val;

        // 表明此时已经找到合适的路径
        // 题目规定必须是遍历到叶子结点，此时的约束不单单是 sum = 0这么简单
        if ( sum == 0 && root.left == null && root.right == null) {
            // 此时必须重新创建一个数组，使用原先的会导致引入后序被修改的问题
            rs.add(new ArrayList<>(paths));
        }else{
            backTrace(root.left,sum,paths);
            backTrace(root.right,sum,paths);
        }
        // 移除掉元素 便于回溯处理
        paths.remove(paths.size() - 1);
    }


}
