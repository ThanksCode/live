package com.com.basic.java.类型信息.transfer;

import com.com.basic.java.类型信息.transfer.dao.AccountDao;
import com.com.basic.java.类型信息.transfer.dao.DynamicProxy;
import com.com.basic.java.类型信息.transfer.dao.impl.AccountDaoImpl;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-25 15:36
 **/
public class Demo {

    public static void main(String[] args) {
        AccountDao accountDao = (AccountDao)new DynamicProxy().getInstance(new AccountDaoImpl());
        if (accountDao == null) {
            System.out.println("nullpointer");
        }
        //转账
        accountDao.transfer("zhangSan" , "Lisi",100);
        accountDao.add("zhangFei");

    }
}
