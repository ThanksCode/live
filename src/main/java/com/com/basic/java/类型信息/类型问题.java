package com.com.basic.java.类型信息;



/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-18 10:42
 **/
class father {
    static {
        System.out.println("father class");
    }
}
class son extends father {
    static {
        System.out.println("son class");
    }
}
public class 类型问题 {
    public static void main(String[] args) {
        Class<son> s = son.class;
        Class<? super son> up = s.getSuperclass();
        //Class<father> f= s.getSuperclass();
        System.out.println(1);

    }
}
