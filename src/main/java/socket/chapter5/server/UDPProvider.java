package socket.chapter5.server;

import socket.chapter5.constants.UDPConstants;
import socket.chapter5.utils.ByteUtils;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.UUID;

/** 客户端信息提供者
 *  接受广播包，并进行服务器端信息的回送
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 15:46
 **/
public class UDPProvider {

    // 提供一个实例
    private static Provider PROVIDER_INSTANCE;

    static void start(int port) {
        // 确保单例  避免多个回送
        stop();
        String sn = UUID.randomUUID().toString();
        Provider provider = new Provider(sn, port);
        provider.start();
        PROVIDER_INSTANCE = provider;
    }

    // 关闭连接
    static void stop() {
        if (PROVIDER_INSTANCE != null) {
            PROVIDER_INSTANCE.exit();
            PROVIDER_INSTANCE = null;
        }
    }


    /****
     * 服务提供者
     */
    private static class Provider extends Thread{

        // 口令信息
        private final byte [] sn;
        // 发送信息的端口信息
        private final int port ;
        // 退出服务的标志信息
        private boolean done = false;
        // 回送信息包
        private DatagramSocket ds = null;
        // 存储客户端回送消息的Buffer
        final byte[] buffer = new byte[128];

        Provider(String sn, int port) {
            super();
            this.sn = sn.getBytes();
            this.port = port;
        }


        @Override
        public void run() {
            super.run();

            System.out.println("UDPProvider Started.");

            try{
                // 监听30201端口信息
                ds = new DatagramSocket(UDPConstants.PORT_SERVER);
                // 接收消息的Packet
                DatagramPacket receivePack = new DatagramPacket(buffer, buffer.length);

                while( !done ) {
                    // 接受发送的数据包信息
                    ds.receive(receivePack);

                    // 发送者的IP地址
                    String clientIp = receivePack.getAddress().getHostAddress();
                    int clientPort = receivePack.getPort();
                    int clientDataLen = receivePack.getLength();
                    byte[] clientData = receivePack.getData();
                    // head 后紧跟两个字节 用于存放指定 short 占用两个字节  客户端回送字节信息
                    boolean isValid = clientDataLen >= (UDPConstants.HEADER.length + 2 + 4)
                            && ByteUtils.startsWith(clientData, UDPConstants.HEADER);
                    // 打印接收到的信息与发送者的信息
                    System.out.println("ServerProvider receive form ip:" + clientIp
                            + "\tport:" + clientPort + "\tdataValid:" + isValid);

                    if (!isValid) {
                        // 无效继续
                        continue;
                    }

                    // 解析指令
                    // 指令格式 密钥头 + 命令(short 类型) +  端口信息
                    //  命令  1 表示接收搜索  2 表示回送服务器信息


                    // 获取信息公共的头部长度信息
                    // 解析命令与回送端口
                    int index = UDPConstants.HEADER.length;
                    short cmd = (short) ((clientData[index++] << 8) | (clientData[index++] & 0xff));
                    int responsePort = (((clientData[index++]) << 24) |
                            ((clientData[index++] & 0xff) << 16) |
                            ((clientData[index++] & 0xff) << 8) |
                            ((clientData[index] & 0xff)));

                    // 构建一份回送信息
                    if (cmd == 1 && responsePort > 0) {
                        // 构建一份回送数据
                        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
                        byteBuffer.put(UDPConstants.HEADER);
                        byteBuffer.putShort((short) 2);
                        byteBuffer.putInt(port);
                        byteBuffer.put(sn);
                        int len = byteBuffer.position();
                        // 直接根据发送者构建一份回送信息
                        DatagramPacket responsePacket = new DatagramPacket(buffer,
                                len,
                                receivePack.getAddress(),
                                responsePort);
                        // 回送信息
                        ds.send(responsePacket);
                        System.out.println("ServerProvider response to:" + clientIp + "\tport:" + responsePort + "\tdataLen:" + len);
                    } else {
                        System.out.println("ServerProvider receive cmd nonsupport; cmd:" + cmd + "\tport:" + port);
                    }
                }
            }catch (Exception ignore){
                // 忽略异常信息
            }finally {
                close();
            }
            // 完成
            System.out.println("UDPProvider Finished.");
        }

        // 关闭 DatagramSocket 的连接
        private void close(){
            if ( ds != null ) {
                ds.close();
                ds = null;
            }
        }

        // 退出发送信息
        private void exit(){
            done = true;
            close();
        }


    }
}
