package com.com.basic.java;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-14 14:54
 **/
class User {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

   /* @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }*/

    @Override
    public boolean equals(Object obj) {
        return this.getName() != ((User)obj).getName();
    }
}
class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }


    public int getX() {
        return x;
    }


    public void setX(int x) {
        this.x = x;
    }


    public int getY() {
        return y;
    }


    public void setY(int y) {
        this.y = y;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Point other = (Point) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }


    @Override
    public String toString() {
        return "x:" + x + ",y:" + y;
    }

}

public class _值传递思考 {

    private  static void change(User user){
        User u1 = new User();
        u1.setName("zhangsNA");
        user.setName("lisi");
        boolean flag = u1.equals(user);

        System.out.println("u1 hascode" + u1.hashCode() +"u2 hascode " + user.hashCode());
        System.out.println(flag);
    }
    public static void main(String[] args) {
//        int x = 2;
//        int x1 = x ;
//        User user = new User();
//        user.setName("zhangSan");
//        change(user);
//        System.out.println(user);
//        System.out.println(1);
        Collection set = new HashSet();
        Point p1 = new Point(1, 1);
        Point p2 = new Point(1, 2);

        set.add(p1);
        set.add(p2);

        p2.setX(10);
        p2.setY(10);
        // 会导致删除失败！
        boolean flag = set.remove(p2);

        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Object object = iterator.next();
            System.out.println(object);
        }


    }
}
