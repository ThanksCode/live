package com.com.springmvc.v2.service.impl;

import com.com.springmvc.v2.annotation.Service;
import com.com.springmvc.v2.service.IModifyService;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:57
 **/
@Service("aa")
public class ModifyService implements IModifyService {

    @Override
    public String add(String name, String addr) {
        return "invork add name = " + name + ",addr=" + addr;
    }

    @Override
    public String remove(Integer id) {
        return "invork remove id = " + id;
    }

}
