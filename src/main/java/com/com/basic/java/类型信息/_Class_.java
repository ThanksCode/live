package com.com.basic.java.类型信息;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-24 18:48
 **/
class Base{

}
class Dervied extends Base{

}

public class _Class_ {
    public static void main(String[] args) {
        Dervied dervied = new Dervied();
        System.out.println(("x instanceof base " + (dervied instanceof Base)));
        System.out.println("x == Baese " + ( (Object)dervied.getClass() == Base.class));
        Class tClass = Dervied.class;
        Class [] interfaces = tClass.getDeclaredClasses();
        Class [] interfaces1 = tClass.getInterfaces();
        System.out.println(interfaces.length);
        for (Class c : interfaces) {
            System.out.println(c.getSimpleName());
        }
    }
}
