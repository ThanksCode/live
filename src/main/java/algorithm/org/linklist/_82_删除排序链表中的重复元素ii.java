package algorithm.org.linklist;

import offer.ListNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-09 21:29
 **/
public class _82_删除排序链表中的重复元素ii {

    /***
     * 重建一个链表，对于有重复元素值的结点不进行放入
     * @param head
     * @return
     */
    public ListNode deleteDuplicates(ListNode head) {
        if ( head == null) {
            return null;
        }

        // 保存新生成的链表信息
        ListNode pre = new ListNode(-1);
        pre.next = null;
        // cur用于遍历处理
        ListNode cur = pre;

        while ( head != null ) {
            boolean flag = false;
            ListNode last = head.next;

            // 寻找是否有相同值信息
            while ( last != null && last.val == head.val){
                last = last.next;
                flag = true;
            }
            // 插入结点信息
            if ( !flag ) {
                ListNode tmp = head;
                cur.next = tmp;
                tmp.next = null;
                cur = tmp;
            }

            head = last;
        }

        return pre.next;
    }
}
