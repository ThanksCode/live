package algorithm.org.binaryTree;
/***
 * 给定一个二叉树，返回它的中序 遍历。
 *
 * 示例:
 *
 * 输入: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * 输出: [1,3,2]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/binary-tree-inorder-traversal
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

import offer.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-28 18:12
 **/
public class _中序遍历 {

    private List<Integer> result = new ArrayList<>();
    public List<Integer> inorderTraversal(TreeNode root, int index ) {
        inOrderTree(root);
        return result;
    }

    //递归版本
    private void inOrderTree(TreeNode root) {

        if (root == null) {
            return;
        }
        inOrderTree(root.left);
        result.add(root.val);
        inOrderTree(root.right);
    }


    //非递归版本
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        // 处理特殊情况
        if ( root == null){
            return list;
        }

        Stack<TreeNode> stack = new Stack<>();

        while ( root != null || !stack.isEmpty()){

            // 一路向左
            while( root != null) {
                stack.push(root);
                root = root.left;
            }

            // 存放元素信息
            if ( !stack.isEmpty()){
                TreeNode cur = stack.pop();
                list.add(cur.val);
                root = cur.right;
            }

        }

        return list;
    }

    // 重载无法以返回值类型进行区分处理
    // 中序版本 2020-7-28
    public List<Integer> inorderTraversalV1(TreeNode root){

        if ( root == null ){
            return null;
        }
        List<Integer> rs = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);

        while ( !stack.isEmpty()){
            // 取出第一个元素
            TreeNode cur = stack.peek();
            // 一路向左
            while ( cur.left != null) {
                stack.push(cur.left);
                cur = cur.left;
            }
            // 弹出结点
            cur = stack.pop();
            // 右子树怎么办？
            rs.add(cur.val);
             if ( cur.right != null) {
                stack.push(cur.right);
            }
        }


        return rs;
    }




    public List<Integer> inorderTraversalV2(TreeNode root){
        if ( root == null ){
            return null;
        }
        List<Integer> rs = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        // 需要有一个结点记录位置，避免重复访问某结点
        while( root != null || !stack.isEmpty()){
            // 将结点全部放入
            while ( root != null){
                stack.push(root);
                root = root.left;
            }
            // 弹出结点
            if ( !stack.isEmpty()){
                TreeNode cur = stack.pop();
                rs.add(cur.val);
                root = cur.right;

            }
        }
        return rs;
    }



    // 递归校验
    double last = -Double.MAX_VALUE;
    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        if (isValidBST(root.left)) {
            if (last < root.val) {
                last = root.val;
                return isValidBST(root.right);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(3);

        _中序遍历 obj = new _中序遍历();
        obj.inorderTraversalV1(root);
    }


}
