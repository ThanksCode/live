package patter.geek.openClose;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 16:47
 **/
public class Alert {

    private List<AbstractAlertHandler> alertHandlers = new ArrayList<>();
    public void addAlertHandler(AbstractAlertHandler alertHandler) {
        this.alertHandlers.add(alertHandler);
    }

    public void check(AlertInof apiStatInfo) {
        for (AbstractAlertHandler handler : alertHandlers) {
            handler.check(apiStatInfo);
        }
    }
}
