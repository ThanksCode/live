package algorithm.org.algorithm4;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-27 18:40
 **/
public class QuickSort {

    public void quickSort(int [] arr , int lo , int hi){
        if ( lo < hi ){
            int index = partion(arr,lo , hi);
            quickSort(arr,lo,index - 1);
            quickSort(arr,index +1 , hi);
        }
    }
    // 三向切分快排
    public void threeQucikSort(int [] arr ,int lo , int hi ){
        if ( lo < hi ) {
            int lt = lo , i = lo + 1, gt = hi;
            int point = arr[lo];
            while ( i <= gt ){
                // 大于放到最后
                if ( arr[i] > point){
                    swap(arr,i,gt -- );
                }else if ( arr[i] < point){
                    swap(arr,lt++,i++);
                }else{
                    i ++ ;
                }
            }
            threeQucikSort(arr,lo,lt - 1);
            threeQucikSort(arr,gt + 1 , hi);
        }
    }
    private int partion(int [] arr, int lo , int hi){
        int i = lo , j = hi ;
        int point = arr[lo];
        while ( i < j ) {
            while ( i < j && arr[j] > point ){
                j --;
            }
            while ( i <j && arr[i] <= point){
                i ++ ;
            }
            swap(arr,i,j);
        }
        swap(arr,lo,i);
        return i ;
    }

    private void swap(int [] arr , int i , int j ) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }


    public static void main(String[] args) {
        QuickSort sort = new QuickSort();
        int [] arr = SortUtils.createArray(1000);
        System.out.println("before ......" + SortUtils.compare(arr));
        sort.quickSort(arr,0,arr.length-1);
        System.out.println("after ......" + SortUtils.compare(arr));

    }

}
