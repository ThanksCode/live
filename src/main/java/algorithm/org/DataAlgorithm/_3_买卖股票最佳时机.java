package algorithm.org.DataAlgorithm;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-23 18:56
 **/
public class _3_买卖股票最佳时机 {

    /***
     * 121. 买卖股票的最佳时机
     * 给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
     *
     * 如果你最多只允许完成一笔交易（即买入和卖出一支股票一次），设计一个算法来计算你所能获取的最大利润。
     *
     * 注意：你不能在买入股票前卖出股票。
     * @param prices
     * @return
     */
    // 121 版本1

    public int maxProfit(int[] prices) {
        // 非法数据
        if ( prices.length == 0 || prices == null){
            return 0;
        }
        // maxPrice最大收益  minPrice记录当前遇到的最小值
        int maxPrice = 0 , minPrice = Integer.MAX_VALUE;

        for( int i = 0 ; i < prices.length ; i ++ ){
            // 更新标定信息
            if( prices[i] < minPrice){
                minPrice = prices[i];
            }
            //  计算差价 跟新价值
            if ( prices[i] - minPrice > maxPrice){
                maxPrice =  prices[i] - minPrice ;
            }
        }
        System.out.println(minPrice);
        return maxPrice;
    }

    // 122 ii

    public int maxProfitV2(int[] prices) {
        // 非法数据
        if ( prices.length == 0 || prices == null){
            return 0;
        }
        // 总价
        int total = 0 ,index = 0;
        while ( index < prices.length){
            while ( index < prices.length - 1 && prices[index] >= prices[index + 1] ){
                index ++ ;
            }
            // 波谷
            int valley = prices[index];
            while( index < prices.length - 1 && prices[index] < prices[index + 1]){
                index ++ ;
            }
            // 波峰
            int peek = prices[index];
            total += peek - valley;
            // 后移位置
            index ++ ;
        }

        return total;
    }
    public static void main(String[] args) {
        _3_买卖股票最佳时机 obj = new _3_买卖股票最佳时机();
        int [] rs = {7,1,5,3,6,4};
        System.out.println(obj.maxProfit(rs));

    }
}
