package mybatisDemo.基础模块.Pool;


import org.apache.ibatis.datasource.pooled.PooledDataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-23 10:21
 **/
public class PoolState {
    protected PooledDataSource dataSource;
    protected final List<PooledConnection> idleConnections = new ArrayList();
    protected final List<PooledConnection> activeConnections = new ArrayList();
    protected long requestCount = 0L;
    protected long accumulatedRequestTime = 0L;
    protected long accumulatedCheckoutTime = 0L;
    protected long claimedOverdueConnectionCount = 0L;
    protected long accumulatedCheckoutTimeOfOverdueConnections = 0L;
    protected long accumulatedWaitTime = 0L;
    protected long hadToWaitCount = 0L;
    protected long badConnectionCount = 0L;

    public PoolState(PooledDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public synchronized long getRequestCount() {
        return this.requestCount;
    }

    public synchronized long getAverageRequestTime() {
        return this.requestCount == 0L ? 0L : this.accumulatedRequestTime / this.requestCount;
    }

    public synchronized long getAverageWaitTime() {
        return this.hadToWaitCount == 0L ? 0L : this.accumulatedWaitTime / this.hadToWaitCount;
    }

    public synchronized long getHadToWaitCount() {
        return this.hadToWaitCount;
    }

    public synchronized long getBadConnectionCount() {
        return this.badConnectionCount;
    }

    public synchronized long getClaimedOverdueConnectionCount() {
        return this.claimedOverdueConnectionCount;
    }

    public synchronized long getAverageOverdueCheckoutTime() {
        return this.claimedOverdueConnectionCount == 0L ? 0L : this.accumulatedCheckoutTimeOfOverdueConnections / this.claimedOverdueConnectionCount;
    }

    public synchronized long getAverageCheckoutTime() {
        return this.requestCount == 0L ? 0L : this.accumulatedCheckoutTime / this.requestCount;
    }

    public synchronized int getIdleConnectionCount() {
        return this.idleConnections.size();
    }

    public synchronized int getActiveConnectionCount() {
        return this.activeConnections.size();
    }


}
