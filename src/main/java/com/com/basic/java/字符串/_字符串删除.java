package com.com.basic.java.字符串;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-19 10:39
 **/
public class _字符串删除 {
    @Override
    public String toString() {
        //return super.toString();
        return "InfiniteRecursion address " + super.toString() + "\n";
    }

    //StringBulider提供的 delete方法
    public static void main(String[] args) {
        String str = "helloword";
        System.out.println(str.length());
        System.out.println(new StringBuilder(str).delete(str.length() - 4 , str.length() ));
        _字符串删除 oj = new _字符串删除();
        System.out.println(oj);
    }
}
