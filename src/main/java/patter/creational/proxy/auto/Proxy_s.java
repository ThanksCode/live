package patter.creational.proxy.auto;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/***
 * 手写动态代理类信息
 */
public class Proxy_s {


    private static String ln = "\r\n";
    public static Object newProxyInstance(ClassLoader_s classLoader_s, Class[] interfaces, InvocationHandler_s h) throws IOException, NoSuchMethodException, ClassNotFoundException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //1、生成源代码
        String proxySrc = generateSrc(interfaces[0]);

        //2、将生成的源代码输出到磁盘，保存为.java文件
        String filePath = Proxy_s.class.getResource("").getPath();
        System.out.println("文件地址： ： " + filePath);
        File f = new File(filePath + "$Proxy0.java");
        FileWriter fw = new FileWriter(f);
        fw.write(proxySrc);
        fw.flush();
        fw.close();


        //3、编译源代码，并且生成.class文件
        JavaCompiler  compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager manager = compiler.getStandardFileManager(null, null, null);
        Iterable iterable = manager.getJavaFileObjects(f);

        JavaCompiler.CompilationTask task = compiler.getTask(null, manager, null, null, null, iterable);
        task.call();
        manager.close();

        //4.将class文件中的内容，动态加载到JVM中来
        Class proxyClass = classLoader_s.findClass("$Proxy0");
        Constructor c = proxyClass.getConstructor(InvocationHandler_s.class);
        f.delete();

        //5.返回被代理后的代理对象
        return c.newInstance(h);

    }

    private static String generateSrc(Class<?> interfaces) {
        StringBuffer sb = new StringBuffer();
        sb.append("package patter.creational.proxy.auto;" + ln); //包名
        sb.append("import java.lang.reflect.Method;" + ln);  //拼接待导入的包信息
        sb.append("public class $Proxy0 implements " + interfaces.getName() + "{" + ln);

        //声明InvocationHandler对象信息
        sb.append(" InvocationHandler_s h ;" + ln);
        sb.append("public $Proxy0(InvocationHandler_s h) { " + ln);//构造方法的拼接
        sb.append("this.h = h ;" + ln);
        sb.append("}" + ln);

        //遍历所有方法加以实现
        for(Method m : interfaces.getMethods()) {
            //拼接方法信息
            sb.append("public " + m.getReturnType().getName() +
                    " " + m.getName() + "() {" + ln);

            sb.append("try { " + ln);
            sb.append("Method m = " + interfaces.getName() + ".class.getMethod(\"" + m.getName() + "\" ,new Class[]{});" + ln);
            sb.append("this.h.invoke(this,m,null);" + ln);
            sb.append("}catch(Throwable e){e.printStackTrace();}" + ln);
            sb.append("}" + ln);
        }

        sb.append("}");//拼接最后的}信息
        return sb.toString();
    }
}
