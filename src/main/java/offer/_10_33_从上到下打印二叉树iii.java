package offer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 请实现一个函数按照之字形顺序打印二叉树，即第一行按照从左到右的顺序打印，第二层按照从右到左的顺序打印，第三行再按照从左到右的顺序打印，其他行以此类推。
 *
 *  
 *
 * 例如:
 * 给定二叉树: [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * 返回其层次遍历结果：
 *
 * [
 *   [3],
 *   [20,9],
 *   [15,7]
 * ]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/cong-shang-dao-xia-da-yin-er-cha-shu-iii-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-30 22:58
 **/


public class _10_33_从上到下打印二叉树iii {

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList();
        Queue queue = new LinkedList();

        // 处理异常情况
        if (root == null) {
            return list;
        }

        queue.add(root);

        while ( !queue.isEmpty()){
            // 临时存放结果
            LinkedList<Integer>  rs = new LinkedList<>();

            // 循环遍历
            for ( int i = queue.size() ; i > 0 ; i -- ) {
                // 保存当前结点
                TreeNode cur = (TreeNode) queue.poll();

                 // 表示右 则进行队尾插入
                if( list.size() % 2 == 0) {
                    rs.addLast(cur.val);
                }else{
                    // 表示奇数层，进行队头插入处理
                    rs.addFirst(cur.val);
                }

                // 将cur子节点入栈
                if ( cur.left != null) {
                    queue.add(cur.left);
                }

                if ( cur.right != null) {
                    queue.add(cur.right);
                }
            }

            // 结果集加入到list中
            list.add(rs);

        }
        return list;
    }


}
