package algorithm.org.DataAlgorithm;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-31 13:54
 **/
public class _12_在排序数组中查找数字 {

    public int search(int[] nums, int target) {
        int first = binarySearch(nums,target);
        // 体会此处的处理方式
        int last = binarySearch(nums,target+1);
        return (nums[first] == target) ? last - first : 0;
    }


    private int binarySearch(int [] nums,int target ){
        int i = 0 , j = nums.length - 1;
        // 二分查找
        while ( i < j ) {
            int mid = i + (j - i ) / 2 ;
            if ( nums[mid] >= target){
                j = mid - 1;
            }else{
                i = mid ;
            }
        }
        return i;
    }
}