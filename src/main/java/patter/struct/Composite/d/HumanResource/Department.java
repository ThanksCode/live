package patter.struct.Composite.d.HumanResource;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 17:24
 **/
public class Department extends HumanResource {

    private List<HumanResource> subDepartment = new ArrayList<>();
    private double totalSalary = 0;

    //利用父类构造

    public Department(long id ) {
        super(id);
    }

    @Override
    public double calculateSalary() {
        // 设计浮点运算尽量使用bigDecimal进行计算
        for( HumanResource humanResource : subDepartment){
            totalSalary += humanResource.calculateSalary();
        }
        return totalSalary;
    }

    public void addSubNode(HumanResource hr) {
        subDepartment.add(hr);
    }
}
