package patter.struct.Composite.d.HumanResource;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 17:20
 **/
public class Employee extends HumanResource {

    // 父类没有无参构造器，子类也不能有无参构造器

    public Employee(long id,double salary){
        // 使用父类提供的构造器
        super(id);
        this.salary = salary;
    }

    @Override
    public double calculateSalary() {
        return salary;
    }
}
