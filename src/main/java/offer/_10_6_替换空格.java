package offer;

/***
 * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：s = "We are happy."
 * 输出："We%20are%20happy."
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ti-huan-kong-ge-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-2-7
 */
public class _10_6_替换空格 {

    public String replaceSpace(String s,int index ) {
        StringBuilder sb = new StringBuilder();
        for ( int i = 0 ; i < s.length() ; i ++) {
            if ( s.charAt(i) == ' '){
                sb.append("%20");
            }else{
                sb.append(s.charAt(i));
            }
        }

        return sb.toString();
    }

    // 2020-7-16
    public String replaceSpace(String s ){
        // 处理特殊情况
        if(s == null) {
            return null;
        }
        StringBuilder rs = new StringBuilder(s);
        // 处理字符串信息  将原先的数组信息扩容
        for ( int i = 0 ; i < s.length() ; i ++ ) {
            if ( s.charAt(i) == ' '){
                rs.append("  ");
            }
        }

        int p1 = s.length() - 1;
        int p2 = rs.length() - 1;
        while ( p1 != 0) {
            char cur = s.charAt(p1);

            if ( cur == ' '){
                rs.setCharAt(p2--,'0');
                rs.setCharAt(p2--,'2');
                rs.setCharAt(p2--,'%');
            }else{
                rs.setCharAt(p2--,cur);
            }
        }

        return rs.toString();
    }
}
