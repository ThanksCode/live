package com.com.springmvc.v2.servlet;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:47
 **/
public class Demo {

    public String name ;
    public String password;
    public static void main(String[] args) {

        String packageName = "com.com.springmvc.v2.controller";
        System.out.println(packageName.replaceAll("\\.","/"));
        URL url = Demo.class.getClassLoader().getResource("/" +
                packageName.replaceAll("\\.","/"));
        //替换后的路径  当前文件所处路径+/com/com/springmvc/v2/controller
        if ( url == null) {
            System.out.println("null");
        }
        Class clazz = Demo.class;
        System.out.println(clazz.getName());
        Field [] fields = clazz.getDeclaredFields();
        for ( Field field : fields) {
            System.out.println(field.getType());
        }




    }
}
