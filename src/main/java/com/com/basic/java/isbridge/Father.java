package com.com.basic.java.isbridge;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-15 20:58
 **/
public class Father <T> {

   int method(T msg){
        System.out.println(msg);
        return 1;
    }
}
