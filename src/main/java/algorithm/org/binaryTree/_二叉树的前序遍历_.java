package algorithm.org.binaryTree;

/***
 * 给定一个二叉树，返回它的 前序 遍历。
 *
 *  示例:
 *
 * 输入: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * 输出: [1,2,3]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/binary-tree-preorder-traversal
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

import offer.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-26 21:47
 **/
public class _二叉树的前序遍历_ {

    //保存结果信息
    private List<Integer> outPut = new ArrayList<>();

    //方法一：递归写法
    public List<Integer> preorderTraversal(TreeNode root , int index ) {
        preOrderTree(root);
        return outPut;
    }

    private void preOrderTree(TreeNode root) {
        /*if(root != null) {
            outPut.add(root.val);
        }
        if ( root.left != null) {
            preOrderTree(root.left);
        }
        if ( root.right != null) {
            preOrderTree(root.left);
        }*/

        //递归式解法
        if(root == null) {
            return ;
        }
        outPut.add(root.val);
        //遍历左子树
        preOrderTree(root.left);
        //遍历右子树
        preOrderTree(root.right);

    }


    //迭代写法，借助栈自己模拟递归实现
    public List<Integer> preorderTraversal(TreeNode root  ) {

        //结果存放
        List<Integer> result = new ArrayList<>();
        //处理极端情况
        if ( root == null) {
            return result;
        }
        //利用内置stack数据结构
        Stack<TreeNode> stack = new Stack<>();


        while ( root != null || !stack.isEmpty()) {
            while ( root != null) {
                //将值放入到结果集中
                result.add(root.val);
                stack.push(root);
                //遍历左枝信息
                root = root.left;
            }

            //处理右子树内容
            if ( !stack.isEmpty()){
                TreeNode tmp = stack.pop();
                //重新赋值到root
                root = tmp.right;
            }

        }

        return result;
    }
}
