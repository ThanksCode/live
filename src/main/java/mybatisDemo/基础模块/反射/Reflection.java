package mybatisDemo.基础模块.反射;
/****
 * 仿照mybatis的反射工具类
 * 仅获取Bean对象中的get方法
 */

import java.lang.reflect.Method;
import java.util.*;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-16 10:01
 **/
public class Reflection {

    private final Class<?> type;
    // 可写属性集合
    private final String[] readablePropertyNames;
    private final Map<String, InvokeHandler> getMethods = new HashMap<String,InvokeHandler>();

   public Reflection(Class<?> clazz) {
       this.type = clazz;
       addGetMethods(clazz);
       // toArray会返回一个泛型数组信息
       readablePropertyNames = getMethods.keySet().toArray(new String[getMethods.keySet().size()]);
   }

    private void addGetMethods(Class<?> clazz) {
        Map<String, List<Method>> conflictingGetters = new HashMap<String, List<Method>>();
        // 获取所有方法
        Method[] methods = getClassMethods(clazz);
        for (Method method : methods) {
            // 过滤掉有参数的方法----> get方法不可能有参数信息
            if (method.getParameterTypes().length > 0) {
                continue;
            }
            // 获取get方法的名称信息
            String name = method.getName();
            if ((name.startsWith("get") && name.length() > 3)
                    || (name.startsWith("is") && name.length() > 2)) {
                name = PropertyNamer.methodToProperty(name);
                addMethodConflict(conflictingGetters, name, method);
            }
        }
        resolveGetterConflicts(conflictingGetters);
    }

    /***
     * 获取类的所有方法信息
     * @param cls
     * @return
     */
    private Method[] getClassMethods(Class<?> cls) {
        // 存放类的所有方法信息
        Map<String, Method> uniqueMethods = new HashMap<String, Method>();
        Class<?> currentClas = cls;
        while ( currentClas != null) {
            // 获取类中的所有方法
            addUniqueMethods(uniqueMethods,currentClas.getDeclaredMethods());

            // 处理来自接口和来自父类的方法信息
            Class<?>[] interfaces = cls.getInterfaces();

            for ( int i = 0 ; i < interfaces.length ; i ++ ) {
                addUniqueMethods(uniqueMethods,interfaces[i].getDeclaredMethods());
            }
            // 处理父类信息
            currentClas = currentClas.getSuperclass();

        }

        // 获取到所有的value信息
        Collection<Method> collection = uniqueMethods.values();
        // 返回方法数组信息
        return collection.toArray(new Method[collection.size()]);
    }

    /***
     * 添加方法信息
     * @param uniqueMethods
     * @param methods
     */
    private void addUniqueMethods(Map<String, Method> uniqueMethods, Method[] methods) {
        for(Method method : methods) {
            // 不处理从父类继承而来的泛型方法
            if ( !method.isBridge()){
                // 获取方法的唯一签名
                String signature = getSignature(method);

                //去重
                if( !uniqueMethods.containsKey(signature)){
                    uniqueMethods.put(signature,method);
                }
            }
        }
    }

    /***
     * 为方法生成唯一表示信息
     * 返回值类型＃方法名称：参数列表信息
     * 例如，Reflector.getSignature(Method ）方法的唯一签名是：
     *  java . lang.String#getSignature :  java.lang.reflect .Method
     * @param method
     * @return
     */
    private String getSignature(Method method){
        StringBuilder sb = new StringBuilder();
        // 获取返回类型信息
        Class<?> returnType = method.getReturnType();
        if ( returnType != null) {
            sb.append(returnType + "#");
        }
        //拼接方法名
        sb.append(method.getName());

        // 处理形参列表信息
        Class<?>[] parameters = method.getParameterTypes();
        // 遍历 拼接到字符后
        for( int i = 0 ; i < parameters.length ; i ++ ) {
            if ( i == 0 ) {
                sb.append(":");
            }else{
                sb.append(",");
            }
            sb.append(parameters[i].getName());
        }

        return sb.toString();
    }

    /***
     *
     * @param conflictingGetters
     */
    private void resolveGetterConflicts(Map<String,List<Method>> conflictingGetters) {
        for (Map.Entry<String, List<Method>> entry : conflictingGetters.entrySet()) {
            Method winner = null;
            String propName = entry.getKey();
            for (Method candidate : entry.getValue()) {
                if (winner == null) {
                    winner = candidate;
                    continue;
                }
                Class<?> winnerType = winner.getReturnType();
                Class<?> candidateType = candidate.getReturnType();
                if (candidateType.equals(winnerType)) {
                    if (!boolean.class.equals(candidateType)) {
                        throw new RuntimeException(
                                "Illegal overloaded getter method with ambiguous type for property "
                                        + propName + " in class " + winner.getDeclaringClass()
                                        + ". This breaks the JavaBeans specification and can cause unpredictable results.");
                    } else if (candidate.getName().startsWith("is")) {
                        winner = candidate;
                    }
                } else if (candidateType.isAssignableFrom(winnerType)) {
                    // OK getter type is descendant
                } else if (winnerType.isAssignableFrom(candidateType)) {
                    winner = candidate;
                } else {
                    throw new RuntimeException(
                            "Illegal overloaded getter method with ambiguous type for property "
                                    + propName + " in class " + winner.getDeclaringClass()
                                    + ". This breaks the JavaBeans specification and can cause unpredictable results.");
                }
            }
            addGetMethod(propName, winner);
        }

    }

    /***
     * 对mybatis中的方法进行了简化
     * @param name
     * @param method
     */
    private void addGetMethod(String name, Method method) {
        getMethods.put(name, new GetMethod(method));

    }

    /***
     * 添加方法名---> 方法的映射
     * @param conflictingMethods
     * @param name
     * @param method
     */
    private void addMethodConflict(Map<String, List<Method>> conflictingMethods, String name, Method method) {
        List<Method> list = conflictingMethods.get(name);
        if (list == null) {
            list = new ArrayList<Method>();
            conflictingMethods.put(name, list);
        }
        list.add(method);
    }

}
