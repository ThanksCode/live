package socket.chapter3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 搜索方
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-25 11:45
 **/
public class UDPSearher {

    public static void main(String[] args) throws IOException {
        System.out.println("UDPSearher Started");

        // 指定一个端口用于数据接受数据  系统分配端口
        DatagramSocket ds = new DatagramSocket();
        //构建一份回送数据
        String requestData =  "hello provider : ";
        //回送信息实体
        byte [] requestBytes = requestData.getBytes();
        // 构建回送实体
        DatagramPacket requestPacket = new DatagramPacket(requestBytes,
                requestBytes.length);

        requestPacket.setAddress(InetAddress.getLocalHost());
        // 指定对方接受端口
        requestPacket.setPort(30000);
        //发送
        ds.send(requestPacket);

        // 构建接受实体
        final byte [] buf = new byte[512];
        DatagramPacket receivePack = new DatagramPacket(buf,0,buf.length);

        // 接受信息
        ds.receive(receivePack);
        //打印接收到的信息与发送者的信息

        //发送者ip
        String ip = receivePack.getAddress().getHostAddress();
        int port = receivePack.getPort();
        int dateLen = receivePack.getLength();
        String data = new String(receivePack.getData(),0,dateLen);

        System.out.println("UDPSeacher receive from ip " + ip
                + "\t port :" + port + "\t data :" + data);

        System.out.println("UDPSeacher  Finished .");
        ds.close();
    }
}
