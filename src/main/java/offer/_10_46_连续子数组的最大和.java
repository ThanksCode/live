package offer;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-21 15:04
 **/
public class _10_46_连续子数组的最大和 {

    public int maxSubArray(int[] nums) {
        int maxNum = nums[0] , curNums = nums[0];
        for ( int i = 0 ; i < nums.length ; i ++ ) {
            if ( curNums < 0) {
                curNums = nums[i];
            }else{
                curNums += nums[i];
            }
            // 记录最大元素
            maxNum = maxNum > curNums ? maxNum : curNums;
        }
        return maxNum;
    }

}
