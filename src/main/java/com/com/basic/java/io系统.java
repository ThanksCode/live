package com.com.basic.java;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-30 18:30
 **/
public class io系统 {

    List<File> files = new ArrayList<>();
    List<File> dirs = new ArrayList<>();

    public void scanFile(String path){
        File file = new File(path);

        for (File item : file.listFiles()){
            if ( item.isDirectory()) {
                dirs.add(item);
            }else{
                files.add(item);
            }
        }
    }

    public void printFiles ( ){
        for(File item : files) {
            System.out.println(item.getName());
        }
    }
    public void printDirs ( ){
        for(File item : dirs) {
            System.out.println(item.getName());
        }
    }



    public static void main(String[] args) {
        io系统 io = new io系统();
        io.scanFile("D:\\livehood\\livehood\\src\\main\\java\\com\\com\\basic\\java");
        //System.out.println(io.files.size());
        io.printFiles();
        io.printDirs();
    }
}
