package algorithm.org.DataAlgorithm;

import java.util.Arrays;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-21 22:21
 **/
public class _2_最长公共前缀 {

    // 横向扫描
    public String longestCommonPrefix(String[] strs) {
        // 特殊情况
        if (strs == null || strs.length == 0){
            return "";
        }

        // 前缀信息
        String prefix = strs[0];
        // 遍历集合
        for( int i = 1 ; i < strs.length ; i --) {
            // 直到找到匹配处停止
            while ( strs[i].indexOf(prefix) != 0 ){
                prefix.substring(0,prefix.length() - 2);
            }
        }
        return prefix;
    }

    /***
     * 方法一是横向扫描，依次遍历每个字符串，更新最长公共前缀。另一种方法是纵向扫描。
     * 纵向扫描时，从前往后遍历所有字符串的每一列，
     * 比较相同列上的字符是否相同，如果相同则继续对下一列进行比较，
     * 如果不相同则当前列不再属于公共前缀，当前列之前的部分为最长公共前缀。
     *
     * @param strs
     * @return
     */
    // 纵向扫描
    public String longestCommonPrefixV1(String [] strs ){
        // 特殊情况
        if (strs == null || strs.length == 0){
            return "";
        }

        //避免越界问题，筛取数组中长度最小的数据信息
        String prefix = strs[0];
        int len = prefix.length();
        // 按照列信息比较
        for ( int i = 0 ; i < len ; i ++ ) {
            char cur = prefix.charAt(i);
            for ( int j = 1 ; j < strs.length ; j ++ ) {
                if (  strs[i].length() == len||strs[j].charAt(i) != cur ){
                    prefix = prefix.substring(0,i);
                }
            }
        }
        return prefix;
    }

    // 纵向扫描

    public String longestCommonPrefixV3(String [] strs ) {
        // 特殊情况
        if (strs == null || strs.length == 0) {
            return "";
        }
        //避免越界问题，筛取数组中长度最小的数据信息
        String prefix = strs[0];
        for( int i = 0 ; i < prefix.length(); i ++ ) {
            // 定位信息
            char cur = prefix.charAt(i);
            // 对数组中元素逐位比较
            for( int j = 1 ; j < strs.length ;  j ++ ){
                if( strs[j].charAt(i) != cur ){
                    return prefix.substring(0,i);
                }
            }
        }


        return prefix;
    }

    // 2020 - 7 - 29
    public String longestCommonPrefixV2(String[] strs) {
        // 特殊情况
        if (strs == null || strs.length == 0) {
            return "";
        }
        // 前缀信息
        String prefix = strs[0];

        for( int i = 1 ; i < strs.length; i ++ ) {
            while ( strs[i].indexOf(prefix) != 0) {
                if ( prefix.length() == 0) {
                    return "";
                }
                prefix = prefix.substring(0,prefix.length() -1);
            }
        }

        return prefix;

    }
    public static void main(String[] args) {
        _2_最长公共前缀 obj = new _2_最长公共前缀();
        String [] strs = {"flower","flow","flight"};
        System.out.println(obj.longestCommonPrefixV1(strs));

    }



}
