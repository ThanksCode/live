package patter.creational.factoryMethod;

public class Demo {

    public static void main(String[] args) {
        JavaFactory javaFactory = new JavaFactory();
        Video javaVideo = javaFactory.getVideo();
        Article javaArticle = javaFactory.getArtile();

        javaArticle.writeArticle();
        javaVideo.produce();
    }
}
