package offer;


import java.util.Stack;

/***
 * 输入一个链表，输出该链表中倒数第k个节点。为了符合大多数人的习惯，本题从1开始计数，即链表的尾节点是倒数第1个节点。例如，一个链表有6个节点，从头节点开始，它们的值依次是1、2、3、4、5、6。这个链表的倒数第3个节点是值为4的节点。
 *
 *  
 *
 * 示例：
 *
 * 给定一个链表: 1->2->3->4->5, 和 k = 2.
 *
 * 返回链表 4->5.
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date  2020-2-19
 *
 *
 */
public class _10_13_链表中第K个结点 {

    /****
     * 将节点放入栈中，之后获取到的第size - k 的结点信息
     * @param head
     * @param k
     * @return
     */
    public ListNode getKthFromEnd(ListNode head, int k) {
        Stack<ListNode> stack = new Stack<>();

        while ( head != null) {
            stack.push(head);
            head = head.next;
        }

        return stack.get(stack.size() - k);

    }

    /***
     * 结点处理遍历方式
     * @param head
     * @param k
     * @return
     */
    public ListNode getKthFromEndV2(ListNode head, int k){
        // 首先避免head为空
        // 同时更要注意 k = 0 的特殊情况 以及k < 0的异常情况
        if ( head == null || k <= 0) {
            return head;
        }

        ListNode fast = head;
        ListNode slow = head;
        // 处理 链表长度小于k的情况  保证移动 k - 1个 从零开始的缘故
        for( int i = 0 ; i < k - 1 ; i ++ ) {
            // 指针后移处理
            if ( fast.next != null) {
                fast = fast.next;
            }else{
                // len < k
                return null;
            }
        }


        // 开始移动
        // 此处写成 fast.next != null  写成 fast != null 会导致多移动一个元素
        while ( fast.next != null) {
            slow = slow.next;
            fast = fast.next;
        }
        return slow;

    }

}
