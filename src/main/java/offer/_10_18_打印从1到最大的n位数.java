package offer;

/***
 * 输入数字 n，按顺序打印出从 1 到最大的 n 位十进制数。比如输入 3，则打印出 1、2、3 一直到最大的 3 位数 999。
 *
 * 示例 1:
 *
 * 输入: n = 1
 * 输出: [1,2,3,4,5,6,7,8,9]
 *  
 *
 * 说明：
 *
 * 用返回一个整数列表来代替打印
 * n 为正整数
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/da-yin-cong-1dao-zui-da-de-nwei-shu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-2-23
 *
 * 解题：
 *  考虑一种情况越界问题！！！
 */

import java.util.ArrayList;

public class _10_18_打印从1到最大的n位数 {

    public int[] printNumbers(int n) {
        ArrayList<Integer> list = new ArrayList<>();

        int max = (int)Math.pow(10,n);
        int tmp = 1;
        while ( tmp < max) {
            list.add(tmp++);
        }

        int rs[] = new int[list.size()];
        for ( int i = 0 ; i < list.size() ; i ++ ){
            rs[i] = list.get(i);
        }

        return  rs;
    }
}
