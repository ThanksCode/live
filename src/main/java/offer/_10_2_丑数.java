package offer;

import java.util.ArrayList;

/****
 * 我们把只包含因子 2、3 和 5 的数称作丑数（Ugly Number）。求按从小到大的顺序的第 n 个丑数。
 *
 *  
 *
 * 示例:
 *
 * 输入: n = 10
 * 输出: 12
 * 解释: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 是前 10 个丑数。
 * 说明:  
 *
 * 1 是丑数。
 * n 不超过1690。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/chou-shu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * date: 2020-2-13
 */
public class _10_2_丑数 {

    public int nthUglyNumber(int n) {

        if( n == 0 ){
            return 0;
        }

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
//        丑数必定是2 3 5的整数倍
        int m2 = 0 , m3 = 0 , m5 =0;
        while ( arrayList.size() < n ) {

            int t2 = arrayList.get(m2) * 2;
            int t3 = arrayList.get(m3) * 3;
            int t5 = arrayList.get(m5) * 5;

            int min = Math.min(t2,Math.min(t3,t5));
            arrayList.add(min);

            //这样判断避免了重复元素的情况
            if ( t2 <= min) {
                m2 ++ ;
            }

            if ( t3 <= min ){
                m3 ++ ;
            }

            if ( t5 <= min) {
                m5 ++ ;
            }

        }


        return arrayList.get(arrayList.size() - 1);
    }

    public static void main(String[] args) {
        _10_2_丑数 app = new _10_2_丑数();
        app.nthUglyNumber(10);
    }

}
