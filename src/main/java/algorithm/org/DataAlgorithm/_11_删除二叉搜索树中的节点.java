package algorithm.org.DataAlgorithm;

import offer.TreeNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 14:58
 **/
public class _11_删除二叉搜索树中的节点 {

    public TreeNode deleteNodeV1(TreeNode root, int key) {
        TreeNode cur;
        // 处理异常情况
        if ( root == null){
            return null;
        }

        // 首先找到结点信息

        if( root.val > key) {
            root.left = deleteNodeV1(root.left,key);
        }else if ( root.val > key) {
            root.right = deleteNodeV1(root.right,key);
        }else{
            // 找到了相等的结点处
            // 分情况考虑
          /*  // 1叶子结点
            if( root.left == null && root.right == null) {
                root = null;
            }*/
            if( root.left != null && root.right != null) {
                // 左子树中找一个最大的
                cur = findMax(root.left);
                root.val = cur.val;
                // 在左子树中删除以cur为值的结点
                root.left = deleteNodeV1(root.left,cur.val);
            }else{
                // 左子树不空
                if( root.left != null) {
                    root = root.left;
                }else if ( root.right != null) {
                    root = root.right;
                }
            }
        }

        return root;
    }

    public TreeNode deleteNode(TreeNode root, int key) {
        TreeNode cur;
        // 处理异常情况
        if ( root == null){
            return null;
        }

        // 首先找到结点信息

        if( root.val > key) {
            root.left = deleteNode(root.left,key);
        }else if ( root.val < key) {
            root.right = deleteNode(root.right,key);
        }else{
            // 找到了相等的结点处
            // 分情况考虑
            if( root.left == null && root.right == null) {
                root = null;
            }

            if( root.left != null && root.right != null) {
                // 左子树中找一个最大的
                cur = findMax(root.left);
                root.val = cur.val;
                // 在左子树中删除以cur为值的结点
                root.left = deleteNode(root.left,cur.val);
            }else{
                // 左子树不空
               if (root.left == null) {
                   root = root.right;
               }
               if ( root.right == null ) {
                   root = root.left;
               }
            }
        }

        return root;
    }

    // 左子树中找一个最大的结点进行替代
    // 寻找子树中的最大结点

    private TreeNode findMax(TreeNode root ){
        if ( root == null) {
            return root;
        }

        // 一路向右进行寻找
        while ( root.right != null) {
            root = root.right;
        }
        return root;
    }
}
