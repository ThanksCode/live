package patter.geek.openClose;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 16:48
 **/
public abstract class AbstractAlertHandler {

    protected AlertRule rule;
    protected NotificationEmergencyLevel notificationEmergencyLevel;

    public AbstractAlertHandler(AlertRule alertRule , NotificationEmergencyLevel notificationEmergencyLevel) {
        this.rule = alertRule;
        this.notificationEmergencyLevel = notificationEmergencyLevel;
        System.out.println("父类抽象构造器");
    }

    public abstract void check(AlertInof alertInof);

}
