package patter.struct.Composite.d.MOOC;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 17:09
 **/
public class Course extends CatalogComponent {

    private String name ;
    private double price;

    public Course(String name , double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void print() {
        System.out.println("Course Name:"+name+" Price:"+price);
    }
}
