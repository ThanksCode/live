package offer;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-12 18:00
 **/
public class _10_38_二叉树的镜像 {

    // 利用前序遍历的思路
    public TreeNode mirrorTree(TreeNode root) {
        // 异常处理
        if( root == null ) {
            return  root;
        }
       // 左子树，右子树同时为null
        // 最初写成 root.laft != null && root.right != null -----》return null;
        // 处理[1] 必然出错！
        if ( root.left == null
                && root.right == null) {
            return root;
        }
        // 交换左子树，右子树
        TreeNode cur = root.left;
        root.left =  root.right;
        root.right =  cur;

        // 递归处理
        if ( root.left != null) {
            mirrorTree(root.left);
        }

        if ( root.right != null) {
            mirrorTree(root.right);
        }

        return root;
    }
}
