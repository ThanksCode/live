package patter.geek;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-28 12:02
 **/
public class _滥用方法 {

    private class ShoppItem {
        private String itemId;
        private Double price;

        public String getItemId() {
            return itemId;
        }

        public Double getPrice() {
            return price;
        }
    }

    private Double totalPrice;
    private Integer itemCount ;
    private List<ShoppItem> cart  = new ArrayList<>();

    // 误区：不管是否使用就构建get/set方法

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

   /* public List<ShoppItem> getCart() {
        return cart;
    }
    */

   public List<ShoppItem> getCart(){
       return Collections.unmodifiableList(this.cart);
   }

}


