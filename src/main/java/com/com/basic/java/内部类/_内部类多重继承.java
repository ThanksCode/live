package com.com.basic.java.内部类;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-13 09:05
 **/
public class _内部类多重继承 {

   abstract class E{}

   class Z extends _内部类多重继承 {
       E makeE(){
           return new E(){};
       }
   }


}
