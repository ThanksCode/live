package algorithm.org.binaryTree;

import offer.TreeNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 22:52
 **/
public class _110平衡二叉树 {

    public boolean isBalanced(TreeNode root) {
        if ( root == null) {
            return true;
        }
        // 递归处理 左子树，右子树分别是平衡的，并且高度差不超过2
        return isBalanced(root.left) && isBalanced(root.right)
                && Math.abs(getHeight(root.left) - getHeight(root.right)) < 2 ;
    }

    private int getHeight(TreeNode root) {
        if ( root == null ) {
            return 0;
        }
        // 返回左子树 右子树的最大高度
        int lh = getHeight(root.left);
        int rh = getHeight(root.right);
        int height = lh > rh ? lh : rh;
        return height + 1 ;
    }



    //2020-7-14

    public boolean isBalancedV1(TreeNode root) {
        if ( root == null) {
            return true;
        }

        // 分别处理左右的情况  以及高度情况
        return isBalanced(root.left) && isBalanced(root.right)
                &&Math.abs(getDeep(root.left) - getDeep(root.right)) < 2;

    }
    private int getDeep(TreeNode root) {
        if ( root == null) {
            return 0;
        }

        int left = getDeep(root.left);
        int right = getDeep(root.right);

        int height = left > right ? left : right;

        return height + 1;
    }

}
