package algorithm.org.linklist;

import offer.ListNode;

/**
 * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
 *
 * 示例 1:
 *
 * 输入: 1->1->2
 * 输出: 1->2
 * 示例 2:
 *
 * 输入: 1->1->2->3->3
 * 输出: 1->2->3
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-07 13:41
 **/
public class _83删除排序链表中的重复元素 {

    // 遍历到一个结点后判断其后序结点是否有同该节点值相同的
    public ListNode deleteDuplicates(ListNode head) {
        // 处理异常值信息
        if( head == null) {
            return null;
        }
        // 利用current来遍历遍历，如果使用head,则会导致head指向发生改变
        ListNode current = head;
        // 循环遍历
        while ( current != null) {
            ListNode tmp = current.next;
            // 跳过值相同的结点信息
            while ( tmp != null && tmp.val == current.val){
                // 结点后移
                tmp = tmp.next;
            }
            current.next = tmp;
            // 重新指向结点信息
            current = current.next;

        }
        return head;
    }

    // 删除后序重复结点信息
    public ListNode deleteDuplicatesV1(ListNode head ){
        if( head == null) {
            return null;
        }
        // 保持在移动中head的指向不发生改变
        ListNode cur=  head;

        while ( cur != null) {
            ListNode last = cur.next;
            while ( last != null && last.val == cur.val) {
                // 遇到相同的元素不断后移
                last = last.next;
            }
            // 此时 last.val 同 cur.val 值不一样
            cur.next = last;
            cur = last;
        }

        return head;
    }

}
