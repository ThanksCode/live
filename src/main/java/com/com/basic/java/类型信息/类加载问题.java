package com.com.basic.java.类型信息;

import java.lang.reflect.Constructor;
import java.util.Arrays;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-18 08:50
 **/
class Tony {
    static {
        System.out.println("load Tony");
    }
}

class Jam {
    public Jam(String msg){
        System.out.println(msg);
    }
    static {
        System.out.println("load Jam");
    }
}
public class 类加载问题 {
    public static void main(String[] args) throws ClassNotFoundException {
        new Tony();
        Jam jam = new Jam("1");
        Class.forName("com.com.basic.java.类型信息.Jam");
        Constructor [] constructors = jam.getClass().getConstructors();
        System.out.println(Arrays.toString(constructors));
    }
}
