package com.com.springmvc.v2.controller;

import com.com.springmvc.v2.annotation.Autowired;
import com.com.springmvc.v2.annotation.Controller;
import com.com.springmvc.v2.annotation.RequestMapping;
import com.com.springmvc.v2.annotation.RequestParam;
import com.com.springmvc.v2.service.IModifyService;
import com.com.springmvc.v2.service.IQueryService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:55
 **/
@Controller
@RequestMapping("/web")
public class Action {

    @Autowired
    private IQueryService queryService;
    @Autowired("aa")
    private IModifyService modifyService;

    @RequestMapping("/search/*.do")
    public void search(HttpServletRequest request, HttpServletResponse response,
                       @RequestParam("name") String name){
        String result = queryService.search(name);
        out(response,result);
    }


    @RequestMapping("/add.do")
    public void add(HttpServletRequest request,HttpServletResponse response,
                    @RequestParam("name") String name,
                    @RequestParam("addr") String addr){
        String result = modifyService.add(name,addr);
        out(response,result);
    }


    @RequestMapping("/remove.do")
    public void remove(HttpServletRequest request,HttpServletResponse response,
                       @RequestParam("id") Integer id){
        String result = modifyService.remove(id);
        out(response,result);
    }


    private void out(HttpServletResponse response,String str){
        try {
            response.getWriter().write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
