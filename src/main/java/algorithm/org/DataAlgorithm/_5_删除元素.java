package algorithm.org.DataAlgorithm;

/***
 * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 *
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 *
 *  
 *
 * 示例 1:
 *
 * 给定 nums = [3,2,2,3], val = 3,
 *
 * 函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。
 *
 * 你不需要考虑数组中超出新长度后面的元素。
 * 示例 2:
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-element
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-26 17:05
 **/
public class _5_删除元素 {

    public int removeElement(int[] nums, int val) {
        // cur 表示游标 count表示删除后元素的个数
        int cur = 0;
        for(int i = 0 ; i  < nums.length ; i ++ ){
            if ( nums[i] != val){
                nums[cur ++ ] = nums[i];
            }
        }
        return cur;
    }
}
