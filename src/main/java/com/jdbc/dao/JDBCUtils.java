package com.jdbc.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-09 17:55
 **/
public class JDBCUtils {
    private  static Properties prop = new Properties();
    static {
        InputStream in = JDBCUtils.class.getClassLoader().getResourceAsStream("JdbcConfig.properties");

        try {
            prop.load(in);
            //加载驱动
            Class.forName(prop.getProperty("jdbc.driver"));
        }catch (Exception e ) {
            throw new RuntimeException("初始化错误");
        }

    }


    public static Connection getConnection () {
       Connection con = null;
        try {
            con = DriverManager.getConnection(prop.getProperty("jdbc.url"),
                    prop.getProperty("jdbc.user"),
                    prop.getProperty("jdbc.password"));
        }catch (Exception e) {
            throw new RuntimeException("获取连接失败");
        }

        return con;
    }
}
