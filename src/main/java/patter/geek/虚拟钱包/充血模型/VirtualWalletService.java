package patter.geek.虚拟钱包.充血模型;



import patter.geek.虚拟钱包.贫血模型.InsufficientBalanceException;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 15:27
 **/
public class VirtualWalletService {

    // 通过构造函数或者 IOC 框架注入
    private VirtualWalletRepository walletRepo = new VirtualWalletRepository();
    private VirtualWalletRepository virtualWalletRepo = new VirtualWalletRepository();
    private VirtualWalletTransactionRepository transactionRepo;

    public VirtualWallet getVirtualWallet(Long walletId) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWallet wallet = convert(walletEntity);
        return wallet;
    }

    private VirtualWallet convert(VirtualWalletEntity walletEntity) {
        return new VirtualWallet(walletEntity.getId());
    }

    public BigDecimal getBalance(Long walletId) {
        return virtualWalletRepo.getBalance(walletId);
    }
    public void debit(Long walletId, BigDecimal amount) throws InsufficientBalanceException {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWallet wallet = convert(walletEntity);
        wallet.debit(amount);
        walletRepo.updateBalance(walletId, wallet.balance());
    }
    public void credit(Long walletId, BigDecimal amount) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWallet wallet = convert(walletEntity);
        wallet.credit(amount);
        walletRepo.updateBalance(walletId, wallet.balance());
    }
}
