package patter.geek.虚拟钱包.充血模型;

import patter.geek.虚拟钱包.贫血模型.InsufficientBalanceException;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 15:26
 **/
public class VirtualWallet {

    private Long id;
    private Long createTime = System.currentTimeMillis();;
    private BigDecimal balance = BigDecimal.ZERO;
    public VirtualWallet(Long preAllocatedId) {
        this.id = preAllocatedId;
    }
    public BigDecimal balance() {
        return this.balance;
    }

    /***
     * 将业务功能转接到domain域中
     * @param amount
     * @throws InsufficientBalanceException
     */
    public void debit(BigDecimal amount) throws InsufficientBalanceException {
        if (this.balance.compareTo(amount) < 0) {
            throw new InsufficientBalanceException();
        }
        this.balance.subtract(amount);
    }


    public void credit(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
        }
        this.balance.add(amount);
    }
}
