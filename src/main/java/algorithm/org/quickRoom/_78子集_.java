package algorithm.org.quickRoom;
/***
 * 给定一组不含重复元素的整数数组 nums，返回该数组所有可能的子集（幂集）。
 *
 * 说明：解集不能包含重复的子集。
 *
 * 示例:
 *
 * 输入: nums = [1,2,3]
 * 输出:
 * [
 *   [3],
 *   [1],
 *   [2],
 *   [1,2,3],
 *   [1,3],
 *   [2,3],
 *   [1,2],
 *   []
 * ]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/subsets
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-25 18:25
 **/
public class _78子集_ {

    List<List<Integer>> output = new ArrayList();
    int n, k;

    public List<List<Integer>> subsets(int[] nums) {

        n = nums.length;
        //有全部包含的可能！
        for( k = 0 ; k < n + 1 ; k ++ ) {
            backtrack(0,new ArrayList<>(),nums);
        }
        return output;
    }


    //回溯
    private void backtrack(int first, ArrayList<Integer> curr, int[] nums) {
        //回溯结束条件
        if ( curr.size() == k) {
            output.add(new ArrayList<>(curr));
        }

        //遍历内容信息
        for( int i = first ; i < n ; i ++ ) {
            //加入元素信息
            curr.add(nums[i]);
            //试探性递归
            backtrack(i + 1 , curr,nums);

            // 归零
            curr.remove(curr.size() - 1);
        }
    }
}
