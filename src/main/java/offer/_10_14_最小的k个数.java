package offer;

/***
 * 面试题40. 最小的k个数
 * 输入整数数组 arr ，找出其中最小的 k 个数。例如，输入4、5、1、6、2、7、3、8这8个数字，则最小的4个数字是1、2、3、4。
 *
 *
 *
 * 示例 1：
 *
 * 输入：arr = [3,2,1], k = 2
 * 输出：[1,2] 或者 [2,1]
 * 示例 2：
 *
 * 输入：arr = [0,1,2,1], k = 1
 * 输出：[0]
 *
 *
 */

import java.util.Comparator;
import java.util.PriorityQueue;

/****
 * 　优先队列PriorityQueue是Queue接口的实现，可以对其中元素进行排序，
 *
 * 可以放基本数据类型的包装类（如：Integer，Long等）或自定义的类
 *
 * 对于基本数据类型的包装器类，优先队列中元素默认排列顺序是升序排列
 *
 * 但对于自己定义的类来说，需要自己定义比较器
 *
 * peek()//返回队首元素
 * poll()//返回队首元素，队首元素出队列
 * add()//添加元素
 * size()//返回队列元素个数
 * isEmpty()//判断队列是否为空，为空返回true,不空返回false
 * @2020-2-20
 */
public class _10_14_最小的k个数 {

    public int[] getLeastNumbers(int[] arr, int k) {

        PriorityQueue<Integer> queue = new PriorityQueue<>( new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        for ( int i : arr){
            queue.add(i);

            if ( queue.size() > k){
                queue.poll();
            }
        }

        int rs[] = new int [k];

        for ( int i = 0 ; i < k ; i ++ ){
            rs[i] = queue.poll();
        }



        return rs;
    }
}
