package algorithm.activeEverday;

/***
 * 字符串压缩。利用字符重复出现的次数，编写一种方法，实现基本的字符串压缩功能。比如，字符串aabcccccaaa会变为a2b1c5a3。若“压缩”后的字符串没有变短，则返回原先的字符串。你可以假设字符串中只包含大小写英文字母（a至z）。
 *
 * 示例1:
 *
 *  输入："aabcccccaaa"
 *  输出："a2b1c5a3"
 * 示例2:
 *
 *  输入："abbccd"
 *  输出："abbccd"
 *  解释："abbccd"压缩后为"a1b2c2d1"，比原字符串长度更长。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/compress-string-lcci
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-3-16
 *      解法：
 *          最直接的思路在于
 *              选区一个目标元素，之后进行比较操作如果后序元素同该元素相同，统计计数
 *     如果写成
 *     for (int i = 0 ; i < S.length() - 1 ) {
 *         int cn = 1 ;
 *         int j = i + 1;
 *         while ( S.charAt(i)  == S.chart(j) ){
 *             j ++ ;
 *             cn ++
 *         }
 *         //省略拼接代码操作
 *         i  = j
 *     }
 *
 *单独来看：思路貌似合理，但是有一个致命问题 ，在while中缺乏对j的判断，此时不加控制必然导致越界问题
 *     而如果加上对j的判断又会导致后续的部分元素无法统计
 *  所以应该改变思路：
 *      此题最直接的思路在于：
 *          首先记录一个字符，之后比较后序字符有几个同该字符相同，若相同则加入新的字符串中
 *      这种思路有两种方法实现，既然循环嵌套的方式难以实现，转换一种思路，新增设一个变量，来记录待比较的字符
 *
 *      //表名当前访问字符同待比较字符相同，则计数器cn++
 *      if( 当前字符 == pre ){
 *          cn ++ ;
 *      }else{
 *          //字符拼接至新的字符串中
 *          cn = 1;
 *          pre = S.charAt(i);//记录当前字符，作为新的待比较字符
 *      }
 *      //这样判断会失去最后一次的结果所以还需加上最后一次的结果信息
 *
 *      最后在判断条件中有一点明确指出：
 *          题目要求的条件在于：如果新的字符串长度小于待压缩字符串才返回，否则返回原字符串
 *
 */
public class _01_06_字符串压缩 {

    public String compressString(String S) {
        //处理特殊情况
        if ( S.length() == 0 || S == null) {
            return S;
        }
        StringBuilder sb = new StringBuilder();
        char pre = S.charAt(0);//记录前一个字符信息
        int cn = 1;//统计词频
        for ( int i = 1 ; i < S.length()  ; i ++ ) {
            if( pre == S.charAt(i)) {
                cn ++ ;
            }else{
                sb.append(pre);
                sb.append(cn);
                pre = S.charAt(i);
                cn = 1;
            }
        }
        //将最后一次的词拼接上去
        sb.append(pre).append(cn);

        if ( sb.length() < S.length()) {
            return  S;
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        _01_06_字符串压缩 d = new _01_06_字符串压缩();
        String str = d.compressString("abbccd");
        System.out.println(str);
    }
}
