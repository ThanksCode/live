package socket.chapter6.client;

import socket.chapter6.constants.UDPConstants;
import socket.chapter6.entry.ServerInfo;
import socket.chapter6.utils.ByteUtils;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * UDP 搜索者，用于搜索服务支持方
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 15:24
 **/
public class  UDPSearcher {

    // 客户端端口信息
    private static final int LISTEN_PORT = UDPConstants.PORT_CLIENT_RESPONSE;

    // 搜索服务器
    public static ServerInfo searchServer(int timeout) throws  Exception {
        System.out.println("UDPSearcher Started.");

        // 成功收到回送的栅栏
        CountDownLatch receiveLatch = new CountDownLatch(1);
        Listener listener = null;
        try {
            listener = listen(receiveLatch);
            sendBroadcast();
            receiveLatch.await(timeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 完成
        System.out.println("UDPSearcher Finished.");
        if (listener == null) {
            return null;
        }
        List<ServerInfo> devices = listener.getServerAndClose();
        if (devices.size() > 0) {
            return devices.get(0);
        }
        return null;
    }
    // 返回监听对象信息
    private static Listener listen(CountDownLatch receiveLatch) throws InterruptedException {
        System.out.println("UDPSearcher start listen.");
        // 构建一个用于监听是否启动的栅栏
        CountDownLatch startDownLatch = new CountDownLatch(1);
        Listener listener = new Listener(LISTEN_PORT,startDownLatch,receiveLatch);
        // 启动线程  由于处于异步调用，所以调用start后并不一定启动线程
        listener.start();
        // 让启动栅栏处于等待状态，处于无限等待情况 除非抛异常使得程序结束
        startDownLatch.await();
        return listener;
    }
    // 发送广播信息
    private static void sendBroadcast() throws Exception {
        System.out.println("UDPSearcher sendBroadcast started.");

        // 作为服务搜索方自动分配端口信息
        DatagramSocket ds = new DatagramSocket();

        // 构建一份请求数据
        ByteBuffer byteBuffer = ByteBuffer.allocate(128);
        // 头部信息
        byteBuffer.put(UDPConstants.HEADER);
        // 指令信息
        byteBuffer.putShort((short)1);
        // 端口信息  信息回送端口
        byteBuffer.putInt(LISTEN_PORT);
        // 构建发送信息
        // 构建发送信息的数据包信息  ----> byte数组 以及数据长度信息
        DatagramPacket requestPacke = new DatagramPacket(byteBuffer.array(),byteBuffer.position() + 1);
        // 设定地址信息  发送广播信息
        requestPacke.setAddress(InetAddress.getByName("255.255.255.255"));
        // 设定服务器接收端口信息
        requestPacke.setPort(UDPConstants.PORT_SERVER);
        // 发送信息
        ds.send(requestPacke);
        // 释放资源
        ds.close();
    }
    // 监听信息
    private static class Listener extends Thread{
        // 监听端口信息
        private final int listenPort;
        // 启动进程
        private final CountDownLatch startDownLatch;
        // 接收进程
        private final CountDownLatch receiveDownLatch;
        // 设备信息
        private final List<ServerInfo> serverInfoList = new ArrayList<>();
        // 缓冲数组
        private final byte[] buffer = new byte[128];
        // 信息公共长度信息
        private final int minLen = UDPConstants.HEADER.length + 2 + 4;
        private boolean done = false;
        private DatagramSocket ds = null;

        private Listener(int listenPort, CountDownLatch startDownLatch, CountDownLatch receiveDownLatch) {
            super();
            this.listenPort = listenPort;
            this.startDownLatch = startDownLatch;
            this.receiveDownLatch = receiveDownLatch;
        }

        @Override
        public void run() {
            super.run();
            // 启动栅栏
            startDownLatch.countDown();
            try{
                while( !done ) {

                    DatagramSocket ds = new DatagramSocket(listenPort);

                    // 构建一个数组信息
                    DatagramPacket receivePacket = new DatagramPacket(buffer,buffer.length);
                    // 接受数据包信息
                    ds.receive(receivePacket);
                    // 提取出ip port dataLen 等信息
                    String ip = receivePacket.getAddress().getHostAddress();
                    int port = receivePacket.getPort();
                    int dataLen = receivePacket.getLength();
                    byte [] data = receivePacket.getData();

                    // 校验信息是否合法
                    boolean isValid = dataLen >= minLen
                            && ByteUtils.startsWith(data,UDPConstants.HEADER);

                    System.out.println("UDPSearcher receive form ip:" + ip
                            + "\tport:" + port + "\tdataValid:" + isValid);

                    if ( !isValid) {
                        // 无效继续
                        continue;
                    }
                    // 规定 回送信息的cmd命令为2  1表示发送信息
                    ByteBuffer byteBuffer = ByteBuffer.wrap(buffer, UDPConstants.HEADER.length, dataLen);
                    final short cmd = byteBuffer.getShort();
                    final int serverPort = byteBuffer.getInt();
                    if (cmd != 2 || serverPort <= 0) {
                        System.out.println("UDPSearcher receive cmd:" + cmd + "\tserverPort:" + serverPort);
                        continue;
                    }

                    // 构建一个暗号信息
                    String sn = new String(buffer, minLen, dataLen - minLen);
                    ServerInfo info = new ServerInfo(serverPort, ip, sn);
                    serverInfoList.add(info);
                    // 成功接收到一份
                    receiveDownLatch.countDown();
                }
            }catch (Exception ignore){
                // 忽略异常信息
            }finally {
                // 释放资源
                close();
            }
        }

        public List<ServerInfo> getServerAndClose() {
            close();
            return serverInfoList;
        }

        private void close(){
            if ( ds != null ){
                ds.close();
                ds = null;
            }
        }
    }
}
