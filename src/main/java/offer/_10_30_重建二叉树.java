package offer;

/**
 * 输入某二叉树的前序遍历和中序遍历的结果，请重建该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
 *
 *  
 *
 * 例如，给出
 *
 * 前序遍历 preorder = [3,9,20,15,7]
 * 中序遍历 inorder = [9,3,15,20,7]
 * 返回如下的二叉树：
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/zhong-jian-er-cha-shu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-02 08:23
 **/
public class _10_30_重建二叉树 {

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if( preorder == null || inorder == null || preorder.length <= 0){
            return null;
        }
        return constructRoot(preorder,0,preorder.length - 1,inorder,0,inorder.length - 1);
    }

    private TreeNode constructRoot(int[] preorder, int pre, int endPre, int[] inorder, int startOrder, int endOrder) {

        if(pre > preorder.length - 1 || startOrder > endOrder){
            return null;
        }

        int target = preorder[pre];
        int find = 0;
        //找到根节点在中序序列中的位置
        for ( int i = startOrder ; i <= endOrder ; i ++ ){
            if ( inorder[i] == target) {
                find = i;
                break;
            }
        }
        int size = find - startOrder;
        TreeNode root = new TreeNode(target);
        root.left = constructRoot(preorder,pre + 1,pre + size,inorder,startOrder,find -1 );
        root.right = constructRoot(preorder,pre+size + 1 ,endPre,inorder,find + 1,endOrder);

        return root;

    }
}
