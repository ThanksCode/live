package mybatisDemo.基础模块.Mapper;




import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

/****
 * 注解获得是二维数组的原因
 * 得到的结果是一个二维数组.
 * 那么这个二维数组是怎么排列组合的呢?
 * 首先举个例子:
 *     @RedisScan
 *     public void save(@RedisSave()int id,@RedisSave()String name){
 *
 *     }
 *
 * 第一个参数下表为0,第二个为1
 *
 * 也就是说:annos[0][0] = RedisSave
 *                annos[1][0] = RedisSave
 * 也就是说,二维数组是包含多个仅有一个值的数组.
 *
 * 因为参数前可以添加多个注解,所以是二维数组,一个参数上不可以添加相同的注解,同一个注解
 * 可以加在不同的参数上!

 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-25 10:30
 **/
public class ParamResolver {
    private final SortedMap<Integer, String> names;

    private boolean hasParamAnnotation;

    public ParamResolver( Method method){
        final Class<?>[] paramTypes = method.getParameterTypes();
        final Annotation[][] paramAnnotations = method.getParameterAnnotations();
        final SortedMap<Integer, String> map = new TreeMap<Integer, String>();
        int paramCount = paramAnnotations.length;
        // get names from @Param annotations
        for (int paramIndex = 0; paramIndex < paramCount; paramIndex++) {

            String name = null;
            for (Annotation annotation : paramAnnotations[paramIndex]) {
                if (annotation instanceof Param) {
                    hasParamAnnotation = true;
                    name = ((Param) annotation).value();
                    break;
                }
            }
            if (name == null) {
                // @Param was not specified.
                if (name == null) {
                    // use the parameter index as the name ("0", "1", ...)
                    // gcode issue #71
                    name = String.valueOf(map.size());
                }
            }
            map.put(paramIndex, name);
        }
        names = Collections.unmodifiableSortedMap(map);
    }

    // 解析注解信息
    public ParamResolver(Class config , Method method){
        // 获取方法上参数的注解信息
        Annotation [][] annotations = method.getParameterAnnotations();
        // 创建map集合 保存位置信息
        final SortedMap<Integer, String> map = new TreeMap<Integer, String>();
        int paramCount = annotations.length;
        for ( int paramindex = 0 ; paramCount < paramCount ; paramindex ++ ) {
            // 一个参数上可能有多个注解信息，此时仅处理@Parame
            String name = null;
            for (Annotation annotation : annotations[paramindex]){
                if ( annotation instanceof Param){
                    // 获取注解上的信息
                    name = ((Param)annotation).value();
                    break;
                }
            }

            // 此时处理没有注解情况
            if ( name == null) {
                name = String.valueOf(map.size());
            }
            map.put(paramindex,name);
        }
        // 避免了随意的修改
        //unmodifiableSortedMap() 用于获取指定的SortedMap的不可修改视图。
        names = Collections.unmodifiableSortedMap(map);
    }





    public static void main(String[] args) throws NoSuchMethodException {
        MapperImpl mapper = new MapperImpl();
        Class<?> c = MapperImpl.class;
        ParamResolver paramResolver = null ;
        // 获取方法是 方法名，参数类型 int类型参数不能用Integer来获取
        Method method = c.getMethod("fun",int.class,int.class);
        paramResolver = new ParamResolver(c.getMethod("fun", int.class, int.class, int.class));


    }
}
