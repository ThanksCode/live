package patter.geek.虚拟钱包.贫血模型;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 14:59
 **/
public class VirtualWalletControlle {
    // 通过构造函数或者 IOC 框架注入
    private VirtualWalletService virtualWalletService;
    public BigDecimal getBalance(Long walletId) { return null; } // 查询余额
    public void debit(Long walletId, BigDecimal amount) {  } // 出账
    public void credit(Long walletId, BigDecimal amount) {  } // 入账
    public void transfer(Long fromWalletId, Long toWalletId, BigDecimal amount) { }
}
