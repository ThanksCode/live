package mybatisDemo;

/***
 * MD5加密工具类
 */

import mybatisAnnioation.utils.PropertiesUtil;

import java.security.MessageDigest;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-22 17:58
 **/
public class MD5Util {

    private static final String hexDigits[] = {"0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
    /**
     *  返回加密后的信息内容string
     * @param origin
     * @return
     */
    public static String MD5EncodeUtf8(String origin) {
        // 获取后缀信息
        origin = origin + PropertiesUtil.getProperty("password.salt", "");
        return MD5Encode(origin, "utf-8");
    }

    private static String MD5Encode(String origin, String charsetname) {
        String resultString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            if(charsetname == null || "".equals(charsetname)){
                // 默认编码信息格式处理  默认格式为utf-8编码处理
                resultString = byteArrayToHexString(md.digest(origin.getBytes()));
            }else{
                // 主要为了应该不同信息的编码
                resultString = byteArrayToHexString(md.digest(origin.getBytes(charsetname)));
            }
        }catch (Exception exception){
            throw new RuntimeException("无法获取加密算法");
        }
        // 全部转成大写
        return resultString.toUpperCase();
    }

    private static String byteArrayToHexString(byte[] digest) {
        StringBuilder resultString = new StringBuilder();
        for( int i = 0 ; i <digest.length;  i ++ ) {
            resultString.append(byteToHexString(digest[i]));
        }
        return resultString.toString();
    }

    /***
     * 对单个字符进行加密处理
     * @param b
     * @return
     */
    private static String byteToHexString(byte b) {

        int  n = b;
        // 注意：此时进行映射时，数据会转成byte形式  byte数据范围在 -128 - 127
        // 由于对16取模，加256后变成整数，并不影响数据
        if ( n < 0) {
            n += 256;
        }
        // 获取字符下标信息
        int d1 = n / 16;
        int d2 = n % 16;

        //返回信息
        return hexDigits[d1] + hexDigits[d2];
    }

}