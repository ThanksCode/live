package offer;
/***
 * 剑指 Offer 25. 合并两个排序的链表
 * 输入两个递增排序的链表，合并这两个链表并使新链表中的节点仍然是递增排序的。
 *
 * 示例1：
 *
 * 输入：1->2->4, 1->3->4
 * 输出：1->1->2->3->4->4
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-10 13:59
 **/
public class _10_35_合并两个排序的链表 {


    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 非法数据处理
        if ( l1 == null || l2 == null) {
            return null;
        }

        // 带有虚拟头结点的链表
        ListNode head = new ListNode(-1);
        //head.next = null;
        // cur记录当前结点信息
        ListNode cur = head;

        while( l1 != null
                && l2 != null ){
            if ( l1.val >= l2.val){
                cur.next = l2;
                l2 = l2.next;
            }else{
                cur.next = l1;
                l1 = l1.next;
            }
            // 关键  当前指针后移
            cur = cur.next;
        }

        cur.next = l1 == null ? l2 : l1;
        return head.next;

    }
}
