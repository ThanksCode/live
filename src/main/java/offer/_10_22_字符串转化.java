package offer;

/**
 * 写一个函数 StrToInt，实现把字符串转换成整数这个功能。不能使用 atoi 或者其他类似的库函数。
 *
 *  
 *
 * 首先，该函数会根据需要丢弃无用的开头空格字符，直到寻找到第一个非空格的字符为止。
 *
 * 当我们寻找到的第一个非空字符为正或者负号时，则将该符号与之后面尽可能多的连续数字组合起来，作为该整数的正负号；假如第一个非空字符是数字，则直接将其与之后连续的数字字符组合起来，形成整数。
 *
 * 该字符串除了有效的整数部分之后也可能会存在多余的字符，这些字符可以被忽略，它们对于函数不应该造成影响。
 *
 * 注意：假如该字符串中的第一个非空格字符不是一个有效整数字符、字符串为空或字符串仅包含空白字符时，则你的函数不需要进行转换。
 *
 * 在任何情况下，若函数不能进行有效的转换时，请返回 0。
 *
 * 说明：
 *
 * 假设我们的环境只能存储 32 位大小的有符号整数，那么其数值范围为 [−231,  231 − 1]。如果数值超过这个范围，请返回  INT_MAX (231 − 1) 或 INT_MIN (−231) 。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ba-zi-fu-chuan-zhuan-huan-cheng-zheng-shu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @date 2020-3-2
 *
 * 题意分析：
 *      给定一个字符串信息，要求将字符串信息中的数字字符返回成一个整数数据
 *
 *     处理逻辑：
 *         1.去掉所给字符串中多余的空格内容信息
 *         2.截取字符串首字符进行判断，判断其是否为 ‘+’ ， “-” ， “0,1,2……9”之外的数据
 *         3.如果不是上述以外的数据，则进行判断处理，判断其具体的类型
 *                 目的：确保数据的正负情况
 *         4.遍历字符串信息，直到遇到第一个非数字字符终止
 *         5.进行数据转化，统计数值的时候使用long类型数据进行保存
 *             注意一点：在累加的时候可能也会超过long类型数据的数据范围，所以在循环的时候需要加入判断
 *         6.根据标志符号位进行数据的返回
 */
public class _10_22_字符串转化 {

    public int strToInt(String str) {
        //首先去掉开头结尾的多余空格
        str = str.trim();
        int len = str.length();
        StringBuilder bl = new StringBuilder();
        //说明删除空格后是一个空串
        if ( len == 0){
            return 0;
        }
 //假如该字符串中的第一个非空格字符不是一个有效整数字符、字符串为空或字符串仅包含空白字符时
        // 则你的函数不需要进行转换。

        char c = str.charAt(0);
        if ( (c != '+' && c != '-') && !Character.isDigit(c)){
            return 0;
        }

        //表明第一个字符为数字或符号 随后进行判断
        int  flag = 1;
        if ( c == '+'){
            flag = 1;
        }else if ( c == '-'){
            flag = -1;
        }else{
            bl.append(c);
        }

        //从1开始往后截取
        str = str.substring(1);

        //读入后序字符串信息。读到非数字结束
        for( int i = 0; i < str.length() ; i ++) {
            //遇到非数字信息
            if ( !Character.isDigit(str.charAt(i))){
                break;
            }else{
                bl.append(str. charAt(i));
            }
        }

        //System.out.println(bl.toString());
        long sum = 0;
        int rs = 0;
        for ( int i = 0 ; i < bl.length() ; i ++ ){
                sum = sum * 10 + (bl.charAt(i) - '0');
                //累加时可能会超过long类型最大值
                if( sum  > Integer.MAX_VALUE){
                    return flag == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                }
        }

        if ( (sum * flag) > Integer.MAX_VALUE ) {
            rs = Integer.MAX_VALUE;
        }else if ( (sum * flag) < Integer.MIN_VALUE){
            rs = Integer.MIN_VALUE;
        }else{
            rs = (int) (sum * flag);
        }

        return rs;

    }

    public static void main(String[] args) {
        _10_22_字符串转化 obj = new _10_22_字符串转化();

        int tmp = obj.strToInt("-42");
        System.out.println(tmp);
    }
}
