package patter.geek.openClose;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 16:39
 **/
public class NotificationEmergencyLevel {

    // 严重 紧急 普通 无关紧要
    public static final int SEVERE = 1;
    public static final int URGENCY  = 2;
    public static final int NORMAL = 3 ;
    public static final int TRIVIAL = 4;


    public void notify(int level,String txt) {
        System.out.println(" 异常信息 ：   "+ txt);
    }

}
