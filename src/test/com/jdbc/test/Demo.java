package com.jdbc.test;

import com.jdbc.dao.JDBCUtils;
import com.jdbc.dao.Query;
import com.jdbc.dao.User;
import com.web.strategy.ObjectChangeToByte;
import com.web.strategy.UserController;
import javassist.ByteArrayClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Connection;

/**
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-09 18:02
 **/
public class Demo {
    //@Test
    public void fun (){
        Connection con = JDBCUtils.getConnection();
        System.out.println(con);
    }

    //@Test
    public void testReflect () throws IllegalAccessException, InstantiationException {
        Class clazz = User.class;
       Field fields [] = clazz.getDeclaredFields();
        for(Field f : fields) {
            System.out.println(f.getName());
            System.out.println(f.getGenericType());
        }
       Object obj = clazz.newInstance();
        System.out.println(obj);
    }

   // @Test
    public void testQuery(){
        Connection con = JDBCUtils.getConnection();
        String sql = "select * from user where id = 42";
        Query query = new Query();
        User user = (User)Query.query(sql,con,User.class);
        System.out.println(user);

    }

    @Test
    public void testParams() throws Exception{
        Class clazz = UserController.class;
        //获取类本身的方法，不获取继承而来的方法
        Method [] methods = clazz.getDeclaredMethods();
        ClassPool pool = ClassPool.getDefault();
      // UserController controller = new UserController();
        CtClass ctClass = pool.get(clazz.getName());
        for (int i = 0 ; i < methods.length ; i ++ ) {
            //表示不同的处理

            System.out.println(methods[i].getName());
            String methodName = methods[i].getName();
            try{
                CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
                // 使用javaassist的反射方法获取方法的参数名
                MethodInfo methodInfo = ctMethod.getMethodInfo();
                CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
                LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
                if ( attr == null) {
                    //exception
                }
                String [] paramNames = new String[ctMethod.getParameterTypes().length];
                int pos = Modifier.isStatic(ctMethod.getModifiers()) ? 0 : 1;
                for (int j = 0; j < paramNames.length; j++){
                    paramNames[j] = attr.variableName(j + pos);
                }
                // paramNames即参数名
                for (int j = 0; j < paramNames.length; j++) {
                    System.out.println(paramNames[j]);
                }

                }catch (Exception e ) {

                }

        }

    }

    @Test
    public void fun1(){
        Class clazz = UserController.class;
        Method [] methods = clazz.getDeclaredMethods();
        for (Method m: methods
             ) {
            System.out.println(m.getName());
            System.out.println(m.getParameterTypes());
        }
    }
}
