package com.com.并发;
/***
 * code 1
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-29 11:34
 **/
public class LifeOff implements  Runnable{


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    protected int countDown = 10;
    private static int taskCount = 0;
    private final int id = taskCount ++ ;
    public String status(){
        return "#" + id +"(" +
                (countDown > 0 ? countDown : "lifeoff")+ ")";
    }



    @Override
    public void run() {
        while ( countDown -- > 0 ){
            System.out.println(status());
            Thread.yield();
        }
    }

    public static void main(String[] args) {
        LifeOff lifeOff  = new LifeOff();
        lifeOff.run();
        String dem = "1";
        short s1 = 1;
        s1 += 1;
        System.out.println(Math.round(-11.5));
    }

}
