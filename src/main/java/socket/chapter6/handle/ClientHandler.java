package socket.chapter6.handle;

import socket.chapter6.utils.CloseUtils;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-06 11:17
 **/
public class ClientHandler {
    // 连接对象
    private Socket socket;
    // 处理客户端读任务
    private final ClientReadHandler readHandler ;
    // 处理客户端写任务
    private final ClientWriteHandler writeHandler ;
    // 判断是否自身因为无法读取数据而终止退出
    private final ClientHandlerCallback clientHandlerCallback ;
    // 自身信息
    private final String clientInfo;

    public ClientHandler(Socket socket, ClientHandlerCallback clientHandlerCallback) throws IOException {
        this.socket = socket;
        this.readHandler = new ClientReadHandler(socket.getInputStream());
        this.writeHandler = new ClientWriteHandler(socket.getOutputStream());
        this.clientHandlerCallback = clientHandlerCallback;
        this.clientInfo = "A[" + socket.getInetAddress().getHostAddress()
                + "] P[" + socket.getPort() + "]";
        System.out.println("新客户端连接：" + socket.getInetAddress() +
                " P:" + socket.getPort());
    }

    // 发送数据信息
    public void send(String str) {
        // 如果写成匿名内部类的形式 此时传入参数的形式 必须是final修饰
        // 这样才能保证数据能在匿名内部类中使用
        writeHandler.send(str);
    }

    // 读取客户端信息并回显至服务器端
    public void readToPrint() {
        readHandler.start();
    }
    // 退出连接
    public void exit() {
        readHandler.exit();
        writeHandler.exit();
        CloseUtils.close(socket);
        System.out.println("客户端已退出：" + socket.getInetAddress() +
                " P:" + socket.getPort());
    }
    // 由于自身异常而导致结束
    private void exitBySelf() {
        exit();
        clientHandlerCallback.onSelfClosed(this);
    }

    public String getClientInfo() {
        return clientInfo;
    }

    /***
     * 仅处理读任务，负责从客户端读取信息
     */
    class ClientReadHandler  extends Thread{
        // 输入结束标识符
        private boolean flag = false;
        // 输入流
        private final InputStream inputStream;

        // 构建时需要传入输入流对象信息
        ClientReadHandler(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            super.run();
            System.out.println("新客户端连接：" + socket.getInetAddress() +
                    " P:" + socket.getPort());
            try {
                // 得到输入流，用于接收数据
                BufferedReader socketInput = new BufferedReader(new InputStreamReader(
                        inputStream));
                do {
                    // 客户端拿到一条数据
                    String str = socketInput.readLine();
                    if( str == null ) {
                        System.out.println("无法从客户端读取信息！！！！");
                        // 退出当前客户端
                        ClientHandler.this.exitBySelf();
                        break;
                    }
                    // 在屏幕上展示数据信息
                    //System.out.println(str);
                    // 将信息进行转发处理  通知到TCPServer  要避免阻塞 ---》 异步实现 --- 》 借助线程池
                    // 如果发生阻塞将无法获取到读入的信息内容  一定要避免阻塞的发生，所以采用异步方式进行实现
                    clientHandlerCallback.onNewMessageArrived(ClientHandler.this,str);
                } while (!flag);
            } catch (Exception e) {
                if (!flag) {
                    System.out.println("连接异常断开");
                    ClientHandler.this.exitBySelf();
                }
            } finally {
                // 关闭输入流
                CloseUtils.close(inputStream);
            }

            System.out.println("客户端已退出：" + socket.getInetAddress() +
                    " P:" + socket.getPort());

        }

        // 结束输入流的监听
        void exit() {
            flag = true;
            CloseUtils.close(inputStream);
        }
    }


    class ClientWriteHandler {
        private boolean done = false;
        // 输出流
        private final PrintStream printStream;
        // 构建一个线程池方便线程间通信  将线程间复杂的通信交给线程池管理
        private final ExecutorService executorService;

        ClientWriteHandler(OutputStream outputStream) {
            this.printStream = new PrintStream(outputStream);
            this.executorService = Executors.newSingleThreadExecutor();
        }

        private void exit() {
            done = true;
            CloseUtils.close(printStream);
            executorService.shutdownNow();
        }
        // 重点！！ 如果此处使用匿名内部类 final String str 时才能被内部类所使用
        private void send(String str) {
            // 客户端非关闭的情况下才会发送数据信息
            if ( !done ){
                executorService.execute(new WriteRunnable(str));
            }
        }


        class WriteRunnable implements Runnable {
            private final String msg;
            WriteRunnable(String msg) {
                this.msg = msg;
            }
            @Override
            public void run() {
                if (ClientWriteHandler.this.done) {
                    return;
                }
                try{
                    // 回送信息
                    ClientWriteHandler.this.printStream.println(msg);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }


}
