package offer;

/***
 * 给定一个数字，我们按照如下规则把它翻译为字符串：0 翻译成 “a” ，1 翻译成 “b”，……，11 翻译成 “l”，……，25 翻译成 “z”。一个数字可能有多个翻译。请编程实现一个函数，用来计算一个数字有多少种不同的翻译方法。
 *
 *  
 *
 * 示例 1:
 *
 * 输入: 12258
 * 输出: 5
 * 解释: 12258有5种不同的翻译，分别是"bccfi", "bwfi", "bczi", "mcfi"和"mzi"
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ba-shu-zi-fan-yi-cheng-zi-fu-chuan-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 21:22
 **/

/***
 * 数字的组合排列情况
 * 1 12 /2 22
 * 递归思路：
 *  如果当前数字和他下一位数字之和小于26算一种情况
 */
public class _10_28_把数字翻译成字符串 {

    private int count = 0 ;

    public int translateNumV1(int num) {
        String str = String.valueOf(num);
        dfs(str, 0);
        return count;
    }

    private void dfs ( String str , int i ) {
        if ( i >= str.length() - 1 ) {
            count ++ ;
            return ;
        }
        dfs(str,i + 1);
        int tmp  = (str.charAt(i) - '0') * 10 + ( str.charAt(i+1) ) - '0';
        if ( str.charAt(i) > '0' && i + 1 < str.length() && tmp < 26) {
            dfs(str, i + 2) ;
        }

    }

    // v2
    private int total = 0;
    public int translateNum(int num){
        String str = String.valueOf(num);
        backTrace(str,0);
        return total;
    }

    private void backTrace(String str, int i){
        // 结束条件
        if ( i >= str.length() - 1){ // i == str.length()
            total ++ ;
            return;
        }
        // 递归调用
        backTrace(str,i + 1);
        // 校验是否可以调用 backTrace(i + 2 )
        int cur = (str.charAt(i) - '0' )* 10 + str.charAt(i+1) - '0';
        // 条件信息有待优化  越界 0x这种情况 超过26是那种边界条件

        if( cur >= 10 && cur < 26 && i + 1 < str.length() ){
            backTrace(str,i+2);
        }

        /*if ( cur < 26 ) {
            dfs(str,i + 2);
        }*/

    }
}
