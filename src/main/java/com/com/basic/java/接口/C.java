package com.com.basic.java.接口;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-11 09:30
 **/
public class C implements I1,I2 {

    @Override
    public int fun(int i) {
        System.out.println("int fun ");
        return 0;
    }

    @Override
    public void fun() {
        System.out.println("void");
    }

}
