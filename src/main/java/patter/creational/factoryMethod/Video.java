package patter.creational.factoryMethod;

public abstract class Video {
    public abstract void produce();
}
