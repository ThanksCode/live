package com.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * 查询方法
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-09 18:30
 **/
public class Query  {


    public static Object query(String sql , Connection connection,Class clazz) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        RowHandler rowHandler = null;
        Object obj = new Object();
        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            rowHandler = new RowHandler(clazz);
            obj = rowHandler.map(rs);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
