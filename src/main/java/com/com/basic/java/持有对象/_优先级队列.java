package com.com.basic.java.持有对象;

import java.util.*;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-18 17:34
 **/
public class _优先级队列 {

    public static void main(String[] args) {
        //PriorityQueue priorityQueue = new PriorityQueue<Integer>( Collections.reverseOrder());
        PriorityQueue priorityQueue = new PriorityQueue<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        Random random = new Random();
        ArrayList arr = new ArrayList();
        for( int i = 0 ; i < 10 ; i ++ ) {
            int target = random.nextInt(20);
            priorityQueue.add(target);
            arr.add(target);
        }

        ArrayList<Integer> arrayList = new ArrayList<>();
        while ( !priorityQueue.isEmpty()) {
            int targer = (int) priorityQueue.poll();
            arrayList.add(targer);
        }

        System.out.println("入队信息 " + Arrays.toString(arr.toArray()) +
        "出队信息  " + Arrays.toString(arrayList.toArray()) );
        Collections.shuffle(arr);
        System.out.println("入队信息 信息被打乱 " + Arrays.toString(arr.toArray()));


    }




}
