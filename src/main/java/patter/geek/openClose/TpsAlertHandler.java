package patter.geek.openClose;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 16:57
 **/
public class TpsAlertHandler extends AbstractAlertHandler{

    // 加入不调用父类构造器 默认空参构造器能否构建一个对象？ 不行！  缺少构造器，无法通过编译处理
    public TpsAlertHandler(AlertRule rule , NotificationEmergencyLevel notificationEmergencyLevel){
        super(rule,notificationEmergencyLevel);
    }

    @Override
    public void check(AlertInof alertInof) {
        notificationEmergencyLevel.notify(NotificationEmergencyLevel.URGENCY, "tps");
        System.out.println("处理 tps 异常请求信息");
    }
}
