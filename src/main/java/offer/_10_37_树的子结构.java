package offer;

/***
 * 输入两棵二叉树A和B，判断B是不是A的子结构。(约定空树不是任意一个树的子结构)
 *
 * B是A的子结构， 即 A中有出现和B相同的结构和节点值。
 *
 * 例如:
 * 给定的树 A:
 *
 *      3
 *     / \
 *    4   5
 *   / \
 *  1   2
 * 给定的树 B：
 *
 *    4 
 *   /
 *  1
 * 返回 true，因为 B 与 A 的一个子树拥有相同的结构和节点值。
 *
 * 示例 1：
 *
 * 输入：A = [1,2,3], B = [3,1]
 * 输出：false
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/shu-de-zi-jie-gou-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/***
 * 输入:
 * [4,2,3,4,5,6,7,8,9]
 * [4,8,9]
 * 输出
 * false
 * 预期结果
 * true
 */
/***
 * 本题解题思路：递归
 * 子树为空 ---> true;
 * 主树为空 --->  false
 *
 * 递归条件：
 *  先判断当前结点是否同子树当前结点相同
 *     当前结点相同的情况下 ---> 递归校验左子树 右子树情况  checkSubStruct进行校验
 *  否则:
 *     可能主树的左子树  或者   右子树 存在子结构信息  ----> 此时属于递归操作
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-11 18:02
 **/
public class _10_37_树的子结构 {
    // 思路：递归操作
    public boolean isSubStructure(TreeNode A, TreeNode B) {
        // B结点为Null情况
        if( B == null) {
            return true;
        }

        // 此时主树为null 必然不符合条件
        if( A == null) {
            return false;
        }
        // 默认为true;
       /* boolean result = true;
        // 此时根节点的值同B结点值相同
        if ( A.val == B.val) {
            result = checkSubStructure(A,B);
        }

        // 如果不符合则 验证左子树情况
        if ( result == false ) {
            result = checkSubStructure(A.left,B.left);
        }

        if ( result == false) {
            result = checkSubStructure(A.right,B.right);
        }
        return result;

    return checkSubStructure(A,B)
               || checkSubStructure(A.left,B)
               || checkSubStructure(A.right,B);


        */
       return checkSubStructure(A,B)
               || isSubStructure(A.left,B)
               || isSubStructure(A.right,B);
    }

    // 判断是否符合子结构信息
    private boolean checkSubStructure(TreeNode a, TreeNode b) {
        if ( b  == null) {
            return true;
        }

        if ( a == null ) {
            return false;
        }

        if ( a.val != b.val) {
            return false;
        }

        // 递归处理
        return checkSubStructure(a.left,b.left)
                && checkSubStructure(a.right,b.right);
    }


}
