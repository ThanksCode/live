package mybatisDemo.基础模块.类型转换;
/***
 * 主要工作：
 * 1.
 */

import java.lang.reflect.Type;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 09:42
 **/
public class TypeHandlerRegister {
    private final Map<JDBCType, TypeHandler<?>> JDBC_TYPE_HANDLER_MAP = new EnumMap<JDBCType, TypeHandler<?>>(JDBCType.class);
    private final Map<Type, Map<JDBCType, TypeHandler<?>>> TYPE_HANDLER_MAP = new ConcurrentHashMap<Type, Map<JDBCType, TypeHandler<?>>>();
    private final Map<Class<?>, TypeHandler<?>> ALL_TYPE_HANDLERS_MAP = new HashMap<Class<?>, TypeHandler<?>>();

    private static final Map<JDBCType, TypeHandler<?>> NULL_TYPE_HANDLER_MAP = new HashMap<JDBCType, TypeHandler<?>>();



    // 核心注入方法
    private void register(Type javaType, JDBCType jdbcType, TypeHandler<?> handler) {
        // java -> jdbcType
        if( javaType != null){
            // 获取到所有java所对应的类型信息
            Map<JDBCType,TypeHandler<?>> map = TYPE_HANDLER_MAP.get(javaType);
            if( map == null) {
                map = new HashMap<JDBCType,TypeHandler<?>>();
            }
            map.put(jdbcType,handler);
            TYPE_HANDLER_MAP.put(javaType,map);
        }
        // 类型加入All Type映射
        ALL_TYPE_HANDLERS_MAP.put(javaType.getClass(),handler);
    }
}
