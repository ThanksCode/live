package patter.creational.simpleFactory;

public interface Video {

    void produce();
}
