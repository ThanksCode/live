package socket.chapter5.CountDownLatchDemo;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-03 11:49
 **/
public class CountDownLatchDemo {

    public static void main(String[] args) {
        // 线程池
        ExecutorService executor = Executors.newCachedThreadPool();

        CountDownLatch latch = new CountDownLatch(3);

        Worker w1 = new Worker(latch,"张三");
        Worker w2 = new Worker(latch,"李四");
        Worker w3 = new Worker(latch,"王二");

        Boss boss = new Boss(latch);

        executor.execute(w3);
        executor.execute(w2);
        executor.execute(w1);
        executor.execute((Runnable) boss);
        // 关闭线程池
        executor.shutdown();
    }
}
