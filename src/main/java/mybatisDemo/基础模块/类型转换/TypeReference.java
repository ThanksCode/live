package mybatisDemo.基础模块.类型转换;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


/***
 * ParameterizedType详解
 * 参数化类型
 * public interface ParameterizedType extends Type {
 *     Type[] getActualTypeArguments();
 *
 *     Type getRawType();
 *
 *     Type getOwnerType();
 * }
        具有<>符号的变量是参数化类型

 getActualTypeArguments()返回表示此类型实际类型参数的 Type 对象的数组。
 [0]就是这个数组中第一个了，简而言之就是获得超类的泛型参数的实际类型。
 本例中即为获得T的type。

 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 10:15
 **/
public abstract class TypeReference<T> {
    private final Type rawType;

    protected TypeReference() {
        rawType = getSuperclassTypeParameter(getClass());
    }

    Type getSuperclassTypeParameter(Class<?> clazz) {
        Type genericSuperclass = clazz.getGenericSuperclass();
        if (genericSuperclass instanceof Class) {
            // try to climb up the hierarchy until meet something useful
            if (TypeReference.class != genericSuperclass) {
                return getSuperclassTypeParameter(clazz.getSuperclass());
            }

            throw new RuntimeException("'" + getClass() + "' extends TypeReference but misses the type parameter. "
                    + "Remove the extension or add a type parameter to it.");
        }

        Type rawType = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
        // TODO remove this when Reflector is fixed to return Types
        if (rawType instanceof ParameterizedType) {
            rawType = ((ParameterizedType) rawType).getRawType();
        }

        return rawType;
    }

    public final Type getRawType() {
        return rawType;
    }

    @Override
    public String toString() {
        return rawType.toString();
    }

}
