package socket.远程系统.client;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 11:34
 **/

public class ShutWork extends Thread{

    private Client client;
    public ShutWork(Client client) {
        this.client=client;
    }
    @Override
    public void run() {
        client.close();
    }
}