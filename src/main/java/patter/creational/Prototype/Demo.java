package patter.creational.Prototype;


/**
 * 校验深拷贝，浅拷贝！！！！
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-08 21:42
 **/
public class Demo {

    public static void main(String[] args) throws CloneNotSupportedException {
        Job job = new Job("sxdt");
        User u1 = new User("ZhangSan" , "12",job);
        User u2 = (User)u1.clone();
        System.out.println("hello");

    }
}
