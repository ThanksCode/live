package offer;

import com.jdbc.dao.Query;

import java.util.*;

/**
 * 剑指 Offer 32 - I. 从上到下打印二叉树
 * 从上到下打印出二叉树的每个节点，同一层的节点按照从左到右的顺序打印。
 *
 *
 *
 * 例如:
 * 给定二叉树: [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * 返回：
 *
 * [3,9,20,15,7]
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-30 22:41
 **/
public class _10_32_从上到下打印二叉树 {

    public int[] levelOrder(TreeNode root) {
        List<Integer> list = new ArrayList<>();

        if ( root == null) {
            return list.stream().mapToInt(Integer::intValue).toArray();
        }
        Queue queue = new LinkedList();

        queue.add(root);
        while ( !queue.isEmpty()){
            // 记录队列当前长度信息
            int count = queue.size();
            for ( int i = 0 ; i < count ; i ++ ) {
                TreeNode cur = (TreeNode) queue.poll();
                list.add(cur.val);
                if ( cur.left != null) {
                    queue.add(cur.left);
                }

                if ( cur.right != null ) {
                    queue.add(cur.right);
                }
            }
        }
        // 注意此处list ---- > int [] 数组的转变
        return list.stream().mapToInt(Integer::intValue).toArray();
    }
}
