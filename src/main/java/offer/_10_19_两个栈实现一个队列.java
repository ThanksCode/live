package offer;


import java.util.Stack;

/***
 * 用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead ，分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead 操作返回 -1 )
 *
 *  
 *
 * 示例 1：
 *
 * 输入：
 * ["CQueue","appendTail","deleteHead","deleteHead"]
 * [[],[3],[],[]]
 * 输出：[null,null,3,-1]
 * 示例 2：
 *
 * 输入：
 * ["CQueue","deleteHead","appendTail","appendTail","deleteHead","deleteHead"]
 * [[],[],[5],[2],[],[]]
 * 输出：[null,-1,null,null,5,2]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @date 2020-2-24
 *
 * 解题思路：
 *      栈具有先进后出的特性，当元素要出栈是将s1中元素出栈，放入到s2中这样当出队是从s2中弹出元素
 *
 */

//10_19_两个栈实现一个队列
public class  _10_19_两个栈实现一个队列 {

//   声明两个栈
    private Stack<Integer> s1 = null;
    private Stack<Integer> s2 = null;


    public void CQueue() {
        s1 = new Stack<Integer>();
        s2 = new Stack<Integer>();
    }

    public void appendTail(int value) {
//    入队
        s1.push(value);
    }

    public int deleteHead() {

        if ( !s2.empty()){
            return s2.pop();
        }
//        将s1中元素全部入栈s2
        while ( !s1.empty()){
            s2.push(s1.pop());
        }

//        当s1中元素都入栈s2后，假若s2为空则整个过程中没有元素在s1中存放
       if ( s2.empty()){
            return -1;
       }

       return s2.pop();
    }

}
