package algorithm.activeEverday;

import java.util.HashMap;

/***
 * 给你一份『词汇表』（字符串数组） words 和一张『字母表』（字符串） chars。
 *
 * 假如你可以用 chars 中的『字母』（字符）拼写出 words 中的某个『单词』（字符串），那么我们就认为你掌握了这个单词。
 *
 * 注意：每次拼写时，chars 中的每个字母都只能用一次。
 *
 * 返回词汇表 words 中你掌握的所有单词的 长度之和。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：words = ["cat","bt","hat","tree"], chars = "atach"
 * 输出：6
 * 解释：
 * 可以形成字符串 "cat" 和 "hat"，所以答案是 3 + 3 = 6。
 * 示例 2：
 *
 * 输入：words = ["hello","world","leetcode"], chars = "welldonehoneyr"
 * 输出：10
 * 解释：
 * 可以形成字符串 "hello" 和 "world"，所以答案是 5 + 5 = 10。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/find-words-that-can-be-formed-by-characters
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-3-17
 */
public class _1160_拼写单词 {

    public int countCharacters(String[] words, String chars) {
        int cn = 0;
        HashMap<Character ,Integer> map = new HashMap<>();
        //统计字符
        for (char c : chars.toCharArray()) {
            map.put(c,map.getOrDefault(c,0) + 1);
        }
        //遍历字符数组信息
        for(String s : words ) {
            //统计当前单词的字符词频
            HashMap<Character,Integer> tempMap = new HashMap<>();
            for(char c:s.toCharArray()){
                tempMap.put(c,tempMap.getOrDefault(c,0)+1);
            }
            boolean flag = true;
            for( char c : s.toCharArray()) {
                if ( !map.containsKey(c) ) {
                    flag = false;
                    break;
                }

                if ( map.get(c) < tempMap.get(c)) {
                    flag = false;
                    break;
                }
            }

            if ( flag) {
                cn += s.length();
            }
        }
        return cn;
    }
}
