package offer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
/***
 *  典型的全排列问题
 *    在未使用集合前 使用数组保存内容
 *    遇到aab 这样的数组组合时就会出现重复现象
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-21 14:37
 **/
public class _10_45_字符串的排列 {

    private Set<String> rs = new HashSet<>();

    public String[] permutation(String s) {
        char [] chars = s.toCharArray();

        backTracting(new StringBuilder(s),0,s.length());
        return rs.toArray(new String[rs.size()]);
    }

    private void backTracting(StringBuilder str,int s , int len){
        if ( s == len) {
            StringBuilder tmp = new StringBuilder(str);
            rs.add(tmp.toString());
        }
        for ( int i = s ; i < len ; i ++ ) {
            swap(str,s,i);
            backTracting(str,s + 1,len);
            // 便于回溯处理
            swap(str,s,i);
        }
    }


    private void swap(StringBuilder chars , int i , int j ) {
        char tmp = chars.charAt(i);
        chars.setCharAt(i,chars.charAt(j));
        chars.setCharAt(j,tmp);
    }

    public static void main(String[] args) {
        _10_45_字符串的排列  obj = new _10_45_字符串的排列();
        obj.backTracting(new StringBuilder("abc"),0,3);
    }
}
