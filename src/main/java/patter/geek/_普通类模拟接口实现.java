package patter.geek;

/***
 * 必须实现指定方法
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-28 15:31
 **/
class MethodUnSupportedException extends Exception{
    public MethodUnSupportedException(){
        super();
    }
}
class Father {
    public void fun() throws MethodUnSupportedException {
        throw new MethodUnSupportedException();
    }
}
class Son extends Father {

}

public class _普通类模拟接口实现 {
    public static void main(String[] args) throws MethodUnSupportedException {
        Son son = new Son();
        son.fun();
    }

}
