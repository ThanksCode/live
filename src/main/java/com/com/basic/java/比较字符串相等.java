package com.com.basic.java;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-27 14:47
 **/
public class 比较字符串相等 {
    /***
     * 第四个 s6.intern() 首先获取了 s6 的 HelloWorld 字符串，然后在字符串池中查找该字符串，
     * 找到了 s1 的 HelloWorld 并返回。这里如果事前字符串池中没有 HelloWorld 字符串，
     * 那么还是会在字符串池中创建一个 HelloWorld 字符串再返回。
     * 总之返回的不是堆中的 s6 那个字符串。
     *
     * @param args
     */
    public static void main(String[] args) {


            String s1 = "HelloWorld";
            String s2 = new String("HelloWorld");
            String s3 = "Hello";
            String s4 = "World";
            String s5 = "Hello" + "World";
            //String s6 = s3 + s4; // 相当于 new String(s3+s4)
            // 此时s1 ,s6便相等
            String s6 = (s3 + s4).intern();
            // false
            System.out.println(s1 == s2);
            // ？true
            System.out.println(s1 == s5);
            // false
            System.out.println(s1 == s6);
            // true
            System.out.println(s1 == s6.intern());
            // false
            System.out.println(s2 == s2.intern());


    }
}
