package mybatisDemo.基础模块.反射;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-17 22:11
 **/
public class User {

    private String name;
    private String password;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
