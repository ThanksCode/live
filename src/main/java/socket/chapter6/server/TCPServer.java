package socket.chapter6.server;

import socket.chapter6.handle.ClientHandler;
import socket.chapter6.handle.ClientHandlerCallback;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 17:07
 **/
public class TCPServer implements ClientHandlerCallback {
    // 端口信息
    private final int port;
    private ClientListener mListener;
    // 客户端请求处理列表   发送信息时需要注意多线程安全性问题 ，ArrayList并不是线程安全的
    // Collection.SynchronizedList构建的线程安全集合
    // 仅能保证删除 添加这些方法是线程安全的，但是无法保证遍历和删除操作的安全性
    private  List<ClientHandler> clientHandlerList = new ArrayList<>();
    // 构建线程池，
    private final ExecutorService forwardingThreadPoolExecutor;

    public TCPServer(int port) {
        this.port = port;
        // 转发线程池
        this.forwardingThreadPoolExecutor = Executors.newSingleThreadExecutor();
    }

    // 启动服务
    public boolean start() {
        try {
            ClientListener listener = new ClientListener(port);
            mListener = listener;
            listener.start();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    // 停止服务
    public void stop() {
        if (mListener != null) {
            mListener.exit();
        }
        // 退出连接   ---> 加入同步代码块，避免多线程产生的问题
        synchronized (TCPServer.this) {
            for (ClientHandler clientHandler : clientHandlerList) {
                clientHandler.exit();
            }

            clientHandlerList.clear();
        }
        // 清空列表
        clientHandlerList.clear();
        forwardingThreadPoolExecutor.shutdown();
    }

    // 服务端像客户端广播信息
    public void broadcast(String str) {
        // 向各设备发送信息
        for (ClientHandler handler: clientHandlerList
             ) {
            handler.send(str);
        }
    }

    @Override
    public void onNewMessageArrived(ClientHandler handler, String msg) {
        // 向其他客户端发送信息
        // 打印到屏幕
        System.out.println("Received-" + handler.getClientInfo() + ":" + msg);
        // 异步提交转发任务
        forwardingThreadPoolExecutor.execute(() -> {
            synchronized (TCPServer.this) {
                for (ClientHandler clientHandler : clientHandlerList) {
                    if (clientHandler.equals(handler)) {
                        // 跳过自己
                        continue;
                    }
                    // 对其他客户端发送消息
                    clientHandler.send(msg);
                }
            }
        });

    }

    @Override
    public void onSelfClosed(ClientHandler handler) {
        clientHandlerList.remove(handler);
    }

    /**客户端监听类
     * 监听客户端发来的请求
     */
    private class ClientListener extends Thread {
        private ServerSocket server;
        private boolean done = false;


        private ClientListener(int port) throws IOException {
            server = new ServerSocket(port);
            System.out.println("服务器信息：" + server.getInetAddress() + " P:" + server.getLocalPort());
        }

        @Override
        public void run() {
            super.run();
            System.out.println("服务器准备就绪～");
            // 等待客户端连接
            do {
                // 得到客户端
                Socket client;
                try {
                    client = server.accept();
                } catch (IOException e) {
                    continue;
                }
                // 客户端构建异步线程
                ClientHandler clientHandler = null;
                try {
                    clientHandler = new ClientHandler(client,TCPServer.this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // 启动线程
                // 读取数据并打印
                clientHandler.readToPrint();
                // 添加同步处理
                synchronized (TCPServer.this) {
                    clientHandlerList.add(clientHandler);
                }
            } while (!done);

            System.out.println("服务器已关闭！");
        }

        private void exit() {
            done = true;
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
