package algorithm.activeEverday;


import java.util.HashSet;
import java.util.Set;

/***
 * 给定一个包含大写字母和小写字母的字符串，找到通过这些字母构造成的最长的回文串。
 *
 * 在构造过程中，请注意区分大小写。比如 "Aa" 不能当做一个回文字符串。
 *
 * 注意:
 * 假设字符串的长度不会超过 1010。
 *
 * 示例 1:
 *
 * 输入:
 * "abccccdd"
 *
 * 输出:
 * 7
 *
 * 解释:
 * 我们可以构造的最长的回文串是"dccaccd", 它的长度是 7。
 * @date 2020-3-19
 */
public class _409_最长回文串 {

    public int longestPalindrome(String s) {
        Set<Character> set = new HashSet<>();
        int cn = 0 ;
        for (Character c : s.toCharArray()) {
            if ( set.contains(c)) {
                cn += 2;
                set.remove(c);
            }else{
                set.add(c);
            }
        }
        return set.size() >= 0 ? cn + 1 : cn;
    }

}
