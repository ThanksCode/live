package algorithm.org.linklist;

import offer.ListNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-16 18:40
 **/
public class _147对链表进行插入排序 {

    public ListNode insertionSortList(ListNode head) {
        if ( head == null ) {
            return null;
        }
//      所给链表为不带头结点的链表
        ListNode l = new ListNode(-1);
        //将链表转成带头结点的链表
        l.next = head;
       //指向head.next结点
        ListNode cur = l.next.next;
        //置空链表，此时链表为仅有一个有序元素的链表

        l.next.next = null;
        while ( cur != null) {
            //获取当前结点信息
            ListNode tmp = cur.next;
            // 遍历有序链表
            ListNode find = l;

            // 寻找合适的插入位置  此处必须使用 find.next.val进行比较
            // 这样find就记录前驱信息~~~
            while ( find.next != null && find.next.val < cur.val){
                find = find.next;
            }

            // 找到合适位置进行插入处理
            cur.next = find.next;
            find.next = cur;
            // 重新更新结点
            cur = tmp;
        }
        return l.next;
    }
}
