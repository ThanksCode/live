package com.com.basic.java.继承;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-29 13:49
 **/
interface Print{
    void print();
}

class Concert{

}
public abstract class Test extends Concert implements Print {
}
