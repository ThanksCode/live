package patter.creational.proxy.auto;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

/***
 * 自定义实现类加载处理，将信息加载到JVM中
 */
//代码生成、编译、重新动态load到JVM
public class ClassLoader_s  extends ClassLoader{

    private File baseDir ;
    public ClassLoader_s () {
        //创建根路径
        String basePath = ClassLoader_s.class.getResource("").getPath();
        System.out.println(basePath);
        this.baseDir = new File(basePath);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        String className = ClassLoader_s.class.getPackage().getName()+"." + name;
        if ( baseDir != null) {
            File clasFile = new File(baseDir,name.replaceAll("\\.","/") + ".class");
            if ( clasFile.exists()) {
                FileInputStream in = null;
                ByteArrayOutputStream out = null;
                try {
                    in = new FileInputStream(clasFile);
                    out = new ByteArrayOutputStream();
                    byte [] buff = new byte[1024];
                    int len;
                    while ( (len = in.read(buff)) != -1) {
                        out.write(buff,0,len);
                    }
                    return defineClass(className,out.toByteArray(),0,out.size());


                }catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    if ( in != null) {
                        try {
                            in.close();
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if ( out != null) {
                        try {
                            out.close();
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return null;
    }

}
