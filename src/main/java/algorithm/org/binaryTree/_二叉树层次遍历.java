package algorithm.org.binaryTree;

import offer.ListNode;
import offer.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。
 *
 *  
 *
 * 示例：
 * 二叉树：[3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * 返回其层次遍历结果：
 *
 * [
 *   [3],
 *   [9,20],
 *   [15,7]
 * ]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/binary-tree-level-order-traversal
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-30 22:07
 **/
public class _二叉树层次遍历 {

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> list = new ArrayList();
        Queue queue = new LinkedList();

        // 处理异常情况
        if (root == null) {
            return list;
        }

        queue.add(root);

        while ( !queue.isEmpty() ) {
            int count = queue.size();
            List<Integer> rs = new ArrayList<>();
            // 队列中的已有元素出队 同时放入新的元素信息
            while ( count > 0 ) {
                // 注意一个细节 remove移除 index 位置元素，当信息不存在时抛出异常
                TreeNode cur = (TreeNode) queue.poll();
                rs.add(cur.val);
                // 将cur子节点入栈
                if ( cur.left != null) {
                    queue.add(cur.left);
                }

                if ( cur.right != null) {
                    queue.add(cur.right);
                }
                count -- ;
            }
            // 加入到最后的结果集中
            list.add(rs);
        }

        return list;
    }

    // 2020 - 7 - 28
    public List<List<Integer>> level(TreeNode root){
        List<List<Integer>> list = new ArrayList();
        Queue queue = new LinkedList();

        // 处理异常情况
        if (root == null) {
            return list;
        }

        // root insert
        queue.add(root);
        // 加入结束符
   /*     queue.add(null);*/
        while ( !queue.isEmpty()){
            int count = queue.size();
            List<Integer> rs = new ArrayList<>();
            while( count-- > 0 ){
                TreeNode cur = (TreeNode) queue.poll();
                rs.add(cur.val);
                if ( cur.left != null) {
                    queue.add(cur.left);
                }
                if ( cur.right != null){
                    queue.add(cur.right);
                }
            }
            list.add(rs);
        }
        return list;
    }


    //
    public List<List<Integer>> levelV2(TreeNode root){
        int d;
        List<List<Integer>> list = new ArrayList();
        Queue queue = new LinkedList();

        // 处理异常情况
        if (root == null) {
            return list;
        }

        // root insert
        queue.add(root);
        // 加入结束符
        /*     queue.add(null);*/
        List<Integer> rs = new ArrayList<>();
        while ( !queue.isEmpty()){
            TreeNode cur = (TreeNode) queue.poll();
           // if ( cur )
        }
        return list;
    }


    // 队列处理方式
    public List<List<Integer>> levelV3(TreeNode root) {
        List<List<Integer>> list = new ArrayList();
        // 记录层深度
        int leveal = 0 ;

        // 极端情况
        if( root == null ) {
            return null;
        }

        Queue queue = new LinkedList();

        // 根节点入队
        queue.add(root);
        // null 为每层遍历结束的标志
        queue.add(null);

        // 队列非空下进行循环遍历
        List<Integer> levelNum = new ArrayList<>();
        while ( !queue.isEmpty()){
            TreeNode cur = (TreeNode) queue.poll();
            // 表名一层已经别遍历完成
            if ( cur == null ) {
                // 加入一个null
                // ((LinkedList) queue).add(null);  // 会导致无限死循环！！！！！
                if( !queue.isEmpty() ){
                    queue.add(null);
                }
                list.add(levelNum);
                levelNum = new ArrayList<>();
            }else{ // 此处不能放在外面 ！！！
                levelNum.add(cur.val);
                // 处理左子树，右子树
                if ( cur.left !=null){
                    queue.add(cur.left);
                }
                if ( cur.right != null) {
                    queue.add(cur.right);
                }
            }

        }
        return list;
    }

    //error版本
 /*   public List<List<Integer>> levelError(TreeNode root){
        List<List<Integer>> list = new ArrayList();
        Queue queue = new LinkedList();

        // 处理异常情况
        if (root == null) {
            return list;
        }

        // root insert
        queue.add(root);
        // 加入结束符
        //queue.add(null);
        List<Integer> rs = new ArrayList<>();
        while ( !queue.isEmpty()){
            TreeNode cur = (TreeNode) queue.poll();
            if ( cur == null){
                if ( !queue.isEmpty()){
                    // 加入结束记号
                    queue.add(null);
                    list.add(rs);
                    rs = new ArrayList<>();
                }
            }else{
                rs.add(cur.val);
                if ( cur.left != null ){
                    queue.add(cur.left);
                }

                if ( cur.right != null){
                    queue.add(root.right);
                }
            }
        }
        return list;
    }*/
}
