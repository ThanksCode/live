package offer;

/****
 * 写一个函数，输入 n ，求斐波那契（Fibonacci）数列的第 n 项。斐波那契数列的定义如下：
 *
 * F(0) = 0,   F(1) = 1
 * F(N) = F(N - 1) + F(N - 2), 其中 N > 1.
 * 斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。
 *
 * 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fei-bo-na-qi-shu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * 序列：0  1  1  2  3  5 8
 * 公式  f（n） = f(n-1) + f( n-2)
 *
 * date:2020-2-12
 */
public class _10_1_斐波那契数列 {

    public int fib(int n) {

        //处理特殊情况
        if ( n == 0 ){
            return 0;
        }

        if ( n ==  1) {
            return 1;
        }

        int pre = 0;
        int last = 1;

        for ( int i = 2 ; i <= n ; i ++ ){
            int sum =  pre % 1000000007  + last % 1000000007 ;
            pre = last % 1000000007;
            last = sum % 1000000007;
        }

        return last  % 1000000007;
    }
}
