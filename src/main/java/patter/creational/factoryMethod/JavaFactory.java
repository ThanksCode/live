package patter.creational.factoryMethod;

public class JavaFactory extends ProduceFactory {


    @Override
    public Video getVideo() {
        return new JavaVideo();
    }

    @Override
    public Article getArtile() {
        return new JavaArticle();
    }
}
