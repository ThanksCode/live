package com.com.basic.java;

import java.util.Arrays;

/**
 * 基于引用的传递
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-23 20:02
 **/
public class _数组引用传递 {

    public void fun(int [] arr){
        arr[1] = 5;
        System.out.println("函数中数组的内容信息： " + Arrays.toString(arr));
    }

    public static void main(String[] args) {
        _数组引用传递 arrRef = new _数组引用传递();
        int [] arr = {1,2,3};
        System.out.println(Arrays.toString(arr));
        arrRef.fun(arr);
        System.out.println(Arrays.toString(arr));
    }
}
