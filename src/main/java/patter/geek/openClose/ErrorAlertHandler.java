package patter.geek.openClose;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 17:12
 **/
public class ErrorAlertHandler extends AbstractAlertHandler {

    public ErrorAlertHandler(AlertRule rule ,NotificationEmergencyLevel notificationEmergencyLevel){
        super(rule,notificationEmergencyLevel);
    }

    @Override
    public void check(AlertInof alertInof) {
        System.out.println("处理error  信息");
    }
}
