package com.web.strategy;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-10 19:10
 **/
public class UserController implements Serializable {

    public void login(){
        System.out.println("访问登录方法");
    }

    public void detail(String name,String password){
        System.out.println("用户名信息 ：" + name + " 密码信息  " + password );
    }



}
