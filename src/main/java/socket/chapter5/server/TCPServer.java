package socket.chapter5.server;

import com.com.basic.java.类型信息.transfer.dao.AccountDao;
import socket.chapter5.handle.ClientHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 17:07
 **/
public class TCPServer {
    // 端口信息
    private final int port;
    private ClientListener mListener;
    // 客户端请求处理列表
    private  List<ClientHandler> clientHandlerList = new ArrayList<>();

    public TCPServer(int port) {
        this.port = port;
    }

    // 启动服务
    public boolean start() {
        try {
            ClientListener listener = new ClientListener(port);
            mListener = listener;
            listener.start();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    // 停止服务
    public void stop() {
        if (mListener != null) {
            mListener.exit();
        }
        // 退出连接
        for (ClientHandler clientHandler : clientHandlerList) {
            clientHandler.exit();
        }
        // 清空列表
        clientHandlerList.clear();
    }

    // 服务端像客户端广播信息
    public void broadcast(String str) {
        // 像各设备发送信息
        for (ClientHandler handler: clientHandlerList
             ) {
            handler.send(str);
        }
    }

    /**客户端监听类
     * 监听客户端发来的请求
     */
    private class ClientListener extends Thread {
        private ServerSocket server;
        private boolean done = false;


        private ClientListener(int port) throws IOException {
            server = new ServerSocket(port);
            System.out.println("服务器信息：" + server.getInetAddress() + " P:" + server.getLocalPort());
        }

        @Override
        public void run() {
            super.run();
            System.out.println("服务器准备就绪～");
            // 等待客户端连接
            do {
                // 得到客户端
                Socket client;
                try {
                    client = server.accept();
                } catch (IOException e) {
                    continue;
                }
                // 客户端构建异步线程
                ClientHandler clientHandler = null;
                try {
                    clientHandler = new ClientHandler(client, handler -> {
                      // 移除列表
                        clientHandlerList.remove(handler);
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // 启动线程
                // 读取数据并打印
                clientHandler.readToPrint();
                clientHandlerList.add(clientHandler);
                
            } while (!done);

            System.out.println("服务器已关闭！");
        }

        private void exit() {
            done = true;
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
