package algorithm.org.binaryTree;

import offer.TreeNode;

import javax.transaction.TransactionRequiredException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
/***
 * 145. 二叉树的后序遍历
 * 给定一个二叉树，返回它的 后序 遍历。
 *
 * 示例:
 *
 * 输入: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * 输出: [3,2,1]
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 20:58
 **/
public class _后序遍历 {


    public List<Integer> postorderTraversal(TreeNode root , int index) {
        // 返回结果集
        List<Integer> result = new ArrayList<>();

        if ( root == null) {
            return result;
        }

        // 定义两个栈
        Stack<TreeNode> s1 = new Stack<>();
        Stack<TreeNode> s2 = new Stack<>();

        // 存入根结点
        s1.push(root);

        while ( !s1.isEmpty()){

            TreeNode cur = s1.pop();
            // 存入s2
            s2.push(cur);

            if ( cur.left != null ) {
                s1.push(cur.left);
            }

            if ( cur.right != null ) {
                s1.push(cur.right);
            }

        }

        while ( !s2.isEmpty() ) {
            TreeNode cur = s2.pop();
            result.add(cur.val);
        }


        return result;

    }


    // 法二
    public List<Integer> postorderTraversal(TreeNode root) {
        // 返回结果集
        List<Integer> result = new ArrayList<>();

        // 异常结果处理
        if ( root == null) {
            return result;
        }

        Stack<TreeNode> stk = new Stack<>();

        stk.push(root);

        // 处理数据
        while ( !stk.isEmpty()){
            // 检查首元素信息
            TreeNode peek = stk.peek();

            if ( peek == null) {
                // 弹出 null
                stk.pop();
                // 存放后序的内容
                result.add(stk.peek().val);
                // 弹出该结点信息
                stk.pop();
                continue;
            }

            // 装入空节点
            stk.push(null);

            // 依次遍历右子树 左子树
            if ( peek.right != null) {
                stk.push(peek.right);
            }

            if ( peek.left != null) {
                stk.push(peek.left);
            }
        }
        return result;

    }

    // 优化：借助 stack 和LinkedList完成逆序处理
    // 依赖于LinkList 实现List接口  多态的完美体现，面向接口的编程优势
    public List<Integer> pastOrder(TreeNode root) {
        LinkedList<Integer> list = new LinkedList<>();
        if ( root == null ){
            return list;
        }
        Stack<TreeNode> stackTmp = new Stack<>();
        stackTmp.push(root);

        // 循环处理
        while ( !stackTmp.isEmpty()) {
            // 获取第一个入栈结点
            TreeNode cur = stackTmp.pop();
            list.addFirst(cur.val);

            // 一定要保证 先遍历右子树信息  所以后放入右子树结点
            if ( cur.left != null) {
                stackTmp.push(cur.left);
            }

            if ( cur.right != null) {
                stackTmp.push(cur.right);
            }
        }

        return list;
    }


}
