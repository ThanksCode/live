package algorithm.org.binaryTree;

/**
 * 给定一个非空二叉树，返回其最大路径和。
 *
 * 本题中，路径被定义为一条从树中任意节点出发，达到任意节点的序列。该路径至少包含一个节点，且不一定经过根节点。
 *
 * 示例 1:
 *
 * 输入: [1,2,3]
 *
 *        1
 *       / \
 *      2   3
 *
 * 输出: 6
 * 示例 2:
 *
 * 输入: [-10,9,20,null,null,15,7]
 *
 *    -10
 *    / \
 *   9  20
 *     /  \
 *    15   7
 *
 * 输出: 42
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/binary-tree-maximum-path-sum
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 **/

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import offer.TreeNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-03 10:06
 **/
public class _124二叉树中的最大路径和 {

    private int rs = Integer.MIN_VALUE;
    public int maxPathSum(TreeNode root , int index ) {
        // 记录长度
        countSum(root);
        return rs;
    }

    // 统计遍历时的长度信息
   private int countSum(TreeNode root) {

        if ( root == null) {
            return 0;
        }

        // 只有在最大贡献值大于 0 时，才会选取对应子节点
        int leftPath = Math.max(0,countSum(root.left));
        int rightPath = Math.max(0,countSum(root.right));
        // 节点的最大路径和取决于该节点的值与该节点的左右子节点的最大贡献值
        int maxPath = Math.max(rs,leftPath + rightPath + root.val);
        return Math.max(leftPath,rightPath) + root.val;
   }




}
