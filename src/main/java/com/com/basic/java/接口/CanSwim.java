package com.com.basic.java.接口;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-11 09:18
 **/
public interface CanSwim {
    void swim();
}
