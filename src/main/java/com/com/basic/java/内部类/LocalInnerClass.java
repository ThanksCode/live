package com.com.basic.java.内部类;


/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-13 10:13
 **/
public class LocalInnerClass {

    private int count = 0;

    Counter getCounter(String name){

        //局部内部类可访问 局部变量和外部类的属性信息
        class LocalCounter implements  Counter{
            @Override
            public int next() {
                System.out.println(name);
                return count++;
            }
        }
        return new LocalCounter();
    }

    //在匿名内部类中实现
    Counter getCounter_1(String name){
        return new Counter() {
            //代码块初始化变量信息
            {
                System.out.println("counter()");
            }
            @Override
            public int next() {
                System.out.println(name);
                return count ++;
            }
        };
    }


    public static void main(String[] args) {
        LocalInnerClass lic = new LocalInnerClass();
        Counter
                c1 = lic.getCounter("local inner"),
                c2 = lic.getCounter_1("Anonymous inner  ");
        for (int i = 0 ; i < 5 ; i ++ ){
            System.out.println(c1.next());
        }

        for ( int i = 0 ; i < 5 ; i ++ ) {
            System.out.println(c2.next());
        }
    }
}
