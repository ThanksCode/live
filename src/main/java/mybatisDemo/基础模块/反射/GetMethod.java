package mybatisDemo.基础模块.反射;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/** 记录返回值类型和所执行方法名
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-16 10:08
 **/
public class GetMethod implements InvokeHandler {
    private final Class<?> type;
    private final Method method;

    public GetMethod(Method method) {
        this.method = method;

        if (method.getParameterTypes().length == 1) {
            type = method.getParameterTypes()[0];
        } else {
            type = method.getReturnType();
        }
    }

    @Override
    public Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException {
        return method.invoke(target, args);
    }

    @Override
    public Class<?> getType() {
        return type;
    }
}
