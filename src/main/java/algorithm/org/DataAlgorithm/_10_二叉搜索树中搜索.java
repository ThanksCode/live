package algorithm.org.DataAlgorithm;
/***
 * 给定二叉搜索树（BST）的根节点和一个值。 你需要在BST中找到节点值等于给定值的节点。 返回以该节点为根的子树。 如果节点不存在，则返回 NULL。
 *
 * 例如，
 *
 * 给定二叉搜索树:
 *
 *         4
 *        / \
 *       2   7
 *      / \
 *     1   3
 *
 * 和值: 2
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/search-in-a-binary-search-tree
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

import offer.TreeNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 14:29
 **/
public class _10_二叉搜索树中搜索 {

    public TreeNode searchBST(TreeNode root, int val) {
        // 特殊情况
        if( root == null) {
            return null;
        }

        if( root.val > val) {
            return searchBST(root.left,val);
        }else if ( root.val < val) {
            return searchBST(root.right,val);
        }else{
            return root;
        }

    }


    public TreeNode searchBSTV1(TreeNode root, int val) {
        // 特殊情况
        if( root == null) {
            return null;
        }

        while (root != null) {
            if( root.val > val) {
                root = root.left;
            }else if ( root.val < val) {
                root = root.right;
            }else{
               break;
            }
        }
        return root;
    }


}
