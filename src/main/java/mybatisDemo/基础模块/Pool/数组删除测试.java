package mybatisDemo.基础模块.Pool;
/***
 * 源于mybatis源码连接池维护产生
 * 一个疑问：
 *  ArrayList移除第一个元素后
 *      原先位置是否会被填充？
 *      不指定位置，元素会在那插入？
 */


import java.util.ArrayList;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-24 09:04
 **/
public class 数组删除测试 {
    /***
     * 观察源码不难发现，自移除元素时，进行了数据内容的重新拷贝
     * 原数据待复制的位置
     *目标数组存放  元素的位置 长度信息
     * 置空 便于垃圾回收的运行
     * @param args
     */

    /*
    public E remove(int index) {
        rangeCheck(index);

        modCount++;
        E oldValue = elementData(index);

        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index,
                    numMoved);
        elementData[--size] = null; // clear to let GC do its work

        return oldValue;
    }*/
    public static void main(String[] args) {
        ArrayList<Integer> rs = new ArrayList<>();
        int [] rs1 = {1,2,3,4};
        for (int i = 0 ; i < rs1.length; i ++ ) {
            rs.add(rs1[i]);
        }
        // 删除首元素
        rs.remove(0);
        for( int i = 0 ; i < rs.size(); i ++ ) {
            System.out.println("index " + i + "   " + rs.get(i));
        }
    }
}
