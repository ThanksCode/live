package patter.creational.proxy.auto;



public class Test {
    public static void main(String[] args) {

        try {
            Person person = (Person)new Introduce_s().getInstance(new Coder());
            person.findLove();
            person.work();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
