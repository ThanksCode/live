package patter.creational.builder.vesion2;

public class Demo {

    public static void main(String[] args) {
        Course course = new Course.CourseBuilder().buildCourseName("Java设计模式").buildCoursePPT("ppt").build();
        System.out.println(course);
    }
}
