package socket.Demo.server;

import socket.Demo.constants.TCPConstants;

import java.io.IOException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 18:29
 **/
public class Server {
    public static void main(String[] args) {
        TCPServer tcpServer = new TCPServer(TCPConstants.PORT_SERVER);
        boolean isSucceed = tcpServer.start();
        if (!isSucceed) {
            System.out.println("构建TCP连接失败!");
            return;
        }

        UDPProvider.start(TCPConstants.PORT_SERVER);

        try {
            //noinspection ResultOfMethodCallIgnored
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        UDPProvider.stop();
        tcpServer.stop();
    }
}
