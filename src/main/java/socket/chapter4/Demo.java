package socket.chapter4;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-03 09:27
 **/
public class Demo {

    public static void main(String[] args) {
        int x = 0x1234 ;
        int y =  x >> 8;
        System.out.println(Integer.toBinaryString(x));
        System.out.println(Integer.toBinaryString(y & 0xff));
        System.out.println(Integer.toBinaryString(0xff));
    }
}
