package socket.chapter6.client;

import socket.chapter6.entry.ServerInfo;

import java.io.IOException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 16:28
 **/
public class Client {
    public static void main(String[] args) throws Exception {
        ServerInfo info = UDPSearcher.searchServer(10000);
        System.out.println("Server:" + info);

        if (info != null) {
            try {
                TCPClient.linkWith(info);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
