package algorithm.org.binaryTree;

import com.sun.org.apache.regexp.internal.RE;
import offer.TreeNode;

import java.util.*;

/**
 * 199
 * 给定一棵二叉树，想象自己站在它的右侧，按照从顶部到底部的顺序，返回从右侧所能看到的节点值。
 *
 * 示例:
 *
 * 输入: [1,2,3,null,5,null,4]
 * 输出: [1, 3, 4]
 * 解释:
 *
 *    1            <---
 *  /   \
 * 2     3         <---
 *  \     \
 *   5     4       <---
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/binary-tree-right-side-view
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-30 22:25
 **/
public class _二叉树右视图 {

    public List<Integer> rightSideView(TreeNode root) {
        // 结果集
        List<Integer> result = new ArrayList<>();
        // 辅助队列
        Queue queue = new LinkedList();

        queue.add(root);

        while ( !queue.isEmpty()){
            // 每次仅将第一个元素保存至结果数组中
            /*TreeNode cur = (TreeNode) queue.poll();
            result.add(cur.val);*/
            int count = queue.size();
            boolean flag = true;
            // 将队列中其他元素出队
            while ( count > 0 ){
                TreeNode cur = (TreeNode) queue.poll();
                if ( flag ) {
                    flag = false;
                    result.add(cur.val);
                }

                if ( cur.left != null) {
                    queue.add(cur.right);
                }

                if ( cur.right != null) {
                    queue.add(cur.left);
                }

                count -- ;
            }
        }

        return result;
    }

}
