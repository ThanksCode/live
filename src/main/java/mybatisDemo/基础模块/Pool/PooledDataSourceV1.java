package mybatisDemo.基础模块.Pool;


import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-23 10:15
 **/
public class PooledDataSourceV1 implements DataSource {


    private final PoolState state = new PoolState(new PooledDataSource());

    private final UnpooledDataSource dataSource;

    // OPTIONAL CONFIGURATION FIELDS
    protected int poolMaximumActiveConnections = 10;
    protected int poolMaximumIdleConnections = 5;
    protected int poolMaximumCheckoutTime = 20000;
    protected int poolTimeToWait = 20000;
    protected int poolMaximumLocalBadConnectionTolerance = 3;
    protected String poolPingQuery = "NO PING QUERY SET";
    protected boolean poolPingEnabled;
    protected int poolPingConnectionsNotUsedFor;

    private int expectedConnectionTypeCode;

    public PooledDataSourceV1() {
        dataSource = new UnpooledDataSource();
    }

    public PooledDataSourceV1(UnpooledDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public PooledDataSourceV1(String driver, String url, String username, String password) {
        dataSource = new UnpooledDataSource(driver, url, username, password);
        expectedConnectionTypeCode = assembleConnectionTypeCode(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
    }

    public PooledDataSourceV1(String driver, String url, Properties driverProperties) {
        dataSource = new UnpooledDataSource(driver, url, driverProperties);
        expectedConnectionTypeCode = assembleConnectionTypeCode(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
    }

    public PooledDataSourceV1(ClassLoader driverClassLoader, String driver, String url, String username, String password) {
        dataSource = new UnpooledDataSource(driverClassLoader, driver, url, username, password);
        expectedConnectionTypeCode = assembleConnectionTypeCode(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
    }

    public PooledDataSourceV1(ClassLoader driverClassLoader, String driver, String url, Properties driverProperties) {
        dataSource = new UnpooledDataSource(driverClassLoader, driver, url, driverProperties);
        expectedConnectionTypeCode = assembleConnectionTypeCode(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
    }

    @Override
    public Connection getConnection() throws SQLException {
        return popConnection(dataSource.getUsername(), dataSource.getPassword()).getProxyConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return popConnection(username, password).getProxyConnection();
    }

    private PooledConnection popConnection(String username, String password) throws SQLException {
        // 等待标志信息
        boolean countWait = false;
        // 获得当前时间
        Long t = System.currentTimeMillis();
        PooledConnection conn = null;
        int localBadConnectionCount = 0;
        // 在循环中一直获取conn 直到conn不为null
        while (conn == null) {
            // 同步处理
            synchronized (state) {
                if (!state.idleConnections.isEmpty()) {
                    // 空闲池不为空则直接返回第一个
                    // 获取后还应该移除连接对象
                    conn = state.idleConnections.remove(0);
                } else {
                    // 此时判断活跃池是否已经大大最大连接数
                    if (state.activeConnections.size() < poolMaximumActiveConnections) {
                        // 没达到最大连接创建新的连接对象
                        conn = new PooledConnection(dataSource.getConnection(), new PooledDataSource());
                    } else { // 此时已经达到最大连接数
                        // 获取最先创建的连接对象
                        PooledConnection oledActiveConnection = state.activeConnections.get(0);
                        // 获取最长时间戳
                        long longesCheckoutTime = oledActiveConnection.getCheckoutTime();
                        if (longesCheckoutTime > poolMaximumActiveConnections) {
                            // 有连接超时，统计超时连接
                            state.claimedOverdueConnectionCount++;
                            state.accumulatedCheckoutTime += longesCheckoutTime;
                            // 将超时连接移出 activeConnections 集合
                            state.activeConnections.remove(oledActiveConnection);

                            // 如果超时连接未提交，则自动回滚
                            if (!oledActiveConnection.getRealConnection().getAutoCommit()) {
                                oledActiveConnection.getRealConnection().rollback();
                            }

                            // 用已有连接重新初始化一个连接对象信息 并未创建正真的新连接
                            // 创建新的PooledConnection连接
                            conn = new PooledConnection(
                                    oledActiveConnection.getRealConnection(), new PooledDataSource());
                            conn.setCreatedTimestamp(oledActiveConnection.getCreatedTimestamp());
                            conn.setLastUsedTimestamp(oledActiveConnection.getLastUsedTimestamp());
                            // 将超时的PooledConnection设为无效
                            oledActiveConnection.invalidate();
                        } else {//无空闲连接、无法创建新连接且无超时连接，只能阻塞等待
                            try {
                                if (!countWait) {
                                    // 统计等待次数
                                    state.hadToWaitCount++;
                                    countWait = false;
                                }
                                long wt = System.currentTimeMillis();
                                // 阻塞等待
                                state.wait(poolTimeToWait);
                                state.accumulatedWaitTime -= wt;
                            } catch (InterruptedException e) {
                                break;
                            }
                        }

                    }
                }

                if (conn != null) {
                    // 检测conn是否有效 ---》 pingConnection
                    if (conn.isValid()) {
                        if (!conn.getRealConnection().getAutoCommit()) {
                            conn.getRealConnection().rollback();
                        }
                        new ArrayList<>();
                        // 设定所连数据库信息
                        conn.setConnectionTypeCode(assembleConnectionTypeCode(dataSource.getUrl(), username, password));
                        // 设定时间
                        conn.setCheckoutTimestamp(System.currentTimeMillis());
                        // 设定最近使用时间
                        conn.setLastUsedTimestamp(System.currentTimeMillis());
                        // 加入到队列中
                        state.activeConnections.add(conn);
                        state.requestCount++;
                        // 记录请求时间
                        state.accumulatedRequestTime += System.currentTimeMillis() - t;
                    } else {
                        state.badConnectionCount++;
                        // 统计是否能获取到有效连接数
                        localBadConnectionCount++;
                        conn = null;
                        if (localBadConnectionCount > (poolMaximumIdleConnections + poolMaximumLocalBadConnectionTolerance)) {
                            throw new SQLException("PooledDataSource: Could not get a good connection to the database.");
                        }
                    }
                }
            }


        }
        return conn;
    }

    private int assembleConnectionTypeCode(String url, String username, String password) {
        return ("" + url + username + password).hashCode();
    }


    private PooledConnection popConnectionV1(String username, String password) throws SQLException {
        // 等待标志信息
        boolean countWait = false;
        // 获得当前时间
        Long t = System.currentTimeMillis();
        PooledConnection conn = null;
        int localBadConnectionCount = 0;
        // 循环获取连接
        while (conn == null) {
            synchronized (state) {
                // 判断是否有空余连接
                if (!state.idleConnections.isEmpty()) {
                    conn = state.idleConnections.remove(0);
                } else {
                    // 空闲池没有多余连接
                    // 此时活跃池能否可以放入？
                    if (state.activeConnections.size() < poolMaximumActiveConnections) {
                        conn = new PooledConnection(dataSource.getConnection(), new PooledDataSource());
                    } else {
                        //此时活跃池已满
                        // 进行校验，判断最先进入的是否已经过期
                        PooledConnection oldConnection = state.activeConnections.get(0);
                        long checkoutTime = oldConnection.getCheckoutTime();
                        if (checkoutTime > poolTimeToWait) {
                            // 有连接超时，统计超时连接
                            state.claimedOverdueConnectionCount++;
                            state.accumulatedCheckoutTime += checkoutTime;

                            // 移除该连接
                            state.activeConnections.remove(oldConnection);
                            // 处理该连接所涵盖的事务信息
                            if (!oldConnection.getRealConnection().getAutoCommit()) {
                                oldConnection.getRealConnection().rollback();
                            }
                            // 单纯的为了消除重复影响 无任何作用
                            new ArrayList<>();
                            // 重新构建一个新的连接  借魂复生
                            conn = new PooledConnection(oldConnection.getRealConnection(), new PooledDataSource());
                            conn.setCreatedTimestamp(oldConnection.getCreatedTimestamp());
                            conn.setLastUsedTimestamp(oldConnection.getLastUsedTimestamp());
                            // 将超时的PooledConnection设为无效
                            oldConnection.invalidate();
                        } else {//无空闲连接、无法创建新连接且无超时连接，只能阻塞等待
                            // 超时等待处理
                            try {
                                if (!countWait) {
                                    countWait = false;
                                    state.hadToWaitCount++;
                                }
                                long wt = System.currentTimeMillis();
                                state.wait(poolTimeToWait);
                                state.accumulatedWaitTime -= wt;
                            } catch (InterruptedException e) {
                                break;
                            }
                        }
                    }
                }
                // 处理连接
                if (conn != null) {
                    if (conn.isValid()) {
                        if (!conn.getRealConnection().getAutoCommit()) {
                            conn.getRealConnection().rollback();
                        }
                        // 设定所连数据库信息
                        conn.setConnectionTypeCode(assembleConnectionTypeCode(dataSource.getUrl(), username, password));
                        // 设定时间
                        conn.setCheckoutTimestamp(System.currentTimeMillis());
                        // 设定最近使用时间
                        conn.setLastUsedTimestamp(System.currentTimeMillis());
                        // 加入到队列中
                        state.activeConnections.add(conn);
                        state.requestCount++;
                        // 记录请求时间
                        state.accumulatedRequestTime += System.currentTimeMillis() - t;
                    } else {//为获取连接信息 获取到的是无效连接
                        state.badConnectionCount++;
                        // 统计是否能获取到有效连接数
                        localBadConnectionCount++;
                        conn = null;
                        if (localBadConnectionCount > (poolMaximumIdleConnections + poolMaximumLocalBadConnectionTolerance)) {
                            throw new SQLException("PooledDataSource: Could not get a good connection to the database.");
                        }
                    }

                }
            }// synchronized

        }// while
        return conn;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    protected boolean pingConnection(PooledConnection conn) throws SQLException {
        boolean result = true;
        try {
            result = !conn.getRealConnection().isClosed();
        } catch (SQLException e) {
            result = false;
        }
        if (result) {
            //检测 poolPingEnabled 设置，是否运行执行测试 SQL 语句
            //长时间（超过 poolPingConnectionsNotUsedFor 指定的时长）未使用的连接，才需要 ping
            if (poolPingEnabled) {
                if (poolPingConnectionsNotUsedFor >= 0 && conn.getTimeElapsedSinceLastUse() > poolPingConnectionsNotUsedFor) {
                    try {
                        // 执行sql语句
                        Connection realConn = conn.getRealConnection();
                        Statement statement = realConn.createStatement();
                        ResultSet rs = statement.executeQuery(poolPingQuery);
                        rs.close();
                        statement.close();
                        result = true;

                    } catch (Exception e) {
                        conn.getRealConnection().close();
                        result = false;
                    }
                }
            }
        }
        return result;
    }
}
