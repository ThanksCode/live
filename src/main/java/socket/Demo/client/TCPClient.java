package socket.Demo.client;



import socket.Demo.entry.ServerInfo;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 17:27
 **/
public class TCPClient {

    // 根据提供的信息构建TCP连接
    public static void linkWith(ServerInfo info) throws IOException {
        Socket socket = new Socket();
        // 超时时间
        socket.setSoTimeout(3000);

        // 连接本地，端口2000；超时时间3000ms
        socket.connect(new InetSocketAddress(Inet4Address.
                getByName(info.getAddress()), info.getPort()), 3000);

        System.out.println("已发起服务器连接，已经建立TCP连接，并进入后续流程～");
        System.out.println("客户端信息：" + socket.getLocalAddress() + " P:" + socket.getLocalPort());
        System.out.println("服务器信息：" + socket.getInetAddress() + " P:" + socket.getPort());

        try {
            // 发送接收数据
            todo(socket);
        } catch (Exception e) {
            System.out.println("异常关闭");
        }

        // 释放资源
        socket.close();
        System.out.println("客户端已退出～");
    }

    // 收发数据控制
    private static void todo(Socket socket) throws IOException {
        // 操作：获取键盘输入流， 获取socket输出流  获取socket输入流

        // 键盘输入流
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        OutputStream output = socket.getOutputStream();
        PrintStream socketPrint = new PrintStream(output);

        // 获取服务器端输入流
        BufferedReader socketReader = new BufferedReader(new InputStreamReader
                (socket.getInputStream()));

        boolean flag = true;
        do {

            String str = input.readLine();
            // 发送至服务器
            socketPrint.println(str);

            // 读取服务器内容
            String echo =  socketReader.readLine();

            if ( echo.equals("bye")){
                flag = false;
            }else{
                // 回显信息
                System.out.println(echo);
            }
        }while (flag);


        socketReader.close();
        socketPrint.close();


    }
}
