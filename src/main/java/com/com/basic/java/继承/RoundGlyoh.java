package com.com.basic.java.继承;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-10 09:25
 **/
public class RoundGlyoh extends Glyph {

    private int radius = 1;

    RoundGlyoh(int i ){
        radius = i;
        System.out.println("RoundGlyph.RoundGlyph()  + Radius " + "= " + radius);
    }

    @Override
    void draw(){
        System.out.println("RoundGlyph.draw()  radius = " + radius);
    }

    public static void main(String[] args) {
        new RoundGlyoh(12);
    }
}
