package patter.creational.simpleFactory;

public class VideoFactory  {

    /***
     * 使用class类型避免了过多的if-else判断逻辑的出现
     * @param c
     * @return
     */
    public  static  Video getInstance(Class c) {
        Video video = null;
        try{
            video = (Video)Class.forName(c.getName()).newInstance();
            return video;
        }catch (Exception e) {
            throw new RuntimeException("未找到指定类信息");
        }
    }
}
