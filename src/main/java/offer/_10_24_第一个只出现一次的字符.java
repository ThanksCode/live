package offer;

/**
 * 在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。
 *
 * 示例:
 *
 * s = "abaccdeff"
 * 返回 "b"
 *
 * s = ""
 * 返回 " "
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-3-2
 */
public class _10_24_第一个只出现一次的字符 {

    public char firstUniqChar(String s) {
        //首先置为false表示肯定能找到，如果找不到则将flag置为true
        int rs[] = new int[130];//利用一个数组来模拟map的映射

        for( int i = 0 ; i < s.length() ; i ++ ){
            int tmp = s.charAt(i) - 'a';
            if ( rs[tmp] == 0){
                rs[tmp] = 1;
            }else{
                rs[tmp] ++;
            }
        }

        //第二次遍历从数组查找

        for( int i = 0 ; i < s.length() ; i ++ ){
            if ( rs[s.charAt(i) - 'a'] == 1){
                return s.charAt(i);
            }
        }
        return  ' ';

    }

    public static void main(String[] args) {
        _10_24_第一个只出现一次的字符 rs = new _10_24_第一个只出现一次的字符();
        rs.firstUniqChar("abc");
    }
}
