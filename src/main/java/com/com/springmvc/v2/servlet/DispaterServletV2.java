package com.com.springmvc.v2.servlet;

import com.com.basic.java.继承.Meal;
import com.com.springmvc.v2.annotation.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-25 16:03
 **/
public class DispaterServletV2 extends HttpServlet {
    //配置文件信息
    private Properties contextConfig = new Properties();
    //保存包下所有的类信息名字
    private List<String> classNames = new ArrayList<>();
    //ioc容器用来存放对象信息
    private Map<String,Object> instanceMapping = new HashMap<>();
    //建立方法同url之间的映射关系
    private List<Handler> handlerMapping = new ArrayList<Handler>();
    private final char START = 'A';
    private final char END = 'Z';
    @Override
    public void init(ServletConfig config) throws ServletException {
        //加载初始化信息
        doLoadConfig(config.getInitParameter("contextConfigLocation"));
        System.out.println(config.getInitParameter("contextConfigLocation"));
        //获取到配置文件中信息进行扫描
        scanClass(contextConfig.getProperty("scanPackage"));
        instance();
        autowired();
        handlerMapping();
        System.out.println("codnig MVC Framework 已经准备就绪啦，欢迎来戳我!!!");
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try{
            //从上面已经初始化的信息中匹配
            //拿着用户请求url去找到其对应的Method
            boolean isMatcher = pattern(req,resp);
            if(!isMatcher){
                resp.getWriter().write("404 Not Found");
            }
        }catch(Exception e){
            resp.getWriter().write("500 Exception,Details:\r\n" +
                    e.getMessage() + "\r\n" +
                    Arrays.toString(e.getStackTrace()).replaceAll("\\[\\]", "")
                            .replaceAll(",\\s", "\r\n"));
        }

    }
    //类型转换
    private Object castStringValue(String value,Class<?> clazz){

        if(clazz == String.class){
            return value;
        }else if(clazz == Integer.class){
            return Integer.valueOf(value);
        }else if(clazz == int.class){
            return Integer.valueOf(value).intValue();
        }else{
            return null;
        }

    }

    //匹配路径获取参数信息，填充参数
    private boolean pattern(HttpServletRequest req, HttpServletResponse resp) throws InvocationTargetException, IllegalAccessException {
        if(handlerMapping.isEmpty()){
            return false;
        }
        //获取url请求地址路径
        String url = req.getRequestURI();
        String contextPath = req.getContextPath();
        //仅留资源定位符
        url = url.replace(contextPath,"").replaceAll("/+","/");

        //遍历映射集合
        for(Handler handler : handlerMapping){
            try{
                //通配url信息
                Matcher matcher = handler.pattern.matcher(url);
                //未匹配到 则继续遍历所有handler信息
                if( !matcher.matches()) {
                    continue;
                }

                //获取参数类型，进行参数的填充
                Class<?> [] paramTypes = handler.method.getParameterTypes();
                Object [] paramValues = new Object[paramTypes.length];

                //从 req中获取请求参数信息
                Map<String ,String[]> params = req.getParameterMap();
                for(Map.Entry<String,String[]> entry : params.entrySet()){
                    //替换掉 [ xxx,xxxx]中的 [ / ]信息
                    String value = Arrays.toString(entry.getValue()).replaceAll("\\]|\\[", "")
                            .replaceAll(",\\s", ",");
                    System.out.println( Arrays.toString(entry.getValue()).replaceAll("\\]|\\[", "")
                            .replaceAll(",\\s", ","));
                    if (!handler.paramMapping.containsKey(entry.getKey())) {
                        continue;
                    }

                    int index = handler.paramMapping.get(entry.getKey());

                    //类型转换
                    paramValues[index] = castStringValue(value,paramTypes[index]);
                }

                //默认方法中函数req ,resp
                int reqIndex = handler.paramMapping.get(HttpServletRequest.class.getName());
                paramValues[reqIndex] = req;

                int repIndex = handler.paramMapping.get(HttpServletResponse.class.getName());
                paramValues[repIndex] = resp;

                handler.method.invoke(handler.controller,paramValues);

                return true;
            }catch (Exception e) {
                throw e;
            }
        }

        return  false;
    }
    //扫描初始化配置信息
    private void doLoadConfig(String contextConfigLocation) {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation);
        try{
            contextConfig.load(in);
        }catch (IOException e) {
            throw new RuntimeException();
        }finally {
            if ( in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // 扫描路径下的类信息
    private void scanClass(String packageName) {
     //com.com.springmvc.v2  ---> /com/com/springmvc/v2 文件路径
        URL url = this.getClass().getClassLoader().getResource("/"+
        packageName.replaceAll("\\.","/"));
        File classDir = new File(url.getFile());
        //获取文件夹下所有的类信息
        for ( File file : classDir.listFiles()){
            if( file.isDirectory()) {
                scanClass(packageName + "." + file.getName());
            }else  {
                //仅获取.class信息
                if( !file.getName().endsWith(".class")){
                    continue;
                }
                String className = packageName + "." + file.getName().replaceAll(".class","");
                //加入至容器中进行保存
                classNames.add(className);
            }
        }
    }
    //实例化对象信息
    private void instance() {
        //避免空扫
        if (classNames.isEmpty()) {
            return;
        }
        //实例化扫描到的对象信息
        try {
            for (String className : classNames) {
                Class<?> bean = Class.forName(className);
                //具有controller注解信息
                if (bean.isAnnotationPresent(Controller.class)) {
                    //获取bean上控制注解信息
                    Controller controller = bean.getAnnotation(Controller.class);
                    String beanName = controller.value();
                    if ("".equals(beanName.trim())) {
                        //获取类名
                        beanName = toLowerFirstCase(bean.getSimpleName());
                    }
                    instanceMapping.put(beanName,bean.newInstance());
                    //处理接口装配
                } else if (bean.isAnnotationPresent(Service.class)) {
                    Service service = bean.getAnnotation(Service.class);
                    String beanName = service.value();
                    if ("".equals(beanName.trim())) {
                        //获取类名
                        beanName = toLowerFirstCase(bean.getSimpleName());
                    }
                    instanceMapping.put(beanName,bean.newInstance());
                    Class<?> [] interfaces = bean.getInterfaces();
                    for (Class<?> clazz : interfaces) {
                        System.out.println(clazz.getSimpleName());
                        instanceMapping.put(clazz.getName(),bean.newInstance());
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    //实现对象的自动注入处理
    private void autowired() {
        if( instanceMapping.isEmpty()) {
            return;
        }

        //获取instanceMapping中的实体信息
        for ( Map.Entry<String,Object> entry : instanceMapping.entrySet()){
            //获取类上的所有字段信息
            Field[] fieldss = instanceMapping.get(entry.getKey()).getClass().getFields();
            //遍历字段，判断是否存在Autowired注解
            for (Field field : fieldss) {
                if ( !field.isAnnotationPresent(Autowired.class)){
                    continue;
                }
                Autowired autowired = field.getAnnotation(Autowired.class);

                //注意：注解上可能会有value值，
                String beanName = autowired.value();
                if ( "".equals(beanName.trim())){
                    //获取的对象名：com.com.springMVC.V2.service.queryService
                    beanName = field.getType().getName();
                }

                //有注解信息，则需要进行值得注入
                field.setAccessible(true);
                try{
                    field.set(entry.getValue(),instanceMapping.get(beanName));
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //处理映射关系
    private void handlerMapping() {
        if( instanceMapping.isEmpty()){
            return;
        }
        for ( Map.Entry<String,Object> entry : instanceMapping.entrySet()){
            Class<?> clazz = entry.getValue().getClass();
            //仅处理Controller层的信息
            if ( !clazz.isAnnotationPresent(Controller.class)){
                continue;
            }
            String url = "";
            if ( clazz.isAnnotationPresent(RequestMapping.class)){
                RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                url = requestMapping.value();
            }

            //处理方法的映射
            //仅获取本类的方法，不处理从父类继承而来的方法
            Method [] methods = clazz.getDeclaredMethods();

            for(Method method : methods){
                if ( !method.isAnnotationPresent(RequestMapping.class)){
                    continue;
                }
                RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
                String customRegex = ("/" + url + requestMapping.value()).replaceAll("/+","/");

                //此处可根据自主定义来进行修改  适应性修改
                String regex = customRegex.replaceAll("\\*", ".*");
                Map<String,Integer> pm = new HashMap<String,Integer>();

                Annotation [][] paramets = method.getParameterAnnotations();
                for(int i = 0 ; i < paramets.length ;i ++ ){
                    for ( Annotation annotation: paramets[i]){
                        if ( annotation instanceof RequestParam){
                            String paraName = ((RequestParam) annotation).value();
                            if ( !"".equals(paraName.trim())){
                                pm.put(paraName,i);
                            }
                        }
                    }
                }
                //从request中获取参数
                Class<?> [] paramsTypes = method.getParameterTypes();
                for(int i = 0 ; i < paramsTypes.length ; i ++ ) {
                    Class<?> type = paramsTypes[i];
                    if(type == HttpServletRequest.class ||
                            type == HttpServletResponse.class){
                        pm.put(type.getName(), i);
                    }
                }

                handlerMapping.add(new Handler(Pattern.compile(regex),entry.getValue(),method, pm));
               /* System.out.println("Mapping " + customRegex + "  " + method);

              for(String key : pm.keySet()){
                  System.out.println(key + "    " + pm.get(key));
              }*/
            }


        }
    }
    //字符转化
    private String toLowerFirstCase(String simpleName) {
        //转成字符数组信息
        char [] chars = simpleName.toCharArray();

        //仅处理 A-Z 之间的字母信息
        if( chars[0] >= START && chars[0] <= END){
            chars[0] += 32;
        }
        return String.valueOf(chars);
    }

    class Handler{

        protected Pattern pattern;
        protected Object controller; //保存控制层实例信息
        protected Method method; //保存映射的方法
        protected Map<String,Integer> paramMapping; //参数序列信息

        protected Handler(Pattern pattern,Object controller,Method method,Map<String,Integer> paramMapping){
            this.pattern = pattern;
            this.controller = controller;
            this.method = method;
            this.paramMapping = paramMapping;
        }


    }
}
