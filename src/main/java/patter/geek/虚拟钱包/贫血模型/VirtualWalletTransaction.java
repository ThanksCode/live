package patter.geek.虚拟钱包.贫血模型;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 15:19
 **/
class VirtualWalletTransactionEntity{

    public void setAmount(BigDecimal amount) {
    }

    public void setCreateTime(long l) {
    }

    public void setFromWalletId(Long fromWalletId) {
    }

    public void setToWalletId(Long toWalletId) {
    }

    public void setStatus(String toBeExecuted) {
    }
}

public class VirtualWalletTransaction extends VirtualWalletTransactionEntity {
}
