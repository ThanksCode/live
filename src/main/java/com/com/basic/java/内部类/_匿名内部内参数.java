package com.com.basic.java.内部类;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-12 16:04
 **/
public class _匿名内部内参数 {

    public Defition fun(String dest){
        return new Defition() {
            private String llav = dest;
            @Override
            public void print() {
                System.out.println(dest);
            }
        };
    }

    public static void main(String[] args) {
        _匿名内部内参数 p1 = new _匿名内部内参数();
        Defition d1 = p1.fun("zhangSan");
        d1.print();
    }
}
