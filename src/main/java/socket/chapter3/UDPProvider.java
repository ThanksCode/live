package socket.chapter3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * udp提供方
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-25 11:44
 **/
public class UDPProvider {

    public static void main(String[] args) throws IOException {
        System.out.println("UDPProvider Started");

        // 作为接受者 指定一个端口用于数据接受数据 端口 20000
        DatagramSocket ds = new DatagramSocket(30000);
        // 构建接受实体
        final byte [] buf = new byte[512];
        DatagramPacket receivePack = new DatagramPacket(buf,0,buf.length);
        // 接受信息
        ds.receive(receivePack);
        // 打印接收到的信息与发送者的信息
        // 发送者ip
        String ip = receivePack.getAddress().getHostAddress();
        int port = receivePack.getPort();
        int dateLen = receivePack.getLength();
        String data = new String(receivePack.getData(),0,dateLen);

        System.out.println("UDPProvider receive from ip " + ip
        + "\t port :" + port + "\t data :" + data);

        //构建一份回送数据
        String responseData =  "Receive data with len : " + dateLen;
        //回送信息实体
        byte [] responseBytes = responseData.getBytes();
        // 构建回送实体
        DatagramPacket responsePacket = new DatagramPacket(responseBytes,
                responseBytes.length,
                receivePack.getAddress(),
                receivePack.getPort());
        ds.send(responsePacket);

        System.out.println("UDPProvider Finished .");
        ds.close();
    }
}
