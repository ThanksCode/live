package mybatisDemo.基础模块.类型转换;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-21 12:53
 **/
public class 代理 {

    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        map.put("10","2");
        map.clear();
        System.out.println(map.isEmpty());
    }
}
