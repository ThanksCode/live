package com.com.basic.java;

/***
 * 重载中泛型和具体类型同时存在会调用哪个方法？
 *
 */

import java.io.Serializable;

import static javafx.scene.input.KeyCode.T;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-17 17:42
 **/
public class _不同重载类型 {

    public static void main(String[] args) {
        ServerResponse serverResponse = new ServerResponse<>();
        serverResponse.createBySuccessMessage("String 数据");
    }


    public static class ServerResponse<T> implements Serializable {
        private int status;
        private String msg;
        private T data;

        public  void createBySuccessMessage(String msg){
            System.out.println("String 类型数据");
        }

        public  void  createBySuccessMessage(T data){
            System.out.println("泛型数据 类型数据");
        }

    }


    private static class User{
        public User(){
            System.out.println("构建user对象");
        }
    }
}
