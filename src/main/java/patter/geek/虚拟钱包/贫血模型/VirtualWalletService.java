package patter.geek.虚拟钱包.贫血模型;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 15:00
 **/
class VirtualWalletEntity{

    public BigDecimal getBalance() {
        return new BigDecimal(1);
    }
}
public class VirtualWalletService {
    // 通过构造函数或者 IOC 框架注入

    private VirtualWalletRepository walletRepo = new VirtualWalletRepository();
    private VirtualWalletRepository virtualWalletRepo = new VirtualWalletRepository();
    private VirtualWalletTransactionRepository transactionRepo ;

    // 获取虚拟钱包业务对象
    public VirtualWalletBo getVirtualWallet(Long walletId) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWalletBo walletBo = convert(walletEntity);
        return walletBo;
    }

    //  根据传入的钱包实体 产生一个虚拟钱包的业务对象信息
    private VirtualWalletBo convert(VirtualWalletEntity walletEntity) {
            return new VirtualWalletBo();
    }

    public BigDecimal getBalance(Long walletId) {
        return virtualWalletRepo.getBalance(walletId);
    }

    // 扣钱
    public void debit(Long walletId, BigDecimal amount) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        BigDecimal balance = walletEntity.getBalance();
        if (balance.compareTo(amount) < 0) {
            //throw new NoSufficientBalanceException();
        }
        walletRepo.updateBalance(walletId, balance.subtract(amount));
    }


    //   加钱
    public void credit(Long walletId, BigDecimal amount) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        BigDecimal balance = walletEntity.getBalance();
        walletRepo.updateBalance(walletId, balance.add(amount));
    }

    /***
     * 添加了一些跟 transaction 相关的记录和状态更新的代码
     * @param fromWalletId
     * @param toWalletId
     * @param amount
     */
    // 事务控制
    public void transfer(Long fromWalletId, Long toWalletId, BigDecimal amount) {
        VirtualWalletTransactionEntity transactionEntity = new VirtualWalletTransaction();

        // 事物实体类型 为实体增设事务控制
        transactionEntity.setAmount(amount);
        transactionEntity.setCreateTime(System.currentTimeMillis());
        transactionEntity.setFromWalletId(fromWalletId);
        transactionEntity.setToWalletId(toWalletId);
        // 设定状态为待执行
        transactionEntity.setStatus(Status.TO_BE_EXECUTED);
        Long transactionId = transactionRepo.saveTransaction(transactionEntity);
        try {
            // 两个事务并不是在同一个连接中进行处理
            debit(fromWalletId, amount);
            credit(toWalletId, amount);
        } catch (Exception e) {
            transactionRepo.updateStatus(transactionId, Status.FAILED);
        }
        transactionRepo.updateStatus(transactionId, Status.EXECUTED);
    }


}
