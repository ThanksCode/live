package offer;

/***
 * 统计一个数字在排序数组中出现的次数。
 *
 *
 *
 * 示例 1:
 *
 * 输入: nums = [5,7,7,8,8,10], target = 8
 * 输出: 2
 * 示例 2:
 *
 * 输入: nums = [5,7,7,8,8,10], target = 6
 * 输出: 0
 *
 * #date  2020-3-22
 */
public class _10_26_在有序数组中查找元素 {

    public int search(int[] nums, int target) {
        int cn =  0 ;
        //找到元素即停止循环
        for( int i = 0 ; i <  nums.length ; i ++ ) {
            if ( nums[i] == target) {
                cn ++ ;
            }

            if ( nums[i] > target) {
                break;
            }
        }
        return  cn ;
    }
}
