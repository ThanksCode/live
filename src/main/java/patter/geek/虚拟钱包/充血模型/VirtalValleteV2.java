package patter.geek.虚拟钱包.充血模型;

import patter.geek.虚拟钱包.贫血模型.InsufficientBalanceException;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 15:35
 **/
public class VirtalValleteV2 {

    private Long id;
    private Long createTime = System.currentTimeMillis();;
    private BigDecimal balance = BigDecimal.ZERO;
    private boolean isAllowedOverdraft = true;
    private BigDecimal overdraftAmount = BigDecimal.ZERO;
    private BigDecimal frozenAmount = BigDecimal.ZERO;
    public VirtalValleteV2(Long preAllocatedId) {
        this.id = preAllocatedId;
    }
    public void freeze(BigDecimal amount) {  }
    public void unfreeze(BigDecimal amount) { }
    public void increaseOverdraftAmount(BigDecimal amount) {  }
    public void decreaseOverdraftAmount(BigDecimal amount) {  }
    public void closeOverdraft() {  }
    public void openOverdraft() {  }
    public BigDecimal balance() {
        return this.balance;
    }

    public BigDecimal getAvaliableBalance() {
        BigDecimal totalAvaliableBalance = this.balance.subtract(this.frozenAmount);
        if (isAllowedOverdraft) {
            totalAvaliableBalance = totalAvaliableBalance.add(this.overdraftAmount) ;
        }
        return totalAvaliableBalance;
    }


    public void debit(BigDecimal amount) throws InsufficientBalanceException {
        BigDecimal totalAvaliableBalance = getAvaliableBalance();
        if (totalAvaliableBalance .compareTo(amount) < 0) {
            throw new InsufficientBalanceException();
        }
        this.balance.subtract(amount);
    }
    public void credit(BigDecimal amount) throws InvalidAmountException {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException();
        }
        this.balance.add(amount);
    }
}
