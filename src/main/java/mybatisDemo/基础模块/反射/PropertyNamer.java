package mybatisDemo.基础模块.反射;

import java.util.Locale;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-16 10:19
 **/
public class PropertyNamer {
   // Prevent Instantiation of Static Class
    private PropertyNamer() {

    }

    // 截取get或者is后的参数名
    public static String methodToProperty(String name) {
        if (name.startsWith("is")) {
            name = name.substring(2);
        } else if (name.startsWith("get") || name.startsWith("set")) {
            name = name.substring(3);
        } else {
            throw new RuntimeException("Error parsing property name '" + name + "'.  Didn't start with 'is', 'get' or 'set'.");
        }
        // 将参数的第一个字符信息改成小写信息
        if (name.length() == 1 || (name.length() > 1 && !Character.isUpperCase(name.charAt(1)))) {
            name = name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
        }

        return name;
    }

    public static boolean isProperty(String name) {
        return name.startsWith("get") || name.startsWith("set") || name.startsWith("is");
    }

    public static boolean isGetter(String name) {
        return name.startsWith("get") || name.startsWith("is");
    }

    public static boolean isSetter(String name) {
        return name.startsWith("set");
    }
}
