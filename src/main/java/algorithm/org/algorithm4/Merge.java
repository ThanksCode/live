package algorithm.org.algorithm4;

/****
 * 归并排序
 */

import java.util.Arrays;
import java.util.Random;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-18 21:56
 **/
public class Merge {

    public void mergeSort(int [] arr , int lo,int hi){
        if ( lo < hi) {
            int mid = lo + (hi - lo) / 2;
            // 处理前半段  不能 传入 mid - 1!  mid位置处的元素并不是有序的
            mergeSort(arr,lo,mid );
            mergeSort(arr,mid +1,hi);
            merge(arr,lo,mid,hi);
        }
    }

    /***
     * 自底向上排序
     * @param arr
     */
    public void mergeSort(int [] arr){

        // 间隔选取 1  2  4 8 -------
        for ( int i = 1; i < arr.length ; i = i + i ){
            for( int lo = 0 ; lo < arr.length - i ; lo += i + i){
                // 注意：中间元素的选取规则
                merge(arr,lo,lo + i - 1,Math.min(lo + i + i - 1, arr.length - 1));
            }
        }
    }


    public void mergeSortV1(int [] arr) {
        // 每次倍增数据信息
        for( int i = 1 ; i < arr.length ; i += i){
            // 处理归并段的数据信息
            for(int j = 0 ; j < arr.length - i ; j += i + i){
                // 中间元素应该为多少？
                merge(arr,j,j + i- 1,Math.min(j +i +i -1 ,arr.length - 1));
            }
        }
    }


    public void merge(int [] arr , int lo , int mid , int hi){
        // 辅助数组
        int [] aux = new int [arr.length];
        for( int i = 0 ; i < arr.length ; i ++ ) {
            aux[i] = arr[i];
        }
        // 此处从mid+1开始，而不是 直接 j = hi
        //  hi 为后半段结束标识符
        int i = lo , j = mid + 1;
        // 比较归并处理
        for( int k = lo ; k <= hi ; k ++ ) {
            if ( i > mid) {
                arr[k] = aux[j++];
            }else if ( j > hi) {
                arr[k] = aux[i++];
            }else if ( aux[i] > aux[j]){
                arr[k] = aux[j++];
            }else{
                arr[k] = aux[i++];
            }
        }
    }

    // 判断数据是否排序
    public boolean compare(int [] arr) {
        for ( int i = 1 ; i < arr.length  ; i ++ ) {
            if ( arr[i - 1] > arr[i]){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Merge merge  = new Merge();
        int [] arr = new int [1000];
        Random random = new Random();

        for (int i = 0 ; i < arr.length; i ++ ) {
            arr[i] = random.nextInt(1000);
        }
        int [] arr1 = {5,4,3,2,1};
        System.out.println("before   "  + merge.compare(arr1));
        //merge.mergeSort(arr,0,arr.length - 1);
        merge.mergeSortV1(arr1);
        System.out.println("after   " + merge.compare(arr1));
        System.out.println(Arrays.toString(arr1));
    }
}
