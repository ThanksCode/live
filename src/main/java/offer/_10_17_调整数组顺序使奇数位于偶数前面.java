package offer;

import java.util.Arrays;

/**
 * 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有奇数位于数组的前半部分，所有偶数位于数组的后半部分。
 *
 *  
 *
 * 示例：
 *
 * 输入：nums = [1,2,3,4]
 * 输出：[1,3,2,4]
 * 注：[3,1,2,4] 也是正确的答案之一。
 *  
 *
 * 提示：
 *
 * 1 <= nums.length <= 50000
 * 1 <= nums[i] <= 10000
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/diao-zheng-shu-zu-shun-xu-shi-qi-shu-wei-yu-ou-shu-qian-mian-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @date 2020-2-22
 * 解题：利用快排分化左右区间进行数据的填充
 */
public class _10_17_调整数组顺序使奇数位于偶数前面 {

    public int[] exchange(int[] nums  ,int index ) {
        //处理特殊情况
        if ( nums == null ||  nums.length == 0){
            return nums;
        }

        int point = nums[0];
        int left = 0 , right = nums.length - 1;


        while ( left < right) {

            while ( left < right && nums[right] % 2 == 0){
                right -- ;
            }
            //表明遇到了奇数，此时需要将数据放在前半部分！
            if ( left < right) {
                nums[left] = nums[right];
                left ++ ;
            }

            while ( left < right && nums[left] % 2 != 0) {
                left ++ ;
            }

            //表名此时前半段遇到了偶数
            if ( left < right) {
                nums[right] = nums[left];
                right -- ;
            }
        }

        nums[left] = point;


        return nums;
    }


    // 奇数在前 偶数在后
    public int[] exchange(int[] nums){
        //处理特殊情况
        if ( nums == null ||  nums.length == 0){
            return nums;
        }

        // 双指针标定位置
        int begin = 0  , end = nums.length;

        // 循环处理数据
        while ( begin < end ) {

            // 奇数的话向后移动
            while ( begin < end && (nums[begin] & 0x1) != 0) {
                begin ++ ;
            }

            // 处理偶数
            while ( begin < end && (nums[end] & 0x1) == 0){
                end --;
            }

            // 交换数据
            if ( begin < end ) {
                swap(nums,begin,end);

            }
        }
        return nums;
    }

    public void swap(int [] num , int i , int j ) {
        int tmp = num[i];
        num[i] = num[j];
        num[j]  = tmp;
    }

    public static void main(String[] args) {
        _10_17_调整数组顺序使奇数位于偶数前面 demo = new _10_17_调整数组顺序使奇数位于偶数前面();
        int [] arr = {1,2,3,5};
        demo.swap(arr,0,3);
        System.out.println(Arrays.toString(arr));

    }


}
