package offer;

import java.util.ArrayList;
import java.util.List;

/***
 * 0,1,,n-1这n个数字排成一个圆圈，从数字0开始，每次从这个圆圈里删除第m个数字。求出这个圆圈里剩下的最后一个数字。
 *
 * 例如，0、1、2、3、4这5个数字组成一个圆圈，从数字0开始每次删除第3个数字，则删除的前4个数字依次是2、0、4、1，因此最后剩下的数字是3。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * 输入: n = 5, m = 3
 * 输出: 3
 * @date 2020-2-29
 */
public class _10_21_圆圈中最后剩下的数字 {

    public int lastRemaining(int n, int m) {
        List<Integer> list = new ArrayList<>();

        for ( int i = 0 ; i < n ; i ++ ){
            list.add(i);
        }

        int index = (m - 1 ) % n;

        while ( list.size() != 1) {
            list.remove(index);
            //此处使用list.size()信息，避免删除元素后内容长度发生的改变
            index = (index + m - 1) % list.size();
        }

        return list.get(0);

    }
}
