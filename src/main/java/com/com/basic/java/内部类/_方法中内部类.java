package com.com.basic.java.内部类;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-12 15:48
 **/
public class _方法中内部类 {

    public Defition fun( ){
        class Definition implements Defition {
            @Override
            public void print() {
                System.out.println("definition");
            }
        }
        return new Definition();
    }

    public static void main(String[] args) {
        _方法中内部类 demo = new _方法中内部类();
        demo.fun().print();
    }
}
