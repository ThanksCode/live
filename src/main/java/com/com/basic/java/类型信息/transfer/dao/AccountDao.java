package com.com.basic.java.类型信息.transfer.dao;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-25 15:36
 **/
public interface AccountDao {

    void transfer(String from , String to , int money);

    void add(String name);
}
