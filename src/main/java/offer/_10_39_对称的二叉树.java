package offer;

/***
 * 请实现一个函数，用来判断一棵二叉树是不是对称的。如果一棵二叉树和它的镜像一样，那么它是对称的。
 *
 * 例如，二叉树 [1,2,2,3,4,4,3] 是对称的。
 *
 *     1
 *    / \
 *   2   2
 *  / \ / \
 * 3  4 4  3
 *
 * 但是下面这个 [1,2,2,null,3,null,3] 则不是镜像对称的:
 *
 *     1
 *    / \
 *   2   2
 *    \   \
 *    3    3
 *
 *  
 *
 * 示例 1：
 *
 * 输入：root = [1,2,2,3,4,4,3]
 * 输出：true
 * 示例 2：
 *
 * 输入：root = [1,2,2,null,3,null,3]
 * 输出：false
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/dui-cheng-de-er-cha-shu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/***
 *  *     1
 *  *    / \
 *  *   2   2
 *  *  / \ / \
 *  * 3  4 4  3
 *  分析
 *  前序遍历：
 *      1 2 3 4 2 4 3
 *  重新定义一种遍历
 *      根 --> 右 --> 左
 *     1 2 4 3 2 4 3
 *   发现其相似----> 破解关键
 *  对于null的情况返回false
 *        1
 *  *    / \
 *  *   2   2
 *  *    \   \
 *  *    3    3
 *
 *  前序：1 2 3 2 3
 *  重新定义的遍历
 *      1 2 3 2 3 此时虽然相同但不符合定义
 *   1 2 null 3 2 null 3
 *   1 2 3 null 2 3 null
 *   此时并符合定义
 *
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-12 18:11
 **/
public class _10_39_对称的二叉树 {

    public boolean isSymmetric(TreeNode root) {
        return isSymmetric(root,root);
    }

    private boolean isSymmetric(TreeNode r1 ,TreeNode r2){
        // 同时为null 返回 true
        if ( r1 == null && r2 == null){
            return true;
        }

        // 一方为null,另一方不确定返回false
        if( r1 == null  || r2 == null ){
            return false;
        }

        // 如果值不相同，依然不能算是对称二叉树
        if( r1.val != r2.val) {
            return false;
        }

        // 递归处理后序内容
        return isSymmetric(r1.left,r2.right)
                && isSymmetric(r1.right,r2.left);
    }
}
