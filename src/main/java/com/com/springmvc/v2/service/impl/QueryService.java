package com.com.springmvc.v2.service.impl;

import com.com.springmvc.v2.annotation.Service;
import com.com.springmvc.v2.service.IQueryService;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:58
 **/
@Service
public class QueryService implements IQueryService {

    @Override
    public String search(String name) {
        return "invork search name = " + name;
    }

}
