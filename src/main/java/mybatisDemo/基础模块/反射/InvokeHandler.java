package mybatisDemo.基础模块.反射;

import java.lang.reflect.InvocationTargetException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-16 10:07
 **/
public interface InvokeHandler {
    Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException;

    Class<?> getType();
}
