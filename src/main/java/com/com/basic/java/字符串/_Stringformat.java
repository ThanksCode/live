package com.com.basic.java.字符串;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-19 13:36
 **/
public class _Stringformat {
    public static void main(String[] args) {
        String msg = "姓名%s  密码%s";
        String str = String.format(msg,":zhangSan",":123");
        System.out.println(str);
    }
}
