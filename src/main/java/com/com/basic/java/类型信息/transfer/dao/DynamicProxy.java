package com.com.basic.java.类型信息.transfer.dao;

import com.com.basic.java.类型信息.transfer.dao.impl.AccountDaoImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-25 15:43
 **/
public class DynamicProxy implements InvocationHandler {


    private AccountDao accountDao;
    //获得一个代理后的实例对象
    public Object getInstance( AccountDao accountDao){
        this.accountDao = accountDao;
        Class tclass = accountDao.getClass();
        return Proxy.newProxyInstance(tclass.getClassLoader(),tclass.getInterfaces(),
                this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {

        String methoName = method.getName();

        if ( methoName.equals("transfer")){
            System.out.println("开启事务控制");
            try{
                return method.invoke(this.accountDao,args);
            }catch (Exception e){
                //e.printStackTrace();
                if ( e != null) {
                    System.out.println("异常发生");
                    e.printStackTrace();
                }
            }
        }else{
            System.out.println("无需进行事务控制");
        }

        return null;
    }
}
