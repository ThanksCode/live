package com.com.springmvc.v2.service;

import com.com.springmvc.v2.annotation.Service;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:57
 **/

public interface IQueryService {
    public String search(String name);
}
