package socket.chapter5.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * 关闭工具类
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-06 11:23
 **/
public class CloseUtils {
    public static void close(Closeable... closeables) {
        if (closeables == null) {
            return;
        }
        for (Closeable closeable : closeables) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
