package com.com.basic.java;


import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.TreeSet;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-13 10:28
 *
 */


public class Reflection {

    public static Set<String> analyze(Class<?> enumClass){
        System.out.println("print methos ");
        Set<String> methods = new TreeSet<>();

        for (Method m: enumClass.getMethods()){
            methods.add(m.getName());
        }
        return methods;
    }

    public static void main(String[] args) throws IOException {
        Set<String> rs = analyze(Demo.class);
        System.out.println(rs);
    }
}

