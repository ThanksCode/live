package patter.geek.openClose;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 17:18
 **/
public class Demo {

    public static void main(String[] args) {

        AlertInof info = new AlertInof();
        ApplicationContextAlert applicationContextAlert = ApplicationContextAlert.getInstance();

        applicationContextAlert.getAlert().check(info);
    }
}
