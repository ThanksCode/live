package socket.Demo.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 18:29
 **/
public class TCPServer {
    private final int port;
    private ClientListener mListener;

    public TCPServer(int port) {
        this.port = port;
    }

    // 启动
    public boolean start() {
        try {
            ClientListener listener = new ClientListener(port);
            mListener = listener;
            listener.start();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // 停止
    public void stop() {
        if (mListener != null) {
            mListener.exit();
        }
    }

    private static class ClientListener extends Thread{

        private ServerSocket server;
        private boolean done = false;

        private ClientListener(int port) throws IOException {
            server = new ServerSocket(port);
            System.out.println("服务器信息：" + server.getInetAddress() + " P:" + server.getLocalPort());
        }


        @Override
        public void run() {
            super.run();

            System.out.println("------服务器准备就绪，等待连接操作------");
            do {
                Socket client = null;
                try{
                    client = server.accept();
                }catch (Exception ignore){
                    continue;
                }
                // 异步创建线程信息
                ClientHandler clientHandler = new ClientHandler(client);
                // 启动
                clientHandler.start();
            }while (!done);

            System.out.println("------服务器已关闭------");
        }

        void exit() {
            done = true;
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 客户端消息处理
     */
    private static class ClientHandler extends Thread{
        private Socket socket;
        private boolean flag = true;

        ClientHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            super.run();

            System.out.println("新客户端连接：" + socket.getInetAddress() +
                    " P:" + socket.getPort());

            try {
                // 打印来自客户端的信息

                // 客户端输入流信息
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader
                        (socket.getInputStream()));

                PrintStream socketOutput = new PrintStream(socket.getOutputStream());
               do{

                   String str = bufferedReader.readLine();
                   // 判断是否结束
                   if ("bye".equalsIgnoreCase(str)) {
                       flag = false;
                       // 回送
                       socketOutput.println("bye");
                   } else {
                       // 打印到屏幕。并回送数据长度
                       System.out.println(str);
                       socketOutput.println("回送信息长度 ：" + str.length());
                   }
               }while (flag);

               socketOutput.close();
               bufferedReader.close();

            } catch (Exception e) {
                System.out.println("连接异常断开");
            } finally {
                // 连接关闭
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // 退出后输出连接信息
            System.out.println("客户端已退出：" + socket.getInetAddress() +
                    " P:" + socket.getPort());
        }

    }
}
