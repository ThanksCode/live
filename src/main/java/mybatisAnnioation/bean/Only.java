package mybatisAnnioation.bean;

import mybatisAnnioation.annotation.Param;
import mybatisAnnioation.annotation.Query;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-08-01 17:15
 **/
public interface Only {

    @Query("select id,username from stu where id = #{id} and username = #{name}")
    void selectUser(@Param("id")Integer id , @Param("name")String name);

}
