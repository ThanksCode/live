package com.com.basic.java;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-24 15:02
 **/
public class _输出信息 {

    public static void main(String[] args) {
        int [] arr = {1,2,3,4,5,6,7};
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < arr.length ; i ++ ) {
            sb = (i == arr.length - 1 )? sb.append(arr[i]+"") : sb.append(arr[i]+",");
        }

        System.out.println(sb.toString());

    }
}
