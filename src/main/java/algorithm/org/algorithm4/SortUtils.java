package algorithm.org.algorithm4;

import java.util.Random;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-27 18:50
 **/
public class SortUtils {
    // 检验数组是否有序
    public static boolean compare(int [] arr) {
        for ( int i = 1 ; i < arr.length  ; i ++ ) {
            if ( arr[i - 1] > arr[i]){
                return false;
            }
        }
        return true;
    }

    public static int [] createArray(int n ){
        int [] arr = new int [n];
        Random random = new Random();

        for (int i = 0 ; i < arr.length; i ++ ) {
            arr[i] = random.nextInt(1000);
        }
        return arr;
    }

}
