package algorithm.org.linklist;

import offer.ListNode;

import java.util.List;

/**
 * 反转一个单链表。
 *
 * 示例:
 *
 * 输入: 1->2->3->4->5->NULL
 * 输出: 5->4->3->2->1->NULL
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-07 14:22
 **/
public class _206_反转链表 {

    //思路：用一个 prev 节点保存向前指针，temp 保存向后的临时指针
    public ListNode reverseList(ListNode head) {
        if ( head == null || head.next == null) {
            return head;
        }

        ListNode pre = head;
        ListNode last = pre.next;
        // 结点指向空
        pre.next = null;
        // 循环遍历
        while ( last != null){
            // 记录last后序信息
            ListNode next = last.next;
            // 指向头结点
            last.next = pre;
            // 重更新结点信息
            pre = last;
            // 下一个待遍历结点
            last = next;
        }

        return pre;
    }

    public ListNode reverseListV1(ListNode head) {
        if ( head == null) {
            return null;
        }
        // 记录第一个结点信息

        ListNode pre = head;
        ListNode last = head.next;
        // 此时变成  head-null  之后采用头插法对元素进行逆序处理
        pre.next = null;

        // 遍历操作
        while ( last != null) {
            // 记录当前结点后继结点信息，因为会导致断链操作
            ListNode tmp = last.next;
            // 后续信息指向前驱结点
            last.next  = pre;
            // 跟新 pre信息
            pre = last;
            last = tmp;
        }



        return pre;
    }
}
