package mybatisDemo.基础模块.类型转换;

import java.lang.reflect.Type;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 10:16
 **/
public class IntegerTypeHandler extends BaseTypeHandler<Integer> {

    public static void main(String[] args) {
        IntegerTypeHandler handler = new IntegerTypeHandler();
        Type rs = handler.getRawType();
        System.out.println(rs);
    }
}
