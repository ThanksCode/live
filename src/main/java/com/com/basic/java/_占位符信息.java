package com.com.basic.java;

import java.text.MessageFormat;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-23 19:55
 **/
public class _占位符信息 {

    //两种不同的占位形式
    public static void main(String[] args) {
        String msg = "姓名{0}  密码{1}";

        System.out.println(MessageFormat.format(msg,"username","password"));
        String msg1 = "姓名%s  密码%s";
        System.out.println(String.format(msg1, "username","password"));
    }
}
