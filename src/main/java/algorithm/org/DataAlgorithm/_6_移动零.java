package algorithm.org.DataAlgorithm;

/***
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 *
 * 示例:
 *
 * 输入: [0,1,0,3,12]
 * 输出: [1,3,12,0,0]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/move-zeroes
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-26 17:18
 **/
public class _6_移动零 {

    // 题意将0移到末尾

    public void moveZeroes(int[] nums) {

        // 当下下标
        int cur = 0;
        for( int i = 0 ; i < nums.length ; i ++ ) {
            if ( nums[i] != 0){
                nums[cur++] = nums[i];
            }
        }
        // 填充后半段
        for(; cur < nums.length ; ){
            nums[cur++] = 0;
        }
    }


    public void partionV1(int [] nums) {
        if(nums==null) {
            return;
        }
        //两个指针i和j
        int j = 0;
        for ( int i = 0 ; i < nums.length ; i ++ ) {
            //当前元素!=0，就把其交换到左边，等于0的交换到右边
            if ( nums[i] != 0 ) {
                int tmp = nums[i];
                nums[i] = nums[j];
                nums[j++] = tmp;
            }
        }
    }

    public void partion(int [] nums){
        // 标定点
        int left = 0 ,right = nums.length - 1;
        // 处于循环中处理信息
        while( left < right) {
            // 寻找0元素
            while( left < right && nums[left] != 0 ) {
                left ++;
            }

            while ( left < right && nums[right] == 0 ){
                right -- ;
            }
            swap(nums,left,right);
            left ++ ;
            right -- ;
        }

    }

    private void swap(int [] nums , int i , int j ){
        int tmp = nums[j];
        nums[j] = nums[i];
        nums[i] = tmp;
    }
}
