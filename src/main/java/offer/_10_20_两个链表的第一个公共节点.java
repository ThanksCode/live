package offer;



import java.util.TreeSet;

/***
 * 输入两个链表，找出它们的第一个公共节点。
 *
 * 如下面的两个链表：
 * 输入：intersectVal = 8, listA = [4,1,8,4,5], listB = [5,0,1,8,4,5], skipA = 2, skipB = 3
 * 输出：Reference of the node with value = 8
 * 输入解释：相交节点的值为 8 （注意，如果两个列表相交则不能为 0）。从各自的表头开始算起，链表 A 为 [4,1,8,4,5]，链表 B 为 [5,0,1,8,4,5]。在 A 中，相交节点前有 2 个节点；在 B 中，相交节点前有 3 个节点。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/liang-ge-lian-biao-de-di-yi-ge-gong-gong-jie-dian-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * 题目分析：
 *  即从给定的两个链表中找到两个链表的公共结点
 *      难点：两个链表的长度可能并不一样，所以如何处理公共结点信息尤为重要
 *  题目要求：
 *      不破坏链表结构，返回链表相交的节点处，时间复杂度尽量为O(n)
 *
 *
 *
 * 解法：
 *      两种：利用hasMap存储结点地址，遍历两个链表，当出现相同的结点时及遇到的相同结点
 *
 *      长度相差法：
 *          设计两个指针：
 *     思考一个问题：本题和公共祖先之间有何区别和联系？
 */
public class _10_20_两个链表的第一个公共节点 {

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        TreeSet<Integer> res = new TreeSet<>();
        ListNode cur = headA;
        ListNode cur1 = headB;

        if ( headA == null || headB == null){
            return null;
        }



        while ( cur != null){

            res.add(cur.val);
            cur = cur.next;
        }

        while( cur1 != null){

            if ( res.add(cur1.val) == false){

                return cur1;
            }
            cur1 = cur1.next;
        }
        return null;

    }
}
