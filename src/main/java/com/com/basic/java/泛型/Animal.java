package com.com.basic.java.泛型;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-26 13:01
 **/
public interface Animal {

    void run();
}
