package socket.chapter2;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

/**
 * @program: socket
 * @description
 * @author: yihang
 * @create: 2020-06-25 11:28
 **/
public class Client {

    public static void main(String[] args) throws IOException {
        //利用Socket对象进行处理
        Socket socket = new Socket();

        // 设定超时等待时间
        socket.setSoTimeout(3000);

        // 连接指定端口  所需内容 ip地址  端口号   设定端口互连信息
        socket.connect(new InetSocketAddress(Inet4Address.getLocalHost(),2000),30000);

        // 打印连通信息
        System.out.println("已发起服务器连接，并进入后续流程～");
        System.out.println("客户端信息：" + socket.getLocalAddress() + " P:" + socket.getLocalPort());
        System.out.println("服务器信息：" + socket.getInetAddress() + " P:" + socket.getPort());

        try {
            // 发送接收数据
            todo(socket);
        } catch (Exception e) {
            System.out.println("异常关闭");
        }

        // 释放资源
        socket.close();
        System.out.println("客户端已退出～");
    }

    /***
     * 获取数据信息，交付与服务器端
     * @param client
     */
    private static void todo(Socket client) throws IOException {
        // 得到键盘输入流对象
        InputStream in = System.in;
        // 将输入流封装
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        // 获取服务器输出流
        OutputStream outputStream = client.getOutputStream();
        // 转化为打印流
        PrintStream socketPrintStream = new PrintStream(outputStream);

        // 得到Socket输入流，并转换为BufferedReader
        InputStream inputStream = client.getInputStream();
        BufferedReader socketBufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        //  循环及检测输入内容
        boolean flag = true;

        while ( flag ) {
            // 获取内容信息
            String readStr = reader.readLine();
            // 发送到服务器
            socketPrintStream.println(readStr);

            // 从服务器读取一行
            String echo = socketBufferedReader.readLine();

            if ("bye".equalsIgnoreCase(echo)) {
                flag = false;
            }else {
                System.out.println(echo);
            }
        }

        // 资源释放
        socketPrintStream.close();
        socketBufferedReader.close();


    }
}
