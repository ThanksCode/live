package com.jdbc.dao;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.ResultSet;

/**
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-09 18:31
 **/
public class RowHandler {

    private Class clazz ;
    private Field fields [];
    public RowHandler(Class clazz) {
        this.clazz = clazz;
        fields = clazz.getDeclaredFields();

    }

    //获取到结果集合
    public Object map(ResultSet rs ) throws  Exception{
        Object obj = clazz.newInstance();
        while ( rs.next() ) {
            for ( int i = 0 ; i < fields.length; i ++ ){
                Field field = fields[i];
                field.setAccessible(true);
                Object param = rs.getObject(field.getName());
                field.set(obj,param);
            }
        }
        return obj;
    }
}
