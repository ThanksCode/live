package com.com.springmvc.v2.service;

import com.com.springmvc.v2.annotation.Service;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:56
 **/

public interface IModifyService {
    public String add(String name,String addr);
    public String remove(Integer id);

}
