package patter.creational.proxy.auto;

import java.lang.reflect.Method;

/***
 * 自主提供InvocationHandler类信息
 * 需要提供的参数信息也仅仅是invoke方法
 */
public  interface InvocationHandler_s {
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
}
