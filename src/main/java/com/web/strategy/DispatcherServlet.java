package com.web.strategy;

import javassist.*;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LocalVariableAttribute;
import javassist.bytecode.MethodInfo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * 23:50v1 v2全部完成
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-10 18:37
 **/
public class DispatcherServlet extends HttpServlet {

    private List<Handler> handlers = new ArrayList<>();

    @Override
    public void init() throws ServletException {
        try{
            //针对特定类进行处理
            Class clazz = UserController.class;

            //获取所有方法
            Method [] methods = clazz.getDeclaredMethods();
            for( int i = 0 ; i < methods.length ; i ++ ) {
                //创建一个handler对象
                Handler handler = new Handler();
                handler.setController(clazz.newInstance());
                //获取方法
                handler.setMethod(methods[i]);
                //获取方法名
                handler.setUrl("/" + methods[i].getName());
                handlers.add(handler);
            }
        }catch (Exception e ){
            throw  new RuntimeException();
        }
    }

    private void doDispatcher(HttpServletRequest request , HttpServletResponse response) throws  Exception{

        //获取请求资源的方法
        //http://localhost/save
        //结果  /save
        String  uri = request.getRequestURI();
        //根据不同的URi信息进行方法分发，
        //如果原始的方法必定是if-else 进行判断 ， 此时优化成策略模式的应用
        Handler handler = null;
        //根据uri信息断定所需要指定的控制器
        for ( Handler h : handlers) {
            if ( uri.equals(h.getUrl())){
                handler = h;
                break;
            }
        }

        //v.1版本执行无参方法
        /*if ( handler != null) {
            Object obj = null;
            //Object [] rs =  new Object[2];
            try {
                //执行特定方法
                 obj = handler.getMethod().invoke(handler.getController());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }*/
        //拓展v.2 版本信息
        System.out.println(uri);
        //主要措施获取参数信息
        if( handler != null) {
            Class clazz = UserController.class;
            byte [] bytes = ObjectChangeToByte.changeToByte(handler.getController());
            //获取类本身的方法，不获取继承而来的方法
            ClassPool pool = ClassPool.getDefault();
            pool.insertClassPath(new ClassClassPath(this.getClass()));
            //获取类本身的方法，不获取继承而来的方法
            Method [] methods = clazz.getDeclaredMethods();
            //pool.insertClassPath(new ByteArrayClassPath(clazz.getName(),bytes));

            CtClass ctClass = pool.get(clazz.getName());
            //获取方法名
            String methodName = handler.getMethod().getName();
            try{
                CtMethod ctMethod = ctClass.getDeclaredMethod(methodName);
               if ( ctClass == null) {
                   System.out.println("null");
               }
                MethodInfo methodInfo = ctMethod.getMethodInfo();
                CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
                LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
                if ( attr == null) {
                    //exception
                }
                String [] paramNames = new String[ctMethod.getParameterTypes().length];
                int pos = Modifier.isStatic(ctMethod.getModifiers()) ? 0 : 1;
                for (int j = 0; j < paramNames.length; j++){
                    paramNames[j] = attr.variableName(j + pos);
                }

                if ( paramNames.length == 0) {
                    Object obj = handler.getMethod().invoke(handler.getController());
                }else{
                    Object [] rs = new Object[paramNames.length];
                    //处理具有参数的情况
                    for ( int i = 0 ; i < paramNames.length ; i ++ ) {
                        String paramName = paramNames[i];
                        rs[i] = request.getParameter(paramName);
                        System.out.println(paramName);
                    }
                    //执行
                    Object obj = handler.getMethod().invoke(handler.getController(),rs);
                }
            }catch (Exception e ) {

            }

        }


    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            doDispatcher(req,resp);
        }catch (Exception e ) {
            e.printStackTrace();
        }
    }
}
