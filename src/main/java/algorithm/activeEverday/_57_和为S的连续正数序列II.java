package algorithm.activeEverday;

import java.util.ArrayList;
import java.util.List;

/***
 * 输入一个正整数 target ，输出所有和为 target 的连续正整数序列（至少含有两个数）。
 *
 * 序列内的数字由小到大排列，不同序列按照首个数字从小到大排列。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：target = 9
 * 输出：[[2,3,4],[4,5]]
 * 示例 2：
 *
 * 输入：target = 15
 * 输出：[[1,2,3,4,5],[4,5,6],[7,8]]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/he-wei-sde-lian-xu-zheng-shu-xu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-3-6
 *  解题思路：滑动窗口！！！
 */
public class _57_和为S的连续正数序列II {

    public int[][] findContinuousSequence(int target) {
        int left = 1;//左端
        int right = 1;//右端
        int sum = 0;//统计和
        List<int []> list = new ArrayList<>(); //保存结果

        //滑动窗口解题，由于是顺序序列，最大的元素应该之到 target / 2
        while ( left < target / 2 ){

            if ( sum < target) {
                sum += right;
                right ++ ;
            }else if ( sum > target) {
                sum -= left;
                left ++ ;
            }else{
                int index = 0;
                int [] rs = {};
                for ( int i = left ; i <= right ; i++ ){
                    rs[index ++ ] = i;
                }
                list.add(rs);
                left ++;
            }
        }

        //转成二维数组
        return list.toArray(new int[list.size()][]);

    }
}
