package patter.geek.虚拟钱包.贫血模型;

import java.math.BigDecimal;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-29 15:00
 **/
public class VirtualWalletRepository {

    // 根据ID获取一个虚拟钱包对象
    public VirtualWalletEntity getWalletEntity(Long walletId) {
        return new VirtualWalletEntity();
    }

    public BigDecimal getBalance(Long walletId) {
        return new BigDecimal(1);
    }

    public void updateBalance(Long walletId, BigDecimal subtract) {
    }
}
