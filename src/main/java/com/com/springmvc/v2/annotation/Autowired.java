package com.com.springmvc.v2.annotation;

import java.lang.annotation.*;

/**自动注入注解信息
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:26
 **/

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {
    String value() default "";
}
