package com.web.strategy;

/**
 * 工具类，将传入的对象转换成一个字节数组
 *
 */

import java.io.*;

public class ObjectChangeToByte
{
    public static  byte[] changeToByte(Object obj) throws IOException{
        ObjectOutputStream os = null;
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(5000);
        os = new ObjectOutputStream(new BufferedOutputStream(byteStream));
        os.flush();
        os.writeObject(obj);
        os.flush();
        byte[] sendBuf = byteStream.toByteArray();
        os.close();
        return sendBuf;
    }
}
