package offer;
/***
 * 在给定的网格中，每个单元格可以有以下三个值之一：
 *
 * 值 0 代表空单元格；
 * 值 1 代表新鲜橘子；
 * 值 2 代表腐烂的橘子。
 * 每分钟，任何与腐烂的橘子（在 4 个正方向上）相邻的新鲜橘子都会腐烂。
 *
 * 返回直到单元格中没有新鲜橘子为止所必须经过的最小分钟数。如果不可能，返回 -1。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/rotting-oranges
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * 解法：BFS算法的应用
 *      腐烂橘子传染的是跟它相邻的新鲜橘子，所以传染的时候要判断被传染的橘子是不是新鲜橘子。
 *      如果是，则先改变其值为3，表示是新腐烂的。
 *          如果直接改为2，会导致其旁边的橘子也直接被传染，而不是一分钟后再被传染。
 *      等所有新感染的橘子都确定了之后，再让它们统一改为2。
 *

 */
/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-23 20:21
 **/
public class _10_30_腐烂的橘子 {
    //标记是否产生新腐烂的橘子
    private boolean flag = true;
    public int orangesRotting(int[][] grid) {
        int total = 0;
        while ( flag ) {
            //处理感染情况
            flag = rotting(grid);
            if ( flag ) {
                total ++;
            }
        }

        //判断是否有新鲜的橘子
        if( isHavingFresh(grid)){
            return -1;
        }

        return total;
    }

    //判断是否有腐烂的橘子
    private boolean isHavingFresh(int[][] grid) {
        for( int i = 0 ; i < grid.length ; i ++ ){
            for ( int j = 0 ; j < grid[i].length ; j ++ ){
                //1表示新鲜的橘子
                if ( grid[i][j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    //生成腐烂信息
    private boolean rotting(int[][] grid) {

        for( int i = 0 ; i < grid.length ; i ++ ){
            for ( int j = 0 ; j < grid[i].length ; j ++ ){
                //2表示腐烂
                if ( grid[i][j] == 2) {
                    //枚举四个方向 左  上 下 右
                    //4标记待腐烂信息
                    //左
                    if( j - 1 >= 0  && grid[i][j - 1] == 1){
                        grid[i][j-1] = 4;
                    }
                    //上
                    if( i - 1 >= 0 && grid[i-1][j] == 1) {
                        grid[i-1][j] = 4;
                    }
                    //右
                    if ( j + 1 < grid[i].length && grid[i][j+1] == 1){
                        grid[i][j+1] = 4 ;
                    }
                    //下
                    if( i + 1 < grid.length && grid[i+1][j] == 1){
                        grid[i+1][j] = 4;
                    }
                }
            }
        }


        //记录是否有腐烂的橘子
        int sum = 0 ;
        for ( int i = 0 ; i < grid.length ; i ++ ){
            for ( int j = 0 ; j < grid[i].length ; j ++ ){
                if ( grid[i][j] == 4) {
                    sum ++ ;
                    grid[i][j] = 2;
                }
            }
        }

        return sum != 0;
    }


}
