package offer;

import java.util.Stack;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-12 18:24
 **/
public class _10_40_栈的压入弹出序列栈的序列 {

    // 构建一个栈来模拟数据的进出
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> stack = new Stack<>();

        int popIndex = 0;
        for ( int i = 0 ; i < pushed.length ; i ++ ){
            stack.push(pushed[i]);
            // 如果当前栈顶元素信息同pop数组中信息一致则出栈
            while( !stack.isEmpty()
                    && stack.peek() == popped[popIndex]){
                popIndex ++ ;
                // 将该元素出栈
                stack.pop();
            }
        }


        return stack.isEmpty();

        /*int pushIndex = 1;
        int popIndex = 0 ;
        stack.push(pushed[0]);
        while ( !stack.isEmpty()){
            if ( stack.peek() == popped[popIndex]){
                stack.pop();
                popIndex ++ ;
                continue;
            }
            // 加入元素
            stack.push(pushed[pushIndex++]);

        }*/
    }
}
