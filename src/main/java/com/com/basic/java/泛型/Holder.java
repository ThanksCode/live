package com.com.basic.java.泛型;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-26 12:59
 **/

//声明时指定类的泛型，在调用时确定信息
public class Holder<T> {

    private T e;
    public Holder(T t){
        this.e  = t ;
    }

    public T get(){
        return e;
    }

    public void set(T t) {
        this.e = t;
    }


    public static void main(String[] args) {
        Holder<Animal> holder = new Holder<Animal>(new Dot());


    }
}
