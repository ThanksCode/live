package offer;

import java.util.ArrayList;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 17:52
 **/
public class _10_44_顺时针打印矩阵 {

    public int[] spiralOrder(int[][] matrix) {
        ArrayList<Integer> rs = new ArrayList<>();
        // 处理非法数据信息
        if ( matrix != null && matrix.length >  0) {

            int r1 = 0, r2 = matrix.length - 1, c1 = 0, c2 = matrix[0].length - 1;

            // 处理循环
            while ( r1 <= r2 && c1 <= c2){

                // 先从前向后
                for( int i = c1 ; i <= c2 ; i ++ ) {
                    rs.add(matrix[r1][i]);
                }

                // 此时考虑一个问题：避免重复打印边界元素信息
                // 两种解决办法：i <= c2 或者 i = r1 + 1
                // 从上向下
                for ( int i = r1 + 1 ; i <= r2 ; i ++ ) {
                    rs.add(matrix[i][c2]);
                }
                // 注意：避免退化成一行！！！！！！！
                // 从右向左
                if( r1 != r2) {
                    for( int i = c2 -1 ; i >= c1 ; i -- ){
                        rs.add(matrix[r2][i]);
                    }
                }
                // 避免退化成一列
                if( c1 != c2) {
                    // 从下到上
                    for( int i = r2 - 1 ; i > r1 ; i --){
                        rs.add(matrix[i][c1]);
                    }
                }
                r1 ++ ; r2--;c1++;c2--;
            }
        }

        return rs.stream().mapToInt(Integer::intValue).toArray();
    }
}
