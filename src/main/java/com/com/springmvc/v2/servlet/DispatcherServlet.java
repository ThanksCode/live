package com.com.springmvc.v2.servlet;

import com.com.springmvc.v2.annotation.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-14 06:00
 **/
public class DispatcherServlet extends HttpServlet {

    //配置文件信息
    private Properties contextConfig = new Properties();
    //保存包下所有的类信息名字
    private List<String> classNames = new ArrayList<>();
    //ioc容器用来存放对象信息
    private Map<String,Object> instanceMapping = new HashMap<>();

    //建立方法同url之间的映射关系
    private List<Handler> handlerMapping = new ArrayList<Handler>();

    private final char START = 'A';
    private final char END = 'Z';
    @Override
    public void init(ServletConfig config) throws ServletException {
        //1、读取配置文件  从配置文件中获取到配置文件的信息
        doLoadConfig(config.getInitParameter("contextConfigLocation"));
        //2、扫描指定包路径下的类
        scanClass(contextConfig.getProperty("scanPackage"));
        //3、把这些被扫描出来的类进行实例化(BeanFactroy)
        instance();
        //4、建立依赖关系，自动依赖注入
        autowired();
        //5、建立URL和Method的映射关系(HandlerMapping)
        handlerMapping();
        //输出一句话
        System.out.println("codnig MVC Framework 已经准备就绪啦，欢迎来戳我!!!");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try{
            //从上面已经初始化的信息中匹配
            //拿着用户请求url去找到其对应的Method
            boolean isMatcher = pattern(req,resp);
            if(!isMatcher){
                resp.getWriter().write("404 Not Found");
            }
        }catch(Exception e){
            resp.getWriter().write("500 Exception,Details:\r\n" +
                    e.getMessage() + "\r\n" +
                    Arrays.toString(e.getStackTrace()).replaceAll("\\[\\]", "")
                            .replaceAll(",\\s", "\r\n"));
        }

    }
    /***
     * 初始化配置文件信息
     * @param contextConfigLocation
     */
    private void doLoadConfig(String contextConfigLocation){
        InputStream ins = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation);
        try{
            //加载properties中的配置文件信息
            contextConfig.load(ins);
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            if( ins != null) {
                try {
                    ins.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /***
     * 扫描指定路径下的所有类信息
     * @param packageName
     */
    private void scanClass(String packageName){
        //加载配置文件后获取到的信息：com.com.springmvc.v2.controller
        //此处涉及正则小知识：\表转义信息  \\.的意思  ---- > \.
        URL url = this.getClass().getClassLoader().getResource("/" +
                packageName.replaceAll("\\.","/"));
        //替换后的路径  当前文件所处路径+/com/com/springmvc/v2/controller
        //本利而言：D:\livehood\livehood\target\self\WEB-INF\classes\com\com\springmvc\v2\controller
        //文件存放于D盘，所以出现上述信息

        //获取文件夹
        File classDir = new File(url.getFile());
        //遍历路径下的所有文件信息
        for (File file : classDir.listFiles()) {
            //如果该文件下还有子包，递归扫描处理
            if(file.isDirectory()) {
                scanClass(packageName+"."+file.getName());
            }else{
                //仅处理编译形成的.class文件信息
                if( !file.getName().endsWith(".class")){
                    continue;
                }
                //加载所有的类信息，类名以包名 + 类名的形式进行唯一命名
                String className = packageName + "." + file.getName().replaceAll(".class","");
                classNames.add(className);
            }
        }
    }

    /***
     * 实例化对象信息
     */
    private void instance(){
        //避免classNames没有内容信息
       if( classNames.isEmpty()){
           return;
       }
       try{
           //遍历集合信息
        for(String className : classNames) {
            //加载类信息
            Class<?> clazz = Class.forName(className);

            //进行判断，仅对加有注解信息的类进行初始化比如@controller @service 等等
            if( clazz.isAnnotationPresent(Controller.class)){
                //处理注解中可能有配置信息，如果有则使用提供的配置信息
                //如果没有则使用默认类名进行命名处理
                Controller controller = clazz.getAnnotation(Controller.class);
                String beanName = controller.value();
                if ( "".equals(beanName.trim())){
                    beanName = toLowerFirstCase(clazz.getSimpleName());
                }
                //实例化对象
                instanceMapping.put(beanName,clazz.newInstance());
            }else if ( clazz.isAnnotationPresent(Service.class)){
                Service service = clazz.getAnnotation(Service.class);
                String beanName = service.value();
                if ( "".equals(beanName.trim())){
                    beanName = toLowerFirstCase(clazz.getSimpleName());
                }
                instanceMapping.put(beanName,clazz.newInstance());

                //此处若是接口类型并不能直接实例化放入容器
                //原因：service层在控制层注入的信息一般为接口类型，接口类型不能直接实例化！
                //最简单的解决策略：判断是否属于同一类型的接口
                //比如：userService ---- >  userServiceImpl
                //如果接口同属于userServiceImpl则使用userServiceImpl的信息进行实例化
                Class<?> [] interfaces = clazz.getInterfaces();
                for (Class<?> i : interfaces) {
                    instanceMapping.put(i.getName(), clazz.newInstance());
                }

            }
        }
       }catch (Exception e) {
           e.printStackTrace();
       }
    }

    /***
     * 将对象信息进行自动注入处理
     */
    private void autowired(){
        if( instanceMapping.isEmpty()) {
            return;
        }
        //遍历instanceMapping对象实现自动注入
        for(Map.Entry<String,Object> entry : instanceMapping.entrySet()) {
            //获取到类对应下的字段信息 但不获取从父类继承而来的默认字段信息
            //如：@Autowired private Service iservice
            Field [] fields = entry.getValue().getClass().getDeclaredFields();

            //遍历字段信息
            for(Field field : fields) {
                //未加Autowired字段信息
               if ( !field.isAnnotationPresent(Autowired.class)) {
                   continue;
               }

               Autowired autowired = field.getAnnotation(Autowired.class);
               String beanName = autowired.value();
               if ( "".equals(beanName.trim())){
                   //获取的对象名：com.com.springMVC.V2.service.queryService
                   beanName = field.getType().getName();
               }
               //暴力破坏权限
               field.setAccessible(true);
               try{
                   field.set(entry.getValue(),instanceMapping.get(beanName));
               }catch (IllegalAccessException e) {
                   e.printStackTrace();
                   continue;
               }
            }
        }

    }

    private void handlerMapping() {
        //判空操作
        if (instanceMapping.isEmpty()) {
            return;
        }

        for(Map.Entry<String,Object> entry : instanceMapping.entrySet()){
            Class<?> clazz = entry.getValue().getClass();
            //对非控制层的类信息不加处理
            if(!clazz.isAnnotationPresent(Controller.class)){
                continue;
            }
            //建立路径信息
            String url = "";
            //获取到在控制层上配置requestMapping中的信息
            if( clazz.isAnnotationPresent(RequestMapping.class)){
                RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                url = requestMapping.value();
            }
            //仅获取在控制中定义的，不获取从分类中继承而来的信息
            Method [] methods = clazz.getDeclaredMethods();

            for(Method method : methods) {
                //缺少匹配信息的类
                if(!method.isAnnotationPresent(RequestMapping.class)){
                    continue;
                }
                RequestMapping requstMapping = method.getAnnotation(RequestMapping.class);
                //拼接路径信息  其中末尾的正则表达式主要用于解决一下的情况
                // /+  表示一个或多个/
                //避免使用者在定义控制层的requestMapping时有这样的情况
                //  /user/
                //下面方法由有 /function
                //此时拼接就成了 /user//function
                //末尾正则意义就在于避免这种情况的发生
                String customRegex = ("/" + url + requstMapping.value()).replaceAll("/+", "/");
                //此处可根据自主定义来进行修改
                String regex = customRegex.replaceAll("\\*", ".*");
                Map<String,Integer> pm = new HashMap<String,Integer>();


                //获取参数上的注解信息
                Annotation [][] paramets = method.getParameterAnnotations();
                for(int i = 0 ; i < paramets.length;  i ++ ) {
                    for ( Annotation a : paramets[i]){
                        if ( a instanceof  RequestParam){
                            //获取配置注解的信息名
                            String paramName = ((RequestParam)a).value();
                            if(!"".equals(paramName.trim())){
                                pm.put(paramName, i);
                            }
                        }
                    }
                }

                //提取Request和Response的索引
                Class<?> [] paramsTypes = method.getParameterTypes();
                for(int i = 0 ; i < paramsTypes.length; i ++){
                    Class<?> type = paramsTypes[i];
                    if(type == HttpServletRequest.class ||
                            type == HttpServletResponse.class){
                        pm.put(type.getName(), i);
                    }
                }

                handlerMapping.add(new Handler(Pattern.compile(regex),entry.getValue(),method, pm));
                System.out.println("Mapping " + customRegex + "  " + method);
            }
        }

    }

    /***
     * 类型转换处理
     * @param value
     * @param clazz
     * @return
     */
    private Object castStringValue(String value,Class<?> clazz){

        if(clazz == String.class){
            return value;
        }else if(clazz == Integer.class){
            return Integer.valueOf(value);
        }else if(clazz == int.class){
            return Integer.valueOf(value).intValue();
        }else{
            return null;
        }

    }
    /***
     * 处理信息
     * 保存信息时第一个转成字母小写
     * @param simpleName
     * @return
     */
    private String toLowerFirstCase(String simpleName) {
        //转成字符数组信息
        char [] chars = simpleName.toCharArray();

        //仅处理 A-Z 之间的字母信息
        if( chars[0] >= START && chars[0] <= END){
            chars[0] += 32;
        }
        return String.valueOf(chars);
    }


    public boolean pattern(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        //throw new Exception("这是一个假象，是我自己定义异常，弄着玩的");
        if (handlerMapping.isEmpty()) {
            return false;
        }
        //获取请求资源的路径信息
        String url = req.getRequestURI();
        String contextPath = req.getContextPath();
        url = url.replace(contextPath, "").replaceAll("/+", "/");

        for (Handler handler : handlerMapping) {
            try {
                Matcher matcher = handler.pattern.matcher(url);
                if (!matcher.matches()) {
                    continue;
                }

                Class<?>[] paramTypes = handler.method.getParameterTypes();

                Object[] paramValues = new Object[paramTypes.length];

                Map<String, String[]> params = req.getParameterMap();

                for (Map.Entry<String, String[]> param : params.entrySet()) {

                    String value = Arrays.toString(param.getValue()).replaceAll("\\]|\\[", "").replaceAll(",\\s", ",");

                    if (!handler.paramMapping.containsKey(param.getKey())) {
                        continue;
                    }

                    int index = handler.paramMapping.get(param.getKey());

                    //涉及到类型转换
                    paramValues[index] = castStringValue(value, paramTypes[index]);

                }


                //
                int reqIndex = handler.paramMapping.get(HttpServletRequest.class.getName());
                paramValues[reqIndex] = req;

                int repIndex = handler.paramMapping.get(HttpServletResponse.class.getName());
                paramValues[repIndex] = resp;

                //需要对象.方法
                handler.method.invoke(handler.controller, paramValues);

                return true;
            } catch (Exception e) {
                throw e;
            }
        }
        return false;

    }
        /***
         * 建立方法  路径之间的映射关系
         */
     class Handler{

        protected Pattern pattern;
        protected Object controller; //保存控制层实例信息
        protected Method method; //保存映射的方法
        protected Map<String,Integer> paramMapping; //参数序列信息

        protected Handler(Pattern pattern,Object controller,Method method,Map<String,Integer> paramMapping){
            this.pattern = pattern;
            this.controller = controller;
            this.method = method;
            this.paramMapping = paramMapping;
        }


    }

}
