package offer;

import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 21:24
 **/
public class ListNode {
      public int val;
      public ListNode next;
      public ListNode(){

      }
      public ListNode(int val) {
            this.val = val;
            this.next = null;
      }
}
