package com.com.basic.java.isbridge;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-15 21:02
 **/
public class Son extends Father<String> {

    @Override
    public int method(String msg) {
        System.out.println(msg);
        return 1;
    }



    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Son son = new Son();
        Method sonMethod = son.getClass().getMethod("method",String.class);
        sonMethod.invoke(son,"sonMethod");
        System.out.println("sonMethod is Bridge " + sonMethod.isBridge());
        System.out.println(sonMethod.getReturnType());
        System.out.println(boolean.class.equals(sonMethod.getReturnType()));
        /*Method fatherMethod = son.getClass().getMethod("method",Object.class);
        sonMethod.invoke(son,"fatherMethod");
        System.out.println("fatherMethod is Bridge " + fatherMethod.isBridge());*/



    }




}
