package com.handel;

public class H_SqlSession {

    private H_Configuration configuration; //配置环境信息
    private H_Execute execute ;//执行

    public H_SqlSession(H_Configuration configuration , H_Execute execute) {
        this.configuration = configuration;
        this.execute  = execute;
    }

    //根据类信息获取实例化对象
    public <T> T getMapper(Class<T> tClass) throws Exception {
       return configuration.getMapper(tClass,this);
    }



    //查询方法；
    public <T> T selectOne(String statement,String parameter){
        return execute.query(statement, parameter);
    }

}
