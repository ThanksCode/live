package offer;

import java.util.Arrays;
/***
 * 1）当只有一个数字时，只有一种排列情况
 *
 * （2）排列其实就是第一个数字分别与后面的数字进行交换：
 *
 * 在原来排列的基础上，第一个数字与第一个进行交换，即以第一个数字开头，讨论剩下的数字的全排列，剩下数字的全排列按此方式递归得到；
 *
 * 在原来排列的基础上，第一个与第二个进行交换，即以第二个数字开头，讨论剩下的数字的全排列，...；
 *
 * ...
 *
 * 在原来排列的基础上，第一个数组与最后一个进行交换，即以最后一个数组开头，讨论剩下的数字的全排列,....；
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-23 19:33
 **/
public class _全排序 {

    public  void perm(int [] arr , int s ,int len){
        if ( s == len){
//            java输出数组内容借助于Arrays类库的toString方法
            System.out.println(Arrays.toString(arr));
        }
        for(int i = s ; i < len ; i ++ ) {
            swap(arr,s,i);
            perm(arr,s + 1,len);
            swap(arr,s,i );
        }
    }
    private  void swap(int[] arr, int k, int i) {
        int temp;
        temp=arr[k];
        arr[k]=arr[i];
        arr[i]=temp;
    }

    public static void main(String[] args) {
        int [] rs = {1,3,2};
        _全排序 allSort = new _全排序();
        allSort.perm(rs,0,rs.length );
        System.out.println("数组最终结果信息");
        System.out.println(Arrays.toString(rs));
    }

}
