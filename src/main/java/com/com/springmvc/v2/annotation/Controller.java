package com.com.springmvc.v2.annotation;

import java.lang.annotation.*;

/**定义控制层注解
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-05-13 22:24
 **/

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {
    String value() default "";
}
