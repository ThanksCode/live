package algorithm.org.DataAlgorithm;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-27 17:43
 **/
public class _8_字符串中的第一个唯一字符 {

    public int firstUniqChar(String s) {

        if ( s == null || s.length() == 0) {
            return -1;
        }

        for(int i = 0 ; i < s.length() ; i ++ ) {
            char cur = s.charAt(i);
            if ( i + 1 >= s.length()){
                return -1;
            }
            int index = s.indexOf(cur,i+1);
            if ( index == -1){
                return index;
            }
        }
        return -1;
    }

    public int firstUniqCharV2(String s){
        int [] map = new int[128];
        for ( int i = 0 ; i < s.length() ; i ++ ){
            char cur = s.charAt(i);
            map[cur] = i;
        }

        for (int i = 0 ; i < s.length(); i ++ ) {
            char cur = s.charAt(i);
            if ( map[cur] == i ) {
                return i;
            }else{
                // 关键一步
                map[cur] = -1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        _8_字符串中的第一个唯一字符 obj = new _8_字符串中的第一个唯一字符();
        obj.firstUniqChar("leetcode");
    }
}
