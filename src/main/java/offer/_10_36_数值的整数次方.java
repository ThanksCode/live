package offer;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-10 16:25
 **/
public class _10_36_数值的整数次方 {

    public double myPow(double x, int n,int index) {
        // 处理特殊情况
        if ( x == 0 ) {
            return 0;
        }
        // 处理特殊情况
        if( n == 0 || x == 1) {
            return 1;
        }
        long b = n;
        // 标记指数的正负情况
        boolean flag = true;
        // 处理负数情况
        if ( b < 0 ) {
            b = - b;
            flag = false;
        }

        double rs = getPow(x,b);
        if ( !flag) {
            return 1.0 /  rs ;
        }
        return rs;
    }

    private double getPow(double n , long p){
        double rs = n;
        for ( int i = 1 ; i < p ; i ++  ){
            rs *= n;
        }
        return rs;
    }

    // 快速指数幂
    public double myPow(double x, int n){
        if( x == 0) {
            return 0;
        }

        long e = n;

        // 负数情况下指数幂为其倒数
        if ( e < 0 ) {
            // 同时进行处理 ，省去标记量标记处理
            e = -e;
            x = 1 / x;
        }
        double rs = 1.0;
        while ( e > 0 ) {
            // 核心重点！！！！ 处理奇数情况
            if ( (e & 1) == 1 ){
                rs *= x;
            }
            // 记录倍数关系
            x *= x;
            // 右移缩小
            e = e >> 1;
        }

        return rs * x;
    }

}
