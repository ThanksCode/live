package offer;

/***
 * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
 *
 * 示例 1:
 *
 * 输入: 1->1->2
 * 输出: 1->2
 * 示例 2:
 *
 * 输入: 1->1->2->3->3
 * 输出: 1->2->3
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date 2020-3-16
 *  注意：
 *      在循环控制中：cur记录当前结点信息，而通过内部循环的方式来判断后序结点是否同该节点信息相同
 *      此时需要注意不能用 p.next != null 来进行判断，p结点是否为空，如果改用p.next != null
 *      这样会导致最后一个结点无法判断的情况
 *       如：  1  -> 2  ->3     ->3  - > null
 *                       cur   pre  pre.next
 *        此时这种情况便不会判断，导致返回 1-2-3-3!
 */
public class _10_25_删除链表中重复元素 {

    public ListNode deleteDuplicates(ListNode head) {
        ListNode cur = head;
        ListNode p = cur; //当前结点

        while ( cur != null && cur.next != null) {
            p = cur.next;
            while ( p != null && cur.val == p.val ) {
                p = p.next;
            }
            cur.next = p;
            cur = p;
        }
        return head;
    }
}
