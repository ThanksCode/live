package com.com.basic.java.异常;

import javax.annotation.processing.FilerException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-26 16:32
 **/
public abstract class Inner {
   public  abstract void even() throws FilerException;
}
