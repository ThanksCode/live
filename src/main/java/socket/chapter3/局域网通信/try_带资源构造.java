package socket.chapter3.局域网通信;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 07:43
 **/
public class try_带资源构造 {

    public static void main(String[] args) {
        // JDK7 之后引入带资源的构造方式  会自动释放流 无需手动关闭
        try(OutputStream ps = new FileOutputStream("demo.txt")){
        }catch (IOException e) {

        }

    }
}
