package socket.chapter6.constants;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-03 11:26
 **/
public class TCPConstants {
    // 服务器固化UDP接收端口
    public static int PORT_SERVER = 30401;
}
