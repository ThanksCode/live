package com.com.basic.java.继承;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-10 09:23
 **/
public class Glyph {

    void draw(){
        System.out.println("Glyoh . draw");
    }

    Glyph(){
        System.out.println("Glyoh before  draw()");
        draw();
        System.out.println("Glyoh after  draw()");
    }
}
