package offer;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-18 20:48
 **/
public class _10_31_机器人活动范围 {

    boolean [][] visit = new boolean[200][200];
    //计算结果
    public int movingCount(int m, int n, int k) {

        //处理数据非法情况
        if ( m < 0 || n < 0 || k < 0) {
            return 0;
        }
        //visit 默认会置为false
        return movingCore(m,n,k,0,0,visit);

    }
    //处理事务的核心
    private int movingCore(int m , int n , int k ,int row , int col ,boolean [][] visit){
        //统计总数
        int count = 0 ;

        if ( check(m,n,row,col,k) && visit[row][col]){
            //枚举各个方向
            count = 1 + movingCore(m,n,k,row - 1 , col,visit)
                    + movingCore(m,n,k,row + 1 ,col,visit)
                    + movingCore(m,n,k,row ,col + 1,visit)
                    + movingCore(m,n,k,row,col - 1,visit);
        }

        return count;

    }
    //校验数据信息
    private boolean check( int m , int n , int row , int col,int k){

        int sum = getDigitSum(row) + getDigitSum(col);
        if( row >= 0 && row < m && col >= 0 && col < n
                &&  sum <= k ){
            return true ;
        }
        return  false;
    }
    //拆分数据信息
    private int getDigitSum(int num) {
        int sum = 0 ;
        while ( num > 0) {
            sum += num % 10 ;
            num /= 10;
        }
        return sum ;
    }
}
