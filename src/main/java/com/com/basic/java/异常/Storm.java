package com.com.basic.java.异常;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-26 16:32
 **/
public interface Storm {
    void even () throws NullPointerException;
}
