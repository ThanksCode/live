package socket.chapter5.CountDownLatchDemo;

import java.util.concurrent.CountDownLatch;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-03 11:48
 **/
public class Boss implements Runnable {
    private CountDownLatch downLatch;

    public Boss(CountDownLatch downLatch){
        this.downLatch = downLatch;
    }

    public void run() {
        System.out.println("老板正在等所有的工人干完活......");
        try {
            // 进入休眠
            this.downLatch.await();
        } catch (InterruptedException e) {
        }
        System.out.println("工人活都干完了，老板开始检查了！");
    }

}
