package patter.creational.builder;

public class Demo {

    static int rs[] = {3,5,0,1,3,0,6};
    public void fun( int [] nums) {
        int j = 0;
        for ( int i = 0 ; i < nums.length; i ++ ){
            if ( nums[i] != 0) {
                nums[j] = nums[i];
                if ( i != j) {
                    nums[i] = 0;
                }
                j ++ ;
            }
        }
    }
    public static void main(String[] args) {
        CourseBuilder courseBuilder = new CourseActualBuilder();
        Coach coach = new Coach();
        coach.setCourseBuilder(courseBuilder);

        Course course = coach.makeCourse("Java设计模式精讲",
                "Java设计模式精讲PPT",
                "Java设计模式精讲视频",
                "Java设计模式精讲手记",
                "Java设计模式精讲问答");
        System.out.println(course);

        Demo d = new Demo();
        d.fun(rs);
        System.out.print("[");
        for ( int i = 0 ; i < rs.length;  i ++) {
           if ( i != rs.length - 1) {
               System.out.print(rs[i] + ", ");
           }else{
               System.out.print(rs[i]);
           }
        }
        System.out.print("]");

    }

}
