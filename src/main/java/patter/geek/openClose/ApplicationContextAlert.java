package patter.geek.openClose;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 17:09
 **/
public class ApplicationContextAlert {

    private NotificationEmergencyLevel notification;
    private Alert alert;
    private AlertRule alertRule;
    private AlertInof alertInof;

    // 生成饿汉式单例
    private static final ApplicationContextAlert instance = new ApplicationContextAlert();

    public void initializeBeans() {
        this.alert = new Alert();
        this.notification = new NotificationEmergencyLevel();
        this.alertRule = new AlertRule();
        alert.addAlertHandler(new TpsAlertHandler(alertRule,notification));
        alert.addAlertHandler(new ErrorAlertHandler(alertRule,notification));
    }

    // 私有化构造器
    private ApplicationContextAlert(){
        // 初始化
        initializeBeans();
    }

    public Alert getAlert() { return alert; }

    public static ApplicationContextAlert getInstance(){
        return instance;
    }



}
