package socket.chapter6.handle;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-08 17:53
 **/
public interface ClientHandlerCallback {
    // 自身关闭通知
    void onSelfClosed(ClientHandler handler);

    // 收到消息时通知
    void onNewMessageArrived(ClientHandler handler, String msg);
}
