package patter.struct.Composite.d.HumanResource;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 17:27
 **/
class DepartmentRepo {

    // 模拟从数据库中查查询出的数据信息

    private List<HumanResource> items = new ArrayList<>();

    // 数据库查询时，查询表中parentid = id 找到所有的信息，返回到方法中

    public List<Long> getLongSubId(long id) {
        return null;
    }
}
class EmployeeRepo{

    public double getEmployeeSalary(Long employeeId) {
        return 1;
    }

    public List<Long> getDepartmentEmployeeIds(long id) {
        return null;
    }
}
public class DepartmentService {

    private static final long ORGANIZATION_ROOT_ID = 1001;
    // 依赖注入
    private DepartmentRepo departmentRepo;
    // 依赖注入
    private EmployeeRepo employeeRepo;

    public DepartmentService(){
        Department rootDepartment = new Department(ORGANIZATION_ROOT_ID);
        bulidOrigin(rootDepartment);
    }


    private void bulidOrigin(Department department) {
        // 查询子部门
        List<Long> subDepartmentIds =  departmentRepo.getLongSubId(department.id);
        for(Long subDepartmentId : subDepartmentIds) {
            Department subDepartment = new Department(subDepartmentId);
            department.addSubNode(subDepartment);
            bulidOrigin(subDepartment);
        }

        // 记录职员信息
        // 实际中不推荐这样，多次查询数据库效率不高
        // 根据部门id去查询职员id 根据职员id 去查询薪水，之后构建一个职员对象信息
        List<Long> employeeIds = employeeRepo.getDepartmentEmployeeIds(department.id);
        for (Long employeeId : employeeIds) {
            double salary = employeeRepo.getEmployeeSalary(employeeId);
            department.addSubNode(new Employee(employeeId, salary));
        }
    }


}
