package com.com.basic.java.继承;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-10 09:08
 **/
public class Share {

    private int ref = 0 ;
    private  static long counter = 12;
    //private static String str = "demo.txt";
    private final long id = counter++;

    Share(){
        System.out.println("creating "+ this);
    }

    @Override
    public String toString() {
        return "shared" + id;
    }
}
