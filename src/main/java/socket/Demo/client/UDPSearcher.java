package socket.Demo.client;

import socket.Demo.constants.UDPConstants;
import socket.Demo.entry.ServerInfo;
import socket.chapter5.utils.ByteUtils;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 17:27
 **/
public class UDPSearcher {

    private static final int LISTEN_PORT = UDPConstants.PORT_CLIENT_RESPONSE;

    // 搜索信息
    public static ServerInfo searchServer(int timeout) {
        System.out.println("启动UDP搜索信息。。。。。。。");
        CountDownLatch receiveLatch = new CountDownLatch(1);
        Listener listener = null;
        try {
            listener = listen(receiveLatch);
            sendBroadcast();
            receiveLatch.await(timeout, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 完成
        System.out.println("UDP连接结束！！！！.");
        if (listener == null) {
            return null;
        }
        List<ServerInfo> devices = listener.getServerAndClose();
        if (devices.size() > 0) {
            return devices.get(0);
        }
        return null;

    }

    // 发送广播包
    private static void sendBroadcast() throws Exception {
        // 默认分配端口信息
        DatagramSocket ds = new DatagramSocket();

        // 获得一个nio的ByteBuffer对象
        ByteBuffer byteBuffer = ByteBuffer.allocate(128);

        // 构建广播信息
        byteBuffer.put(UDPConstants.HEADER);
        byteBuffer.putShort((short) 1) ;
        // 置入监听端口信息
        byteBuffer.putInt(LISTEN_PORT);

        // position 返回末尾位置信息  长度信息为 index + 1
        DatagramPacket requestPack = new DatagramPacket(byteBuffer.array(),byteBuffer.position() + 1);

        requestPack.setAddress(InetAddress.getByName("255.255.255.255"));
        requestPack.setPort(UDPConstants.PORT_SERVER);
        // 发送信息
        ds.send(requestPack);
        ds.close();
    }

    // 蒋婷
    private static Listener listen(CountDownLatch receiveLatch) throws Exception {
        CountDownLatch startLatch = new CountDownLatch(1);
        Listener listener = new Listener(LISTEN_PORT,startLatch,receiveLatch);
        listener.start();
        startLatch.await();
        return listener;
    }

    private static class Listener extends Thread {
        // 监听端口信息
        private final int listenPort;
        // 启动进程
        private final CountDownLatch startDownLatch;
        // 接收进程
        private final CountDownLatch receiveDownLatch;
        // 设备信息
        private final List<ServerInfo> serverInfoList = new ArrayList<>();
        // 缓冲数组
        private final byte[] buffer = new byte[128];
        private final int minLen = UDPConstants.HEADER.length + 2 + 4;
        private boolean done = false;
        private DatagramSocket ds = null;

        private Listener(int listenPort, CountDownLatch startDownLatch, CountDownLatch receiveDownLatch) {
            super();
            this.listenPort = listenPort;
            this.startDownLatch = startDownLatch;
            this.receiveDownLatch = receiveDownLatch;
        }

        @Override
        public void run() {
            super.run();
            // 启动
            startDownLatch.countDown();

            try {
                while ( !done ) {
                    DatagramSocket ds = new DatagramSocket(listenPort);
                    // 构建一个数组信息
                    DatagramPacket receivePacket = new DatagramPacket(buffer,buffer.length);

                    ds.receive(receivePacket);

                    // 获取信息
                    String ip = receivePacket.getAddress().getHostAddress();
                    int port = receivePacket.getPort();
                    int dataLen = receivePacket.getLength();
                    byte [] data = receivePacket.getData();
                    boolean flag = dataLen >= minLen
                            && ByteUtils.startsWith(data,UDPConstants.HEADER);
                    if ( !flag ) {
                        // 校验无法通过
                        continue;
                    }
                    // 分析数据信息
                    ByteBuffer byteBuffer = ByteBuffer.wrap(data,UDPConstants.HEADER.length,dataLen);
                    // 提取指令信息
                    short cmd = byteBuffer.getShort();
                    int serverPort = byteBuffer.getInt();
                    if (cmd != 2 || serverPort <= 0) {
                        System.out.println("UDPSearcher 收到的命令信息 :" + cmd + "\t服务器端口信息:" + serverPort);
                        continue;
                    }

                    // 构建一个ServiceInfo对象
                    String sn = new String(buffer, minLen, dataLen - minLen);
                    ServerInfo info = new ServerInfo(serverPort, ip, sn);
                    serverInfoList.add(info);
                    // 成功接收到一份
                    receiveDownLatch.countDown();
                }

            }catch (Exception ignore ){

            }finally {
                close();
            }


        }

        // 关闭
        private void close(){
            // 释放者模式
           if( ds != null) {
               ds.close();
           }
        }
        // 返回资源信息
        public List<ServerInfo> getServerAndClose() {
            close();
            return serverInfoList;
        }
    }
}
