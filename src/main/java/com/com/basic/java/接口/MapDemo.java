package com.com.basic.java.接口;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-11 20:21
 **/
public class MapDemo {

    public static void main(String[] args) {
        Map<String ,String > maps = new HashMap<>();
        maps.put("value","value1");

        for ( Map.Entry<String,String> entry : maps.entrySet()){
            System.out.println(maps.get(entry.getKey()));
            System.out.println(entry.getValue());
        }
    }
}
