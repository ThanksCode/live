package offer;

/**
 * 输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历结果。如果是则返回 true，否则返回 false。假设输入的数组的任意两个数字都互不相同。
 *
 *  
 *
 * 参考以下这颗二叉搜索树：
 *
 *      5
 *     / \
 *    2   6
 *   / \
 *  1   3
 * 示例 1：
 *
 * 输入: [1,6,3,2,5]
 * 输出: false
 * 示例 2：
 *
 * 输入: [1,3,2,6,5]
 * 输出: true
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/er-cha-sou-suo-shu-de-hou-xu-bian-li-xu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-07 17:01
 **/

/****
 * 1 3 2 6 5
 * root = 5
 * right = 6  为右子树 起始
 * left = 2   为左子树 终止
 *
 *
 * 递归解析：
 * 终止条件： 当 i≥j ，说明此子树节点数量 ≤1 ，无需判别正确性，因此直接返回 true ；
 * 递推工作：
 * 划分左右子树： 遍历后序遍历的 [i, j]区间元素，寻找 第一个大于根节点 的节点，索引记为 m 。
 * 此时，可划分出左子树区间 [i,m-1] 、右子树区间 [m, j - 1] 、根节点索引 j 。
 * 判断是否为二叉搜索树：
 * 左子树区间 [i, m - 1] 内的所有节点都应 < postorder[j] 。而
 *  第 1.划分左右子树 步骤已经保证左子树区间的正确性，因此只需要判断右子树区间即可。
 * 右子树区间 [m, j-1] 内的所有节点都应 > postorder[j] 。实现方式为遍历
 *
 * 当遇到  postorder[j]≤postorder[j] 的节点则跳出；则可通过 p = j 判断是否为二叉搜索树。
 * 返回值： 所有子树都需正确才可判定正确，因此使用 与逻辑符 && 连接。
 * p=j ： 判断 此树 是否正确。
 * recur(i, m - 1) ： 判断 此树的左子树 是否正确。
 * recur(m, j - 1) ： 判断 此树的右子树 是否正确。
 *
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 *
 */
public class _10_34_二叉搜索树的后序遍历序列 {


    public boolean verifyPostorder(int[] postorder) {
        // 要验证左子树，右子树分别为二叉搜索树
        return verifyPostorder(postorder,0,postorder.length - 1);
    }



    private boolean verifyPostorder(int [] postorder , int start,int end ){
        // 处理异常信息
        if ( start >= end) {
            return true;
        }
        // 获取到根节点的值
        int root = postorder[end];
        // 标记坐标值
        int leftEnd = start ;

        // 寻找左子树终止处
        while(postorder[leftEnd] < root) {
            leftEnd ++;
        }

        int rightEnd = leftEnd;
        // 右子树
        while(postorder[leftEnd] > root){
            rightEnd ++ ;
        }


        return rightEnd == end && verifyPostorder(postorder, start, leftEnd - 1)
                && verifyPostorder(postorder,leftEnd , end - 1);

    }

        /*// 标记左子树、右子树
        boolean left = true , right = true ;

        if( leftEnd > 0) {
            // 左子树情况
            left = verifyPostorder(postorder,start,leftEnd);
        }

        if ( leftEnd < end - 1 ) {
            // 右子树情况
            right = verifyPostorder(postorder,leftEnd + 1 , end -leftEnd- 1);
        }
        return left && right;*/


}

/***
 *
 * 未通过案例：问题在于：
 *  这时
 *   10 6 9 4
 *  循环查找 第一个就大于
 *
 *
 *
 * 输入:
 * [1,2,5,10,6,9,4,3]
 * 输出
 * true
 * 预期结果
 * false
 ***/