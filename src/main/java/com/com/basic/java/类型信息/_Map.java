package com.com.basic.java.类型信息;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-24 18:32
 **/
public class _Map {
    public static void main(String[] args) {
        Map<String ,String > maps = new HashMap<>();
        maps.put("zhangSan","123");
        maps.put("Lisi","234");
        //遍历
        for (Map.Entry<String, String> pair : maps.entrySet()) {
            // String value = pair.getKey();
            System.out.println(pair.getValue());
        }
    }
}
