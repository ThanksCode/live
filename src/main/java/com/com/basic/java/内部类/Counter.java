package com.com.basic.java.内部类;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-13 10:13
 **/
public interface Counter {
    int next();
}
