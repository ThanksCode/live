package socket.chapter5.handle;

import socket.chapter5.utils.CloseUtils;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-06 11:17
 **/
public class ClientHandler {
    // 连接对象
    private Socket socket;
    // 处理客户端读任务
    private final ClientReadHandler readHandler ;
    // 处理客户端写任务
    private final ClientWriteHandler writeHandler ;
    // 判断是否自身因为无法读取数据而终止退出
    private final CloseNotify closeNotify ;

    public ClientHandler(Socket socket, CloseNotify closeNotify) throws IOException {
        this.socket = socket;
        this.readHandler = new ClientReadHandler(socket.getInputStream());
        this.writeHandler = new ClientWriteHandler(socket.getOutputStream());
        this.closeNotify = closeNotify;
        System.out.println("新客户端连接：" + socket.getInetAddress() +
                " P:" + socket.getPort());
    }

    // 发送数据信息
    public void send(String str) {
        writeHandler.send(str);
    }

    // 读取客户端信息并回显至服务器端
    public void readToPrint() {
        readHandler.start();
    }
    // 退出连接
    public void exit() {
        readHandler.exit();
        writeHandler.exit();
        CloseUtils.close(socket);
        System.out.println("客户端已退出：" + socket.getInetAddress() +
                " P:" + socket.getPort());
    }
    // 由于自身异常而导致结束
    private void exitBySelf() {
        exit();
        closeNotify.onSelfClosed(this);
    }

    /***
     * 仅处理读任务，负责从客户端读取信息
     */
    class ClientReadHandler  extends Thread{
        // 输入结束标识符
        private boolean flag = false;
        // 输入流
        private final InputStream inputStream;

        // 构建时需要传入输入流对象信息
        ClientReadHandler(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            super.run();
            System.out.println("新客户端连接：" + socket.getInetAddress() +
                    " P:" + socket.getPort());
            try {
                // 得到输入流，用于接收数据
                BufferedReader socketInput = new BufferedReader(new InputStreamReader(
                        inputStream));
                do {
                    // 客户端拿到一条数据
                    String str = socketInput.readLine();
                    if( str == null ) {
                        System.out.println("无法从客户端读取信息！！！！");
                        // 退出当前客户端
                        ClientHandler.this.exitBySelf();
                        break;
                    }
                    // 在屏幕上展示数据信息
                    System.out.println(str);
                } while (!flag);
            } catch (Exception e) {
                if (!flag) {
                    System.out.println("连接异常断开");
                    ClientHandler.this.exitBySelf();
                }
            } finally {
                // 关闭输入流
                CloseUtils.close(inputStream);
            }

            System.out.println("客户端已退出：" + socket.getInetAddress() +
                    " P:" + socket.getPort());

        }

        // 结束输入流的监听
        void exit() {
            flag = true;
            CloseUtils.close(inputStream);
        }
    }


    class ClientWriteHandler {
        private boolean done = false;
        private final PrintStream printStream;
        // 构建一个线程池方便线程间通信
        private final ExecutorService executorService;

        ClientWriteHandler(OutputStream outputStream) {
            this.printStream = new PrintStream(outputStream);
            this.executorService = Executors.newSingleThreadExecutor();
        }

        private void exit() {
            done = true;
            CloseUtils.close(printStream);
            executorService.shutdownNow();
        }
        // 重点！！ 如果此处使用匿名内部类 final String str 时才能被内部类所使用
        private void send(String str) {
            executorService.execute(new WriteRunnable(str));
        }


        class WriteRunnable implements Runnable {
            private final String msg;

            WriteRunnable(String msg) {
                this.msg = msg;
            }

            @Override
            public void run() {

                if (ClientWriteHandler.this.done) {
                    return;
                }

                try{
                    // 回送信息
                    ClientWriteHandler.this.printStream.println(msg);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }

    public interface CloseNotify {
        void onSelfClosed(ClientHandler handler);
    }
}
