package algorithm.org.linklist;

/***
 * 给定一个链表，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。
 *
 * 为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。 如果 pos 是 -1，则在该链表中没有环。
 *
 * 说明：不允许修改给定的链表。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：head = [3,2,0,-4], pos = 1
 * 输出：tail connects to node index 1
 * 解释：链表中有一个环，其尾部连接到第二个节点。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/linked-list-cycle-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

import offer.ListNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-11 18:42
 **/
public class _142_环形链表II {

    // 注：无环返回null
    public ListNode detectCycle(ListNode head) {
        ListNode cycle = Cycle(head);
        // 无环的情况
        if ( cycle == null) {
            return cycle;
        }

        // 记录链表长度信息  此处必须从1 开始 否则会出现死循环
        int cycleLen = 1;

        // 遍历环中信息
        ListNode cur = cycle;
        while ( cur.next != cycle) {
            // 指针后移
            cur = cur.next;
            cycleLen ++ ;
        }

        // 移动cycleLen长度信息
        cur = head;
        for ( int i = 0 ; i < cycleLen ; i ++ ) {
            cur = cur.next;
        }

        /***
         * 必定出现死循环
        while ( cur != null) {
            cur = cur.next;
            head = head.next;
        }
        **/
        // 避免出现死循环
        while ( cur != head) {
            cur = cur.next;
            head = head.next;
        }
        return head;
    }

    // 校验链表是否有环
    private ListNode Cycle(ListNode head) {
        // 处理异常情况  空节点  或者一个结点的情况
        if ( head == null || head.next == null) {
            return null;
        }
        // 处理结点的遍历操作
        ListNode slow = head;
        ListNode fast = slow.next;

        while ( fast != null
                && fast.next != null
                && fast != slow) {
            slow = slow.next;
            fast = fast.next.next;
        }

        return fast == slow ? fast : null ;
    }
}
