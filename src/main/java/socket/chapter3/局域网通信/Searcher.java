package socket.chapter3.局域网通信;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 08:45
 **/
public class Searcher {

    // 服务器主要功能发送信息  接收provider提供信息
    // 处理单线程信息  实现一对一通信处理 多用户监听可放入到 线程中处理
    public static void main(String[] args) throws IOException {
        System.out.println("search start .........");
        // 采用系统分配端口进行接收数据
        DatagramSocket ds = new DatagramSocket(31456);
        String data = "from searcher message : hello provider";

        System.out.println("本地端口信息： "
                + InetAddress.getLocalHost()
                + "分配默认端口信息  "
                + ds.getPort());

        // 处理来自提供者的请求信息
        final  byte [] receiveData = new byte[512];
        DatagramPacket receivePack = new DatagramPacket(receiveData
                ,receiveData.length);
        ds.receive(receivePack);
        System.out.println( "接收到provider对象的端口" +
                receivePack.getPort());

        int port = receivePack.getPort();
        String ip = receivePack.getAddress().getHostAddress();
        int dataLen = receivePack.getLength();
        // 传送数据以byte数组形式进行数据传递，  最后通过String 构造器利用byte 数组进行构造
        // 提供起始位置 和  终止位置信息
        String  providerData = new String(receivePack.getData(),0,dataLen);

        System.out.println("Searcher receive Data from   " + ip
                + "  provider send port  "  + port
                + "  provider send data  " + providerData);


       /* DatagramPacket receivePack = new DatagramPacket(data.getBytes()
                ,data.getBytes().length);
        final byte [] receive = data.getBytes();
        // 构建提供者接收实体
        DatagramPacket provider = new DatagramPacket(receive,receive.length,
                InetAddress.getLocalHost(),
                30001);
        // 回送到提供者
        ds.send(provider);
*/
        ds.close();
    }

}
