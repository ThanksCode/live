package com.com.basic.java.字符串;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-19 18:15
 **/
public class _定义正则对象 {

    //使用预编译定义正则时，定义成静态变量信息，提高效率
    // Use precompile
    private static Pattern NUMBER_PATTERN = Pattern.compile("[0-9]+");

    public static void main(String[] args) {
        Matcher m = NUMBER_PATTERN.matcher("Stirng demo");
        m.group();
    }
}
