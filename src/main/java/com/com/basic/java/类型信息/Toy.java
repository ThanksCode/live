package com.com.basic.java.类型信息;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-24 17:07
 **/

public class Toy {

    public Toy(int i){
        System.out.println(i);
    }

    public static void main(String[] args) {
        Class clazz = null;
        Object obj = null ;
        try {
            clazz = Class.forName("com.com.basic.java.类型信息.Toy");
            //获取构造器进行实例化
            Constructor constructors = clazz.getConstructor(Integer.TYPE);
            obj = constructors.newInstance(1);
            //obj = clazz.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            if ( obj != null) {
                System.out.println(obj instanceof Toy);
                System.out.println(obj.getClass().getSimpleName());
                System.out.println(obj.getClass().getCanonicalName());
            }
        }

    }
}
