package offer;

import java.util.Arrays;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-23 18:49
 **/
public class _10_48_把数组排成最小的数 {
    public String PrintMinNumber(int[] numbers) {
        if (numbers == null || numbers.length == 0)
            return "";
        int n = numbers.length;
        String[] nums = new String[n];
        for (int i = 0; i < n; i++)
            nums[i] = numbers[i] + "";
        Arrays.sort(nums, (s1, s2) -> (s1 + s2).compareTo(s2 + s1));
        String ret = "";
        for (String str : nums)
            ret += str;
        return ret;
    }
}
