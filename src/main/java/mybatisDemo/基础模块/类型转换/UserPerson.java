package mybatisDemo.基础模块.类型转换;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-20 11:42
 **/
class A {

}
class B {

}
class C {

}
public class UserPerson {
    private static final Map<String, Class<?>> TYPE_ALIASES = new HashMap<String, Class<?>>();
    public static void main(String[] args) {
        String packageName = "mybatisDemo.基础模块.类型转换";
        ResolverUtil<Class<?>> resolverUtil = new ResolverUtil<Class<?>>();
        resolverUtil.find(new ResolverUtil.IsA(UserPerson.class), packageName);
        Set<Class<? extends Class<?>>> typeSet = resolverUtil.getClasses();
        for(Class<?> type : typeSet){
            // Ignore inner classes and interfaces (including package-info.java)
            // Skip also inner classes. See issue #6
            if (!type.isAnonymousClass() && !type.isInterface() && !type.isMemberClass()) {
                String name = type.getSimpleName();
                TYPE_ALIASES.put(name,type);
            }
        }
    }
}
