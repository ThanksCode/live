package patter.creational.Prototype;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

import java.io.*;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-08 21:22
 **/
public class User implements Cloneable,Serializable {
    private String name;
    private String age;
    private Job job;

    public User(String name ,String age ,Job job){
        this.name = name;
        this.age = age ;
        this.job = job;

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    // 返回本类对象信息   通过多次clone方法进行拷贝
   /* @Override
    protected Object clone() throws CloneNotSupportedException {
       Object user ;
       user = (User)super.clone();
       ((User) user).setJob((Job) job.clone());
       return user;
    }*/

    // 根据序列化的方式进行序列化的处理操作
    @Override
    protected Object clone() throws CloneNotSupportedException {
        User user = null;

        try {
            ByteOutputStream byteOutputStream = new ByteOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
            objectOutputStream.writeObject(this);

            // 重新生成新的对象信息
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteOutputStream.toByteArray());
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            user = (User)objectInputStream.readObject();

        }catch (IOException e ) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }
        return user;
    }
}
