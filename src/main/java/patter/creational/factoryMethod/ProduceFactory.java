package patter.creational.factoryMethod;

public abstract class ProduceFactory {

    public abstract Video getVideo();
    public abstract Article getArtile();
}
