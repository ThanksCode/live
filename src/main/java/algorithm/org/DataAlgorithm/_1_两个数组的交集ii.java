package algorithm.org.DataAlgorithm;
/***
 * 给定两个数组，编写一个函数来计算它们的交集。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：nums1 = [1,2,2,1], nums2 = [2,2]
 * 输出：[2,2]
 * 示例 2:
 *
 * 输入：nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * 输出：[4,9]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/intersection-of-two-arrays-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-19 22:29
 **/
public class _1_两个数组的交集ii {
    // 处理数组有序的情况下：利用归并思想进行处理
    public int[] intersect(int[] nums1, int[] nums2) {
        // 处理特殊情况
        if ( nums1 == null || nums2 == null) {
            return null;
        }
        // 排序元素信息
        sorts(nums1,nums2);
        ArrayList<Integer> rs = new ArrayList();
        int left = 0 ,right = 0;
        while(left < nums1.length
                &&  right < nums2.length){

            if ( nums1[left] < nums2[right]) {
                left ++ ;
            }else if ( nums1[left] > nums2[right]) {
                right ++ ;
            }else{
                rs.add(nums1[left]);
                left ++ ;
                right ++ ;
            }
        }

        return rs.stream().mapToInt(Integer::intValue).toArray();

    }

    private  void sorts(int [] a , int []b ){
        Arrays.sort(a);
        Arrays.sort(b);
    }

    public static void main(String[] args) {
        ArrayList<Integer> rs = new ArrayList<>();
        for ( int i = 0 ; i < 5 ; i ++  ){
            rs.add(i);
        }
        int [] tmp = rs.stream().mapToInt(Integer::intValue).toArray();
        System.out.println(Arrays.toString(tmp));
    }

}
