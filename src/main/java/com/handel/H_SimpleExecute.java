package com.handel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class H_SimpleExecute implements H_Execute {

    //执行查询方法
    //返回一个封装好的结果集
    @Override
    public <T> T query(String statement, String parameter) {
        Connection con = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        List<Users> users = new ArrayList<Users>();
        try{
            Class.forName("com.mysql.jdbc.Driver");//加载驱动
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mybaties","root","root");
            String sql = String.format(statement,Integer.parseInt(parameter));
            preparedStatement = con.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();//执行查询
            while( resultSet.next() ) {
                Users user = new Users();
                user.setId(resultSet.getInt(1));
                user.setUsername(resultSet.getString(2));
                user.setBirthday(resultSet.getDate(3));
                user.setSex(resultSet.getString(4));
                user.setAddress(resultSet.getString(5));
                users.add(user);
            }
        }catch (Throwable e) {
            throw new RuntimeException("信息加载错误，无法执行sql查询");
        }finally {
           try{
               if ( con != null) {
                   con.close();
               }
           }catch (SQLException E) {
               System.out.println(E);
           }

        }
        return (T)users.get(0);
    }
}
