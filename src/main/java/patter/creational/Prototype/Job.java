package patter.creational.Prototype;

import java.io.Serializable;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-08 21:54
 **/
public class Job implements Cloneable,Serializable {
    private String jobName ;

    public Job(String jobName) {
        this.jobName = jobName;
    }


    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
