package offer;

/***
 * 请设计一个函数，用来判断在一个矩阵中是否存在一条包含某字符串所有字符的路径。路径可以从矩阵中的任意一格开始，每一步可以在矩阵中向左、右、上、下移动一格。如果一条路径经过了矩阵的某一格，那么该路径不能再次进入该格子。例如，在下面的3×4的矩阵中包含一条字符串“bfce”的路径（路径中的字母用加粗标出）。
 *
 * [["a","b","c","e"],
 * ["s","f","c","s"],
 * ["a","d","e","e"]]
 *
 * 但矩阵中不包含字符串“abfb”的路径，因为字符串的第一个字符b占据了矩阵中的第一行第二个格子之后，路径不能再次进入这个格子。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ju-zhen-zhong-de-lu-jing-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-05 21:37
 **/
public class _10_34_矩阵中的路径_ {

    // 标记数组
    private boolean [][] visit = new boolean [200][200];
    // 长度信息
    private int pathLength = 0;

    public boolean existV1(char[][] board, String word) {
        if ( board == null || word == null) {
            return false;
        }

        //从每个顶点处进行枚举
        for ( int i = 0 ; i < board.length ; i ++ ){
            for ( int j = 0 ; j < board[i].length ; j ++ ) {
                if ( hasPath(board,word,i,j,pathLength)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasPath(char[][] board, String word, int row, int col, int pathLength) {

        // 递归结束条件
        if ( pathLength == word.length() ) {
            return true;
        }

        boolean flag = false;

        if ( row < board.length && row >= 0
                && col < board[row].length && col >= 0
                && visit[row][col] == false
                && board[row][col] == word.charAt(pathLength)){

            // 长度加一
            pathLength ++ ;
            // 将点置为以访问
            visit[row][col] = true;

            //  下 上 左  右
            flag = hasPath(board, word, row + 1, col, pathLength)
                    || hasPath(board, word, row - 1, col, pathLength)
                    || hasPath(board, word, row , col + 1, pathLength)
                    || hasPath(board, word, row , col - 1, pathLength);

            // 回溯
            if ( flag == false) {
                --pathLength;
                visit[row][col] = false;
            }


        }

        return flag;

    }


    /***
     * 2020-7-16 V1
     * @param board
     * @param word
     * @return
     */
    public boolean exist(char[][] board, String word) {
        // 异常处理
        if ( board == null || word == null) {
            return false;
        }

        // 需要遍历所有方格，找出所有可能
        for( int i = 0 ; i < board.length ; i ++ ) {
            for ( int j = 0 ; j  < board[0].length ; j ++ ) {
                // 提供起始位置 i , j word的索引信息
                if ( hasPathV1(board,word,i,j,pathLength)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasPathV1(char[][] board, String word, int row, int col, int pathLength) {
        // 递归终止条件
        if ( pathLength == word.length()) {
            return true;
        }

        boolean rs = false;
        // 判断数据合法性
        // 当前位置未访问 row , col 合法  word.charAt(index) == board[row][col]
        // 有一个细节： col < board[row].length 矩阵不一定是方阵 一定要注意！！！！
        if(  row >= 0 && row < board.length
                && col >= 0 && col < board[row].length
                &&!visit[row][col]
                && word.charAt(pathLength) == board[row][col]){

            // 路径信息加一
            pathLength ++ ;
            // 当前位置被访问
            visit[row][col] = true;

            // 递归遍历该点的周围信息
            rs = hasPathV1(board,word,row + 1 , col , pathLength)
                    ||hasPathV1(board,word,row - 1  , col , pathLength)
                    ||hasPathV1(board,word,row  , col + 1, pathLength)
                    ||hasPathV1(board,word,row   , col - 1 , pathLength);
            // 回溯处理
            if ( rs == false ){
                pathLength -- ;
                visit[row][col] = false;
            }
        }
        return rs;
    }


}
