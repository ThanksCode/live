package algorithm.org.binaryTree;

import offer.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-03 10:54
 **/
public class _104二叉树的最大深度_ {

    // 递归写法   DFS
    public int maxDepth(TreeNode root ) {

        if ( root == null) {
            return 0 ;
        }

        // 递归求出 左子树 右子树高度信息
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        // 返回其中最大者
        return (left > right ? left : right ) + 1;
    }

    // 转换思路，统计树最高有几层 每次有新的层元素加入层数加一  BFS
    public int getDepth(TreeNode root ,int index ) {

        // 特殊情况
        if ( root == null ){
            return 0;
        }

        int leveal = 0;
        // 辅助队列
        Queue queue =  new LinkedList<>();

        queue.add(root);

        // 加入一个 null 来表示当前层的元素已经遍历完
        while ( !queue.isEmpty()) {
            // 先入栈的元素出栈
            TreeNode cur = (TreeNode) queue.poll();

            if ( cur == null) {
                // 表明一层已经被遍历完成，此时需要加入结束标记符
                if ( !queue.isEmpty() ) {
                    queue.add(null);
                }
                // 无用信息
                int l1;
                leveal ++ ;
            }else{
                // 表明此时结点存在左右子树信息
                if ( cur.left != null ) {
                    queue.add(cur.left);
                }

                if ( cur.right != null) {
                    queue.add(cur.right);
                }
            }


        }


        return leveal;
    }


    /**
     * 2020 - 7 --  8
     */

    public int getMax(TreeNode root ){
        if ( root == null ) {
            return 0;
        }

        int left = getMax(root.left);
        int right = getMax(root.right);

        // 返回最大值
        return  left > right ? left + 1 : right + 1;
    }



    // 队列处理方式
    public int getDepth(TreeNode root) {
        // 记录层深度
        int leveal = 0 ;

        // 极端情况
        if( root == null ) {
            return leveal;
        }

        Queue queue = new LinkedList();

        // 根节点入队
        queue.add(root);
        // null 为每层遍历结束的标志
        queue.add(null);

        // 队列非空下进行循环遍历
        while ( !queue.isEmpty()){
            TreeNode cur = (TreeNode) queue.poll();
            // 表名一层已经别遍历完成
            if ( cur == null ) {
                // 加入一个null
               // ((LinkedList) queue).add(null);  // 会导致无限死循环！！！！！
                if( !queue.isEmpty() ){
                    queue.add(null);
                }
                leveal ++ ;
            }else{ // 此处不能放在外面 ！！！
                // 处理左子树，右子树
                if ( cur.left !=null){
                    queue.add(cur.left);
                }
                if ( cur.right != null) {
                    queue.add(cur.right);
                }
            }

        }
        return leveal;
    }

}
