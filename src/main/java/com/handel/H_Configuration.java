package com.handel;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * 占位符的使用
 * 第一种：使用%s占位，使用String.format转换
 *  String.format(url,name,age);
 * 第二种：使用{1}占位，使用MessageFormat.format转换
 *
 * String url02 = "我叫{0},今年{1}岁。";
 *  MessageFormat.format(url02,name,age);
 */
public class H_Configuration {

    //生成一个代理对象
    public <T> T getMapper(Class<T> tClass , H_SqlSession sqlSession) {

        return (T)Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{tClass},
                new H_MapperProxy(sqlSession));
    }


    //Configuration需要提供的信息：
    //xml映射配置信息
    static class MapperXml{

        public static final String nameSpace = "com.handel.IUser";//待实例化类信息
        public static final Map<String,String> methodSqlMapping = new HashMap<String, String>();//用来进行映射处理

        //初始化配置信息
        static {
            methodSqlMapping.put("findById","select * from user where id = %d");
        }
    }
}
