package algorithm.activeEverday;

import java.util.Arrays;

/***
 * 给定不同面额的硬币 coins 和一个总金额 amount。编写一个函数来计算可以凑成总金额所需的最少的硬币个数。如果没有任何一种硬币组合能组成总金额，返回 -1。
 *
 * 示例 1:
 *
 * 输入: coins = [1, 2, 5], amount = 11
 * 输出: 3
 * 解释: 11 = 5 + 5 + 1
 * 示例 2:
 *
 * 输入: coins = [2], amount = 3
 * 输出: -1
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/coin-change
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @datta  2020-3-8
 *
 * 考虑情况：
 *      数组不一定有序，需要对数组先排序
 *
 *   测试点   [186,419,83,408]  6249
 *      直接贪心容易忽略一个问题即：有可能不是从最大数开始探索，其后某个数字探索也能完成
 *
 *
 *   最先找到的并不是最优解
         * 31. 注意不是现实中发行的硬币，面值组合规划合理，会有奇葩情况
         * 32. 考虑到有 [1,7,10] 这种用例，按照贪心思路 10 + 1 + 1 + 1 + 1 会比 7 + 7 更早找到
         * 33. 所以还是需要把所有情况都递归完
 *
 *
 */
public class _322_零钱兑换 {
    /**第一版
     *    public int coinChange(int[] coins, int amount) {
     *         int cn = 0;
     *         int rs = amount;
     *         int sum = 0;
     *          Arrays.sort(coins);
     *         for ( int i = coins.length - 1 ; i >= 0; i -- ){
     *             int tmp = amount / coins[i];
     *             sum += tmp * coins[i];
     *             cn += tmp;
     *             amount -= tmp * coins[i];
     *             if ( sum == amount ) {
     *                 break;
     *             }
     *         }
     *         return  sum == rs ? cn :-1 ;
     *     }
     */
    private int ans = Integer.MAX_VALUE;
    public int coinChange(int[] coins, int amount) {
        if ( amount == 0 ){
            return 0;
        }
        Arrays.sort(coins);

        System.out.println(Arrays.toString(coins));
        dfs(coins,amount,coins.length - 1,0);
        return  ans == Integer.MAX_VALUE ? -1 : ans;
    }

    private void  dfs(int [] coins,int amount ,int index , int cn){
        if ( index < 0) {
            return;
        }
        for ( int k = amount / coins[index] ; k >= 0 ; k -- ){
            int diff = amount - k * coins[index];
            int count = cn + k;

            //找到
            if ( diff == 0 ){
               ans = Math.min(ans,count);
               break;
            }

            if ( count + 1 > ans) {
                break;
            }
            dfs(coins,diff,index -1 ,count);
        }
    }
    /***
     * 数组，目标数，索引，当前使用硬币量，最优解
     *
     */
   /* private void dfs ( int [] coins , int amount ,int index , int cn ){
        //避免贪心找到的不是最优解，而进行解之间的枚举
        if( amount == 0){
            ans = Math.min(cn,ans);
            return;
        }
        //还有一种可能即枚举到开头但是并没有找到最优解
        if( index == 0 ) {
            return;
        }

        for ( int k = amount / coins[index] ; k >= 0 && k + cn < ans ; k -- ){
            System.out.println("for " + ans);
            dfs(coins,amount - k * coins[index],index --,cn + k );
        }
    }*/

    public static void main(String[] args) {
        _322_零钱兑换 demo = new _322_零钱兑换();
        int [] rs = {1,2,5};
        int ts = demo.coinChange(rs,11);
        System.out.println(ts);
    }
}
