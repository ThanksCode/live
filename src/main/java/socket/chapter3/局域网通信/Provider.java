package socket.chapter3.局域网通信;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-02 08:13
 **/
public class Provider {
    // 作为接受者 指定一个端口用于数据接受数据 端口 20000
    // 构建接受实体
    // 接受信息
    // 打印接收到的信息与发送者的信息
    // 发送者ip
    //构建一份回送数据
    // 构建回送实体

    public static void main(String[] args) throws IOException {
        System.out.println("provider start ......");

        DatagramSocket ds = new DatagramSocket(30001);
        final byte [] buffer = new byte[512];

        // 构建接受实体
      /*  DatagramPacket receivePack = new DatagramPacket(buffer,0,buffer.length);
        ds.receive(receivePack);

        // 此时receiverPack中包含有从服务端发送而来的各种信息
        String ip = receivePack.getAddress().getHostAddress();
        int port = receivePack.getPort();
        int dateLen = receivePack.getLength();
        String serverDate = new String(receivePack.getData(),0,dateLen);
        System.out.println("Provider receive Data from " + ip
                + "  server port " + port
                + "  data  " + serverDate);*/


    // 发送信息

     /*   System.out.println(receivePack.getPort());*/
        // 构建回送信息和回送对象
        String  data = "from Provider message:  hello  server";
        byte [] byteData = data.getBytes();
        DatagramPacket backServer = new DatagramPacket(byteData,
                byteData.length,
                InetAddress.getLocalHost(),
                31456);

        ds.send(backServer);
        // 释放资源
        ds.close();


    }
}
