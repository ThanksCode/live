package com.com.basic.java.接口;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-11 09:17
 **/
public interface CanFight {
    void fight();
}
