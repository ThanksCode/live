package algorithm.org.DataAlgorithm;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-31 14:45
 **/
public class _13_缺失数字 {


    public int missingNumber(int[] nums) {
        int i = 0 , j = nums.length -1  ;
        // 二分查找
        while ( i <= j ) {
            int m = i + ( j - i ) / 2;
            if ( nums[m] != m) {
                // 判断前一个是否相等
                if( m == 0 ||  nums[m-1] == m - 1){
                    return m;
                }else{
                    j = m - 1;
                }
            }else{
                i = m + 1;
            }
        }
        if (i == nums.length){
            return i;
        }
        return -1;
    }

    public int missingNumber1(int[] nums) {
        int i = 0, j = nums.length - 1;
        while(i <= j) {
            int m = (i + j) / 2;
            if(nums[m] == m) i = m + 1;
            else j = m - 1;
        }
        return i;
    }


    public static void main(String[] args) {
        System.out.println( 6 /  ((double)6 / 5));
    }

}
