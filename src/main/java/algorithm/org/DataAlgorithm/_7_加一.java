package algorithm.org.DataAlgorithm;
/**
 * 给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
 *
 * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 *
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。
 *
 * 示例 1:
 *
 * 输入: [1,2,3]
 * 输出: [1,2,4]
 * 解释: 输入数组表示数字 123。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/plus-one
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-27 17:06
 **/
public class _7_加一 {

    public int[] plusOne(int[] digits) {

        if ( digits == null || digits.length == 0 ){
            return digits;
        }
        int [] rs = new int[digits.length + 1];
        // rs的索引信息
        int index = rs.length -1;
        // 记录各元素进位累计和信息
        int sum = 0 ;
        // 从后向前遍历
        for ( int i = digits.length - 1 ; i >= 0 ; i --){
            // 记录当前结果
            int cur = 0;
            if ( i == digits.length - 1){
               cur = digits[i]  + 1 + sum;
            }else{
                cur = digits[i] + sum;
            }
            // 更新digits数组内容信息
            digits[i] = cur % 10;
            //sum = (sum + (cur / 10)) %  10;
            sum = cur / 10;
            rs[index--] = cur % 10;
        }

        if ( sum > 10){
            rs[0] = 1;
        }else{
            return digits;
        }


        return rs;
    }

    public int[] plusOneV1(int[] digits){
        int [] rs = new int[digits.length + 1];
        int index = digits.length;
        int sum = 1;
        for ( int i = digits.length - 1 ; i >= 0 ; i --){
            // 记录当前求和信息
            int tmp = digits[i] + sum;
            // 保存信息
            digits[i] = tmp % 10;
            rs[index--] = digits[i];
            // 跟新进位信息
            sum = sum / 10;
        }

        if ( sum == 1){
            rs[0] = 1;
        }else{
           return digits;
        }
        return rs;
    }

}
