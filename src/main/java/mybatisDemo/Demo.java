package mybatisDemo;

import mybatisAnnioation.utils.PropertiesUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-16 10:07
 **/
public class Demo {

    public static void main(String[] args) throws NoSuchAlgorithmException, InterruptedException {
        String s = "123456778";
        MessageDigest md = MessageDigest.getInstance("MD5");
        System.out.println(s.getBytes().length);
        System.out.println(MD5Util.MD5EncodeUtf8(s));
        System.out.println(Arrays.toString(md.digest(s.getBytes())));
        System.out.println(PropertiesUtil.getProperty("password.salt",""));

        TimeTest time = new TimeTest();

        System.out.println(time.getCheckOut());
        Thread.sleep(1000);
        System.out.println(time.getCheckOut());
    }
}
