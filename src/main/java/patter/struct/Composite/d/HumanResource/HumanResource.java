package patter.struct.Composite.d.HumanResource;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-30 17:20
 **/
public abstract class HumanResource {

    protected long id;
    protected double salary;

    public HumanResource(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
    public abstract double calculateSalary();



}
