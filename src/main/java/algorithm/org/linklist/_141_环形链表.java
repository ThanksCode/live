package algorithm.org.linklist;

/***
 * 给定一个链表，判断链表中是否有环。
 *
 * 为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。 如果 pos 是 -1，则在该链表中没有环。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：head = [3,2,0,-4], pos = 1
 * 输出：true
 * 解释：链表中有一个环，其尾部连接到第二个节点。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/linked-list-cycle
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

// 思路：快慢指针

import offer.ListNode;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-10 17:13
 **/
public class _141_环形链表 {

    public boolean hasCycle(ListNode head) {
        // 处理异常情况  空节点  或者一个结点的情况
        if ( head == null || head.next == null) {
            return false;
        }

        // 定义快慢指针
        ListNode slow = head.next;
        ListNode fast = slow.next;

        // fast每次为一此时永远不会相遇！！！！！！ 出现死循环是合乎情理的！！！
    /*    while ( fast != null ){
            // 在环中相遇
            if ( fast == slow ) {
                return true;
            }

            slow = slow.next;
            fast = fast.next;
        }*/
        while ( fast != null
                && fast.next != null  // 主要避免空指针异常的情况
                && fast != slow) {
            slow = slow.next;
            // 这样写有一个问题就是 1 - null 此时双指针移动 必定指向null
            // 在不加条件判断的情况下会导致控制针异常
            fast = fast.next.next;
        }

        return fast == slow;
    }
}
