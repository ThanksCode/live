package offer;

/***
 * 输入一个英文句子，翻转句子中单词的顺序，但单词内字符的顺序不变。为简单起见，标点符号和普通字母一样处理。例如输入字符串"I am a student. "，则输出"student. a am I"。
 *
 *  
 *
 * 示例 1：
 *
 * 输入: "the sky is blue"
 * 输出: "blue is sky the"
 * 示例 2：
 *
 * 输入: "  hello world!  "
 * 输出: "world! hello"
 * 解释: 输入字符串可以在前面或者后面包含多余的空格，但是反转后的字符不能包括。
 * 示例 3：
 *
 * 输入: "a good   example"
 * 输出: "example good a"
 * 解释: 如果两个单词间有多余的空格，将反转后单词间的空格减少到只含一个。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fan-zhuan-dan-ci-shun-xu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date  2020-2-10
 */
public class _10_11_翻转单词顺序 {
    /***
     * 实例：
     *      hello word
     *      第一次：drow ollhe
     *      第二次：word hello
     * @param s
     * @return
     */
    public String reverseWords(String s) {
//去除首尾的空格
        String str = s.trim();
        if (str.equals("")){
            return "";
        }

//        正则匹配空格
        String [] strList = reverse(str).split("\\s+");

        StringBuilder sb = new StringBuilder();
        for ( int i = 0 ; i < strList.length ; i ++ ){
            sb.append(reverse(strList[i]));

            if ( i != strList.length - 1){
                sb.append(' ');
            }
        }

        return sb.toString();
    }
//自制旋转字符串函数
    private String reverse(String  str){
        StringBuilder sb = new StringBuilder(str);
        int start = 0 , last = str.length() - 1;

        while ( start < last) {
            char tmp = sb.charAt(start);
            sb.setCharAt(start,sb.charAt(last));
            sb.setCharAt(last,tmp);
            start ++ ;
            last -- ;
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        StringBuilder rs = new StringBuilder("hello world!");
        System.out.println(rs.reverse());
    }
}
