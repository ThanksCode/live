package patter.creational.proxy.auto;

public class Coder implements Person{

    private String sex = "男";
    private String name = "coders";
    @Override
    public void findLove() {
        System.out.println("我叫" + this.name + ",性别:" + this.sex );
        System.out.println(" 我找对象的要求是:  漂亮，贤惠，温柔");
    }
    @Override
    public void work() {
        System.out.println("工作狂");
    }
}
