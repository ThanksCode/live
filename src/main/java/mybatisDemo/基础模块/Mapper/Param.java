package mybatisDemo.基础模块.Mapper;

import java.lang.annotation.*;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-07-25 10:27
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Param {
    String value();
}
