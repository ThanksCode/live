package patter.creational.factoryMethod;

public interface Article {

    void writeArticle();
}
