package patter.geek;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: XMind
 * @description
 * @author: yihang
 * @create: 2020-06-28 15:09
 **/
interface Filter {
    void doFilter() throws  Exception ;
}
// 接口实现类：鉴权过滤器
 class AuthencationFilter implements Filter {
    @Override
    public void doFilter( ) throws Exception {
//... 鉴权逻辑..
    }
}

// 接口实现类：限流过滤器
 class RateLimitFilter implements Filter {
    @Override
    public void doFilter() throws Exception {
//... 限流逻辑...
    }
}

// 过滤器使用 demo
class Application {
    // filters.add(new AuthencationFilter());
// filters.add(new RateLimitFilter());
    private List<Filter> filters = new ArrayList<>();

    public void handleRpcRequest() {
        try {
            for (Filter filter : this.filters) {
                filter.doFilter();
            }
        } catch(Exception e) {
// ... 处理过滤结果...
        }
// ... 省略其他处理逻辑...
    }
}
public class _接口抽象类 {
}
