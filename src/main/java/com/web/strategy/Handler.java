package com.web.strategy;

import java.lang.reflect.Method;

/**
 * @program: livehood
 * @description
 * @author: yihang
 * @create: 2020-05-10 18:55
 **/
public class Handler {

    private Object controller;
    private Method method;
    private String url;

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
